/*
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE
#define _POSIX_C_SOURCE 199309L

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <pthread.h>

#include <mpfr.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <dlfcn.h>

#define ET_MAIN 1
#include "formula.h"
#undef ET_MAIN

static struct formula *formula = 0;

// http://www.burtleburtle.net/bob/hash/integer.html
static uint32_t wang_hash(uint32_t a)
{
    a = (a ^ 61) ^ (a >> 16);
    a = a + (a << 3);
    a = a ^ (a >> 4);
    a = a * 0x27d4eb2d;
    a = a ^ (a >> 15);
    return a;
}

static double dither(uint32_t x, uint32_t y, uint32_t c)
{
  return wang_hash(c + wang_hash(y + wang_hash(x))) / (double) (0x100000000LL) - 0.5;
}

struct et_pixel
{
  uint16_t x, y; uint8_t w, h;
};

static int et_pixel_cmp(const void *a, const void *b, void *c)
{
  const struct et_pixel *s = a;
  const struct et_pixel *t = b;
  const int *r = c;
  double x = r[0];
  double y = r[1];
  double dax = s->x - x;
  double day = s->y - y;
  double dbx = t->x - x;
  double dby = t->y - y;
  double da = dax * dax + day * day;
  double db = dbx * dbx + dby * dby;
  if (da < db) return -1;
  if (da > db) return 1;
  return 0;
}

struct et
{
  int width;
  int height;
  int maxiters;
  double escape_radius;
  double escape_radius_2;
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
  float *output;
  pthread_t thread;
  int volatile active;
  int volatile running;
  int volatile done_pixels;
  struct et_pixel *pixels;
  int volatile npixel;
};

static struct et *et_new(int width, int height)
{
  struct et *et = calloc(1, sizeof(struct et));
  assert(et);
  et->width = width;
  et->height = height;
  et->maxiters = 1 << 8;
  et->escape_radius = 10000;
  et->escape_radius_2 = et->escape_radius * et->escape_radius;
  mpfr_init2(et->centerx, 53);
  mpfr_init2(et->centery, 53);
  mpfr_init2(et->radius, 53);
  mpfr_set_d(et->centerx, 0, MPFR_RNDN);
  mpfr_set_d(et->centery, 0, MPFR_RNDN);
  mpfr_set_d(et->radius, 2, MPFR_RNDN);
  et->output = calloc(1, 4 * width * height * sizeof(float));
  assert(et->output);

  et->pixels = malloc(width * height * sizeof(struct et_pixel));
  assert(et->pixels);
  int center[2] = { width >> 1, height >> 1 };
  int step = 1 << 7;
  int ix = 0;
  int begin = ix;
  for (int y = 0; y < height; y += step)
    for (int x = 0; x < width; x += step)
    {
      et->pixels[ix].x = x;
      et->pixels[ix].y = y;
      et->pixels[ix].w = step;
      et->pixels[ix].h = step;
      ix++;
    }
  int end = ix;
  qsort_r(&et->pixels[begin], end - begin, sizeof(struct et_pixel), et_pixel_cmp, center);
  for (; step > 1; step >>= 1)
  {
    begin = ix;
    for (int y = 0;  y < height;  y += step)
      for (int x = step >> 1; x < width; x += step)
      {
        et->pixels[ix].x = x;
        et->pixels[ix].y = y;
        et->pixels[ix].w = step >> 1;
        et->pixels[ix].h = step;
        ix++;
      }
    end = ix;
    qsort_r(&et->pixels[begin], end - begin, sizeof(struct et_pixel), et_pixel_cmp, center);
    begin = ix;
    for (int y = step >> 1; y < height; y += step)
      for (int x = 0; x < width; x += step >> 1)
      {
        et->pixels[ix].x = x;
        et->pixels[ix].y = y;
        et->pixels[ix].w = step >> 1;
        et->pixels[ix].h = step >> 1;
        ix++;
      }
    end = ix;
    qsort_r(&et->pixels[begin], end - begin, sizeof(struct et_pixel), et_pixel_cmp, center);
  }
  assert(ix == width * height);
  return et;
}

static void et_delete(struct et *et)
{
  assert(et);
  assert(et->output);
  assert(et->pixels);
  free(et->output);
  free(et->pixels);
  free(et);
}

static void output_pixelf(struct et *et, int npixel, float *glitch, float g, float out0, float out1, float out2, float out3)
{
  int width = et->width;
  int height = et->height;
  int ii = et->pixels[npixel].x;
  int jj = et->pixels[npixel].y;
  int w = et->pixels[npixel].w;
  int h = et->pixels[npixel].h;
  float *output = et->output;
  for (int v = jj; v < jj + h && v < height; ++v)
  {
    for (int u = ii; u < ii + w && u < width; ++u)
    {
      int kk = v * width + u;
      if (glitch[kk] < 0)
      {
        output[4 * kk + 0] = out0;
        output[4 * kk + 1] = out1;
        output[4 * kk + 2] = out2;
        output[4 * kk + 3] = out3;
      }
    }
  }
  glitch[jj * width + ii] = g;
}

static void output_pixel(struct et *et, int npixel, double *glitch, double g, float out0, float out1, float out2, float out3)
{
  int width = et->width;
  int height = et->height;
  int ii = et->pixels[npixel].x;
  int jj = et->pixels[npixel].y;
  int w = et->pixels[npixel].w;
  int h = et->pixels[npixel].h;
  float *output = et->output;
  for (int v = jj; v < jj + h && v < height; ++v)
  {
    for (int u = ii; u < ii + w && u < width; ++u)
    {
      int kk = v * width + u;
      if (glitch[kk] < 0)
      {
        output[4 * kk + 0] = out0;
        output[4 * kk + 1] = out1;
        output[4 * kk + 2] = out2;
        output[4 * kk + 3] = out3;
      }
    }
  }
  glitch[jj * width + ii] = g;
}

static void output_pixell(struct et *et, int npixel, long double *glitch, long double g, float out0, float out1, float out2, float out3)
{
  int width = et->width;
  int height = et->height;
  int ii = et->pixels[npixel].x;
  int jj = et->pixels[npixel].y;
  int w = et->pixels[npixel].w;
  int h = et->pixels[npixel].h;
  float *output = et->output;
  for (int v = jj; v < jj + h && v < height; ++v)
  {
    for (int u = ii; u < ii + w && u < width; ++u)
    {
      int kk = v * width + u;
      if (glitch[kk] < 0)
      {
        output[4 * kk + 0] = out0;
        output[4 * kk + 1] = out1;
        output[4 * kk + 2] = out2;
        output[4 * kk + 3] = out3;
      }
    }
  }
  glitch[jj * width + ii] = g;
}

static void *et_worker(void *threadarg)
{
  struct et *et = threadarg;
  assert(et);

  int width = et->width;
  int height = et->height;
  int n = et->maxiters;
  long double er2 = et->escape_radius_2;
  long double h = mpfr_get_ld(et->radius, MPFR_RNDN) * 2.0 / height;
  float *output = et->output;
  assert(output);
  int e = 0;
  frexpl(h, &e);
  int i0 = width / 2;
  int j0 = height / 2;
  if (! (h > 0))
  {
    et->active = false;
    return 0;
  }

  if (0 && e > -21) // PLAIN FLOAT
  {

    float a = mpfr_get_flt(et->centerx, MPFR_RNDN);
    float b = mpfr_get_flt(et->centery, MPFR_RNDN);
    float *glitch = calloc(1, width * height * sizeof(float));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int k = j * width + i;
        glitch[k] = -1;
        output[4 * k + 0] = -1;
        output[4 * k + 1] = -1;
        output[4 * k + 2] = -1;
        output[4 * k + 3] = -1;
      }
    }
    et->npixel = 0;
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width && et->running; ++i)
      {
        int npixel;
        #pragma omp atomic capture
        npixel = et->npixel++;
        int ii = et->pixels[npixel].x;
        int jj = et->pixels[npixel].y;
        float x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h + a;
        float y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h + b;
        float out[4];
        if (formula->plainf(n, er2, h, x, y, out, &et->running))
        {
          output_pixelf(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
          #pragma omp atomic
          et->done_pixels++;
        }
      }
    }
    free(glitch);
    et->active = false;
    return 0;

  }
  else if (e > -50) // PLAIN DOUBLE
  {

    double a = mpfr_get_d(et->centerx, MPFR_RNDN);
    double b = mpfr_get_d(et->centery, MPFR_RNDN);
    double *glitch = calloc(1, width * height * sizeof(double));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int k = j * width + i;
        glitch[k] = -1;
        output[4 * k + 0] = -1;
        output[4 * k + 1] = -1;
        output[4 * k + 2] = -1;
        output[4 * k + 3] = -1;
      }
    }
    et->npixel = 0;
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width && et->running; ++i)
      {
        int npixel;
        #pragma omp atomic capture
        npixel = et->npixel++;
        int ii = et->pixels[npixel].x;
        int jj = et->pixels[npixel].y;
        double x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h + a;
        double y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h + b;
        double out[4];
        if (formula->plain(n, er2, h, x, y, out, &et->running))
        {
          output_pixel(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
          #pragma omp atomic
          et->done_pixels++;
        }
      }
    }
    free(glitch);
    et->active = false;
    return 0;

  }
  else if (e > -60) // PLAIN LONG DOUBLE
  {

    long double a = mpfr_get_ld(et->centerx, MPFR_RNDN);
    long double b = mpfr_get_ld(et->centery, MPFR_RNDN);
    long double *glitch = calloc(1, width * height * sizeof(long double));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int k = j * width + i;
        glitch[k] = -1;
        output[4 * k + 0] = -1;
        output[4 * k + 1] = -1;
        output[4 * k + 2] = -1;
        output[4 * k + 3] = -1;
      }
    }
    et->npixel = 0;
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width && et->running; ++i)
      {
        int npixel;
        #pragma omp atomic capture
        npixel = et->npixel++;
        int ii = et->pixels[npixel].x;
        int jj = et->pixels[npixel].y;
        long double x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h + a;
        long double y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h + b;
        long double out[4];
        if (formula->plainl(n, er2, h, x, y, out, &et->running))
        {
          output_pixell(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
          #pragma omp atomic
          et->done_pixels++;
        }
      }
    }
    free(glitch);
    et->active = false;
    return 0;

  }
  else if (0 && e > -120) // PERTURB FLOAT
  {

    mpfr_t a, b, r;
    int prec = 53 - mpfr_get_exp(et->radius);
    prec = prec < 53 ? 53 : prec;
    mpfr_init2(a, prec);
    mpfr_init2(b, prec);
    mpfr_init2(r, 53);
    mpfr_set(a, et->centerx, MPFR_RNDN);
    mpfr_set(b, et->centery, MPFR_RNDN);
    mpfr_set(r, et->radius, MPFR_RNDN);
    float *xyzs = malloc(n * 3 * sizeof(float));
    assert(xyzs);
    float *glitch = calloc(1, width * height * sizeof(float));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        glitch[j * width + i] = -1;
        output[4 * (j * width + i) + 0] = -1;
        output[4 * (j * width + i) + 1] = -1;
        output[4 * (j * width + i) + 2] = -1;
        output[4 * (j * width + i) + 3] = -1;
      }
    }

    bool glitched = true;
    int ref = 0;
    do
    {

      glitched = false;
      int m = formula->referencef(n, er2, a, b, xyzs, &et->running);
      et->npixel = 0;
      #pragma omp parallel for
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width && et->running; ++i)
        {
          int npixel;
          #pragma omp atomic capture
          npixel = et->npixel++;
          int ii = et->pixels[npixel].x;
          int jj = et->pixels[npixel].y;
          int k = jj * width + ii;
          if (glitch[k] < 0)
          {
            // ignore isolated small glitches
            int g = 0;
            float out[4];
            for (int v = -1; v <= 1; ++v)
            {
              for (int u = -1; u <= 1; ++u)
              {
                if (u && v) continue;
                if ((! u) && (! v)) continue;
                if (ii + u < 0) continue;
                if (ii + u >= width) continue;
                if (jj + v < 0) continue;
                if (jj + v >= height) continue;
                int kk = (jj + v) * width + (ii + u);
                g += (glitch[kk] < 0);
                out[0] = 1e-30;
                out[1] = output[4 * kk + 1];
                out[2] = output[4 * kk + 2];
                out[3] = output[4 * kk + 3];
              }
            }
            if (g == 0)
            {
              output[4 * k + 0] = out[0];
              output[4 * k + 1] = out[1];
              output[4 * k + 2] = out[2];
              output[4 * k + 3] = out[3];
              glitch[k] = 0;
              #pragma omp atomic
              et->done_pixels++;
            }
            else
            {
              float x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h;
              float y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h;
              if (formula->perturbationf(m, n, er2, h, x, y, xyzs, out, &et->running))
              {
                if (out[0] < 0)
                {
                  // the pixel was glitched
                  if (ii == i0 && jj == j0)
                  {
                    // the reference pixel was glitched, how can that be?
                    output_pixelf(et, npixel, glitch, 0, 1e-30, 0, 0, 0);
                    #pragma omp atomic
                    et->done_pixels++;
                  }
                  else
                  {
                    output_pixelf(et, npixel, glitch, out[0], -1, 0, 0, 0);
                    glitched = true;
                  }
                }
                else
                {
                  if (isnanf(out[0]) || isinff(out[0]))
                  {
                    out[0] = 1e-30;
                  }
                  output_pixelf(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
                  #pragma omp atomic
                  et->done_pixels++;
                }
              }
            }
          }
        }
      }

      if (glitched && et->running)
      {
        float ma = -1.0/0.0;
        int mi = width/2;
        int mj = height/2;
        for (int j = 0; j < height; ++j)
        {
          for (int i = 0; i < width; ++i)
          {
            int k = j * width + i;
            if (glitch[k] < 0 && ma < glitch[k])
            {
              ma = glitch[k];
              mi = i;
              mj = j;
            }
          }
        }
        float dx = ((mi + dither(mi, mj, 0)) - (i0 + dither(i0, j0, 0))) * h;
        float dy = ((mj + dither(mi, mj, 1)) - (j0 + dither(i0, j0, 1))) * h;
        mpfr_set_flt(r, dx, MPFR_RNDN);
        mpfr_add(a, a, r, MPFR_RNDN);
        mpfr_set_flt(r, dy, MPFR_RNDN);
        mpfr_add(b, b, r, MPFR_RNDN);
        i0 = mi;
        j0 = mj;
      }
      fprintf(stderr, "\r\t%8d (%d)", width * height - et->done_pixels, ++ref);

    } while (glitched && et->running);
    free(glitch);
    free(xyzs);
    mpfr_clear(a);
    mpfr_clear(b);
    mpfr_clear(r);
    et->active = false;
    return 0;

  }
  else if (e > -1020) // PERTURB DOUBLE
  {

    mpfr_t a, b, r;
    int prec = 53 - mpfr_get_exp(et->radius);
    prec = prec < 53 ? 53 : prec;
    mpfr_init2(a, prec);
    mpfr_init2(b, prec);
    mpfr_init2(r, 53);
    mpfr_set(a, et->centerx, MPFR_RNDN);
    mpfr_set(b, et->centery, MPFR_RNDN);
    mpfr_set(r, et->radius, MPFR_RNDN);
    double *xyzs = malloc(n * 3 * sizeof(double));
    assert(xyzs);
    double *glitch = calloc(1, width * height * sizeof(double));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        glitch[j * width + i] = -1;
        output[4 * (j * width + i) + 0] = -1;
        output[4 * (j * width + i) + 1] = -1;
        output[4 * (j * width + i) + 2] = -1;
        output[4 * (j * width + i) + 3] = -1;
      }
    }

    bool glitched = true;
    int ref = 0;
    do
    {

      glitched = false;
      int m = formula->reference(n, er2, a, b, xyzs, &et->running);
      et->npixel = 0;
      #pragma omp parallel for
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width && et->running; ++i)
        {
          int npixel;
          #pragma omp atomic capture
          npixel = et->npixel++;
          int ii = et->pixels[npixel].x;
          int jj = et->pixels[npixel].y;
          int k = jj * width + ii;
          if (glitch[k] < 0)
          {
            // ignore isolated small glitches
            int g = 0;
            double out[4];
            for (int v = -1; v <= 1; ++v)
            {
              for (int u = -1; u <= 1; ++u)
              {
                if (u && v) continue;
                if ((! u) && (! v)) continue;
                if (ii + u < 0) continue;
                if (ii + u >= width) continue;
                if (jj + v < 0) continue;
                if (jj + v >= height) continue;
                int kk = (jj + v) * width + (ii + u);
                g += (glitch[kk] < 0);
                out[0] = 1e-30;
                out[1] = output[4 * kk + 1];
                out[2] = output[4 * kk + 2];
                out[3] = output[4 * kk + 3];
              }
            }
            if (g == 0)
            {
              output[4 * k + 0] = out[0];
              output[4 * k + 1] = out[1];
              output[4 * k + 2] = out[2];
              output[4 * k + 3] = out[3];
              glitch[k] = 0;
              #pragma omp atomic
              et->done_pixels++;
            }
            else
            {
              double x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h;
              double y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h;
              if (formula->perturbation(m, n, er2, h, x, y, xyzs, out, &et->running))
              {
                if (out[0] < 0)
                {
                  // the pixel was glitched
                  if (ii == i0 && jj == j0)
                  {
                    // the reference pixel was glitched, how can that be?
                    output_pixel(et, npixel, glitch, 0, 1e-30, 0, 0, 0);
                    #pragma omp atomic
                    et->done_pixels++;
                  }
                  else
                  {
                    output_pixel(et, npixel, glitch, out[0], -1, 0, 0, 0);
                    glitched = true;
                  }
                }
                else
                {
                  if (isnan(out[0]) || isinf(out[0]))
                  {
                    out[0] = 1e-30;
                  }
                  output_pixel(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
                  #pragma omp atomic
                  et->done_pixels++;
                }
              }
            }
          }
        }
      }

      if (glitched && et->running)
      {
        double ma = -1.0/0.0;
        int mi = width/2;
        int mj = height/2;
        for (int j = 0; j < height; ++j)
        {
          for (int i = 0; i < width; ++i)
          {
            int k = j * width + i;
            if (glitch[k] < 0 && ma < glitch[k])
            {
              ma = glitch[k];
              mi = i;
              mj = j;
            }
          }
        }
        double dx = ((mi + dither(mi, mj, 0)) - (i0 + dither(i0, j0, 0))) * h;
        double dy = ((mj + dither(mi, mj, 1)) - (j0 + dither(i0, j0, 1))) * h;
        mpfr_set_d(r, dx, MPFR_RNDN);
        mpfr_add(a, a, r, MPFR_RNDN);
        mpfr_set_d(r, dy, MPFR_RNDN);
        mpfr_add(b, b, r, MPFR_RNDN);
        i0 = mi;
        j0 = mj;
      }
      fprintf(stderr, "\r\t%8d (%d)", width * height - et->done_pixels, ++ref);

    } while (glitched && et->running);
    free(glitch);
    free(xyzs);
    mpfr_clear(a);
    mpfr_clear(b);
    mpfr_clear(r);
    et->active = false;
    return 0;

  }
  else if (e > -16380) // PERTURB LONG DOUBLE
  {

    mpfr_t a, b, r;
    int prec = 53 - mpfr_get_exp(et->radius);
    prec = prec < 53 ? 53 : prec;
    mpfr_init2(a, prec);
    mpfr_init2(b, prec);
    mpfr_init2(r, 53);
    mpfr_set(a, et->centerx, MPFR_RNDN);
    mpfr_set(b, et->centery, MPFR_RNDN);
    mpfr_set(r, et->radius, MPFR_RNDN);
    long double *xyzs = malloc(n * 3 * sizeof(long double));
    assert(xyzs);
    long double *glitch = calloc(1, width * height * sizeof(long double));
    assert(glitch);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        glitch[j * width + i] = -1;
        output[4 * (j * width + i) + 0] = -1;
        output[4 * (j * width + i) + 1] = -1;
        output[4 * (j * width + i) + 2] = -1;
        output[4 * (j * width + i) + 3] = -1;
      }
    }

    bool glitched = true;
    int ref = 0;
    do
    {

      glitched = false;
      int m = formula->referencel(n, er2, a, b, xyzs, &et->running);
      et->npixel = 0;
      #pragma omp parallel for
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width && et->running; ++i)
        {
          int npixel;
          #pragma omp atomic capture
          npixel = et->npixel++;
          int ii = et->pixels[npixel].x;
          int jj = et->pixels[npixel].y;
          int k = jj * width + ii;
          if (glitch[k] < 0)
          {
            // ignore isolated small glitches
            int g = 0;
            long double out[4];
            for (int v = -1; v <= 1; ++v)
            {
              for (int u = -1; u <= 1; ++u)
              {
                if (u && v) continue;
                if ((! u) && (! v)) continue;
                if (ii + u < 0) continue;
                if (ii + u >= width) continue;
                if (jj + v < 0) continue;
                if (jj + v >= height) continue;
                int kk = (jj + v) * width + (ii + u);
                g += (glitch[kk] < 0);
                out[0] = 1e-30;
                out[1] = output[4 * kk + 1];
                out[2] = output[4 * kk + 2];
                out[3] = output[4 * kk + 3];
              }
            }
            if (g == 0)
            {
              output[4 * k + 0] = out[0];
              output[4 * k + 1] = out[1];
              output[4 * k + 2] = out[2];
              output[4 * k + 3] = out[3];
              glitch[k] = 0;
              #pragma omp atomic
              et->done_pixels++;
            }
            else
            {
              //
              long double x = ((ii + dither(ii, jj, 0)) - (i0 + dither(i0, j0, 0))) * h;
              long double y = ((jj + dither(ii, jj, 1)) - (j0 + dither(i0, j0, 1))) * h;
              if (formula->perturbationl(m, n, er2, h, x, y, xyzs, out, &et->running))
              {
                if (out[0] < 0)
                {
                  // the pixel was glitched
                  if (ii == i0 && jj == j0)
                  {
                    // the reference pixel was glitched, how can that be?
                    output_pixell(et, npixel, glitch, 0, 1e-30, 0, 0, 0);
                    #pragma omp atomic
                    et->done_pixels++;
                  }
                  else
                  {
                    output_pixell(et, npixel, glitch, out[0], -1, 0, 0, 0);
                    glitched = true;
                  }
                }
                else
                {
                  if (isnanl(out[0]) || isinfl(out[0]))
                  {
                    out[0] = 1e-30;
                  }
                  output_pixell(et, npixel, glitch, 0, out[0], out[1], out[2], out[3]);
                  #pragma omp atomic
                  et->done_pixels++;
                }
              }
            }
          }
        }
      }

      if (glitched && et->running)
      {
        long double ma = -1.0/0.0;
        int mi = width/2;
        int mj = height/2;
        for (int j = 0; j < height; ++j)
        {
          for (int i = 0; i < width; ++i)
          {
            int k = j * width + i;
            if (glitch[k] < 0 && ma < glitch[k])
            {
              ma = glitch[k];
              mi = i;
              mj = j;
            }
          }
        }
        long double dx = ((mi + dither(mi, mj, 0)) - (i0 + dither(i0, j0, 0))) * h;
        long double dy = ((mj + dither(mi, mj, 1)) - (j0 + dither(i0, j0, 1))) * h;
        mpfr_set_ld(r, dx, MPFR_RNDN);
        mpfr_add(a, a, r, MPFR_RNDN);
        mpfr_set_ld(r, dy, MPFR_RNDN);
        mpfr_add(b, b, r, MPFR_RNDN);
        i0 = mi;
        j0 = mj;
      }
      fprintf(stderr, "\r\t%8d (%d)", width * height - et->done_pixels, ++ref);

    } while (glitched && et->running);
    free(glitch);
    free(xyzs);
    mpfr_clear(a);
    mpfr_clear(b);
    mpfr_clear(r);
    et->active = false;
    return 0;

  }
  else
  {
    et->active = false;
    return 0;
  }
}

static void et_start(struct et *et, const mpfr_t cx, const mpfr_t cy, const mpfr_t r, int maxiters)
{
  mpfr_fprintf(stderr, "Re: %Re\nIm: %Re\nSize: %Re\n", cx, cy, r);
  et->maxiters = maxiters;
  int precision = 53 - mpfr_get_exp(r);
  precision = precision > 53 ? precision : 53;
  mpfr_set_prec(et->centerx, precision);
  mpfr_set_prec(et->centery, precision);
  mpfr_set(et->centerx, cx, MPFR_RNDN);
  mpfr_set(et->centery, cy, MPFR_RNDN);
  mpfr_set(et->radius, r, MPFR_RNDN);
  memset(et->output, 0, 4 * et->width * et->height * sizeof(float));
  et->done_pixels = 0;
  et->running = true;
  et->active = true;
  pthread_create(&et->thread, 0, et_worker, et);
}

static void et_stop(struct et *et, bool force)
{
  if (force) {
    et->running = false;
  }
  pthread_join(et->thread, 0);
}

static bool et_active(const struct et *et)
{
  return et->active;
}

static const float *et_get_output(const struct et *et)
{
  return et->output;
}

static inline int max(int a, int b) {
  return a > b ? a : b;
}

static const char *envs(const char *name, const char *def) {
  const char *e = getenv(name);
  if (e) {
    return e;
  } else {
    return def;
  }
}

static int envi(const char *name, int def) {
  const char *e = getenv(name);
  if (e) {
    return atoi(e);
  } else {
    return def;
  }
}

static double envd(const char *name, double def) {
  const char *e = getenv(name);
  if (e) {
    return atof(e);
  } else {
    return def;
  }
}

static int envr(mpfr_t out, const char *name, const char *def) {
  const char *e = getenv(name);
  if (e) {
    return mpfr_set_str(out, e,   10, MPFR_RNDN);
  } else {
    return mpfr_set_str(out, def, 10, MPFR_RNDN);
  }
}

static const char *blit_vert =
  "#version 130\n"
  "uniform vec4 bounds;\n"
  "in float vertexID;\n"
  "out vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = bounds.xy; } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = bounds.zy; } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = bounds.xw; } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = bounds.zw; }\n"
  "}\n"
  ;

static const char *blit_frag =
  "#version 130\n"
  "uniform sampler2D tex;\n"
  "in vec2 texCoord;\n"
  "out vec4 fragColor;\n"
  "void main() {\n"
  "  fragColor = texture(tex, texCoord);\n"
  "}\n"
  ;

static const char *simple_vert =
  "#version 130\n"
  "in float vertexID;\n"
  "out vec2 texCoord;\n"
  "void main() {\n"
  "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = vec2(0.0, 1.0); } else\n"
  "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = vec2(1.0, 1.0); } else\n"
  "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = vec2(0.0, 0.0); } else\n"
  "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = vec2(1.0, 0.0); }\n"
  "}\n"
  ;

static const char *simple_frag =
  "#version 130\n"
  "uniform sampler2D tex;\n"
  "uniform highp float weight;\n"
  "in vec2 texCoord;\n"
  "out vec4 fragColor;\n"
  "// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl\n"
  "vec3 hsv2rgb(vec3 c) {\n"
  "  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n"
  "  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n"
  "  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);\n"
  "}\n"
  "void main() {\n"
  "  vec2 dx = dFdx(texCoord);\n"
  "  vec2 dy = dFdy(texCoord);\n"
  "  vec4 me = texture(tex, texCoord);\n"
  "  vec4 a = texture(tex, texCoord + dx);\n"
  "  vec4 b = texture(tex, texCoord + dy);\n"
  "  vec4 c = texture(tex, texCoord + dx + dy);\n"
  "  float de = me.x;\n"
  "  if (isnan(de) || isinf(de)) de = 0.0;\n"
  "  float s = de > 0.0 ? (me.y != c.y || a.y != b.y) ? 1.0 : 0.25 : 0.0;\n"
  "  float h = max(max(me.y, c.y), max(a.y, b.y)) / 24.618033988749893;\n"
  "  h -= floor(h);\n"
  "  float v = de > 0.0 ? tanh(clamp(de / weight, 0.0, 8.0)) : 1.0;\n"
  "  bool glitch = de < 0.0;\n"
  "  fragColor = vec4(glitch ? vec3(1.0, 0.0, 0.0) : hsv2rgb(vec3(h,s,v)), glitch ? 0.0 : 1.0);\n"
  "}\n"
  ;

struct state_s {
  GLFWwindow *window;
  struct et *context;

  bool should_stop;
  bool should_restart;
  bool should_redraw;
  bool should_save_now;
  bool should_save_when_done;
  bool should_view_morph;
  bool should_view_morph_julia;

  GLuint fbo;

  GLuint blit_vao;
  GLuint blit_p;
  GLint bounds_u;

  GLuint colourize_vao;
  GLuint colourize_p;
  bool show_lines;
  GLint show_lines_u;
  double log2weight;
  GLint weight_u;

  int width;
  int height;

  int precision;
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
  int maxiters;
  double escape_radius_2;

  double srcx0;
  double srcy0;
  double srcx1;
  double srcy1;

  uint8_t *ppm;
  int starttime;
  int saved;
};
typedef struct state_s state_t;
state_t state_d;

static void refresh_callback(void *user_pointer);

static void button_handler(GLFWwindow *window, int button, int action, int mods) {
  state_t *state = glfwGetWindowUserPointer(window);
  if (action == GLFW_PRESS) {
    double srcx0 = 0, srcy0 = 0, srcx1 = state->width, srcy1 = state->height;
    double x = 0, y = 0;
    glfwGetCursorPos(window, &x, &y);
    mpfr_t cx, cy, r;
    mpfr_init2(cx, state->precision);
    mpfr_init2(cy, state->precision);
    mpfr_init2(r, 53);
    mpfr_set(cx, state->centerx, MPFR_RNDN);
    mpfr_set(cy, state->centery, MPFR_RNDN);
    mpfr_set(r, state->radius, MPFR_RNDN);
    double w = state->width;
    double h = state->height;
    double yy = h - y;
    double dx = 2 * ((x + 0.5) / w - 0.5) * (w / h);
    double dy = 2 * ((y + 0.5) / h - 0.5);
    mpfr_t ddx, ddy;
    mpfr_init2(ddx, 53);
    mpfr_init2(ddy, 53);
    mpfr_mul_d(ddx, r, dx, MPFR_RNDN);
    mpfr_mul_d(ddy, r, dy, MPFR_RNDN);
    switch (button) {
      case GLFW_MOUSE_BUTTON_LEFT:
        mpfr_mul_2si(ddx, ddx, -1, MPFR_RNDN);
        mpfr_mul_2si(ddy, ddy, -1, MPFR_RNDN);
        mpfr_add(cx, cx, ddx, MPFR_RNDN);
        mpfr_add(cy, cy, ddy, MPFR_RNDN);
        mpfr_mul_2si(r, r, -1, MPFR_RNDN);
        state->precision += 1;
        mpfr_set_prec(state->centerx, state->precision);
        mpfr_set_prec(state->centery, state->precision);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        mpfr_set(state->radius, r, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 0.5 * (srcx0 - x) + x;
        srcx1 = 0.5 * (srcx1 - x) + x;
        srcy0 = 0.5 * (srcy0 - yy) + yy;
        srcy1 = 0.5 * (srcy1 - yy) + yy;
        break;
      case GLFW_MOUSE_BUTTON_RIGHT:
        mpfr_sub(cx, cx, ddx, MPFR_RNDN);
        mpfr_sub(cy, cy, ddy, MPFR_RNDN);
        mpfr_mul_2si(r, r, 1, MPFR_RNDN);
        state->precision -= 1;
        mpfr_set_prec(state->centerx, state->precision);
        mpfr_set_prec(state->centery, state->precision);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        mpfr_set(state->radius, r, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 2.0 * (srcx0 - x) + x;
        srcx1 = 2.0 * (srcx1 - x) + x;
        srcy0 = 2.0 * (srcy0 - yy) + yy;
        srcy1 = 2.0 * (srcy1 - yy) + yy;
        break;
      case GLFW_MOUSE_BUTTON_MIDDLE:
        mpfr_add(cx, cx, ddx, MPFR_RNDN);
        mpfr_add(cy, cy, ddy, MPFR_RNDN);
        mpfr_set(state->centerx, cx, MPFR_RNDN);
        mpfr_set(state->centery, cy, MPFR_RNDN);
        state->should_restart = true;
        srcx0 -= state->width / 2 - x;
        srcx1 -= state->width / 2 - x;
        srcy0 -= state->height / 2 - yy;
        srcy1 -= state->height / 2 - yy;
        break;
    }
    mpfr_clear(cx);
    mpfr_clear(cy);
    mpfr_clear(r);
    mpfr_clear(ddx);
    mpfr_clear(ddy);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, state->fbo);
    glBlitFramebuffer(0, 0, state->width, state->height, 0, 0, state->width, state->height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    state->srcx0 = srcx0;
    state->srcy0 = srcy0;
    state->srcx1 = srcx1;
    state->srcy1 = srcy1;
  }
(void) mods;
}

static void key_handler(GLFWwindow *window, int key, int scancode, int action, int mods) {
  state_t *state = glfwGetWindowUserPointer(window);
  double srcx0 = 0, srcy0 = 0, srcx1 = state->width, srcy1 = state->height;
  double x = state->width/2, y = state->height/2;
  bool capture = false;
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
      case GLFW_KEY_ESCAPE:
        state->should_stop = true;
        break;
      case GLFW_KEY_J:
        state->should_view_morph = true;
        state->should_view_morph_julia = true;
        break;
      case GLFW_KEY_M:
        state->should_view_morph = true;
        state->should_view_morph_julia = false;
        break;
      case GLFW_KEY_C:
        glBindFramebuffer(GL_FRAMEBUFFER, state->fbo);
        glClear(GL_COLOR_BUFFER_BIT);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        state->should_redraw = true;
        break;
      case GLFW_KEY_S:
        if (mods & GLFW_MOD_SHIFT) {
          state->should_save_now = true;
        } else {
          state->should_save_when_done = true;
        }
        break;
      case GLFW_KEY_9:
        state->log2weight -= 0.25;
        state->should_redraw = true;
        fprintf(stderr, "W = %g\n", state->log2weight);
        break;
      case GLFW_KEY_0:
        state->log2weight += 0.25;
        state->should_redraw = true;
        fprintf(stderr, "W = %g\n", state->log2weight);
        break;
      case GLFW_KEY_MINUS:
        state->maxiters >>= 1;
        state->maxiters = state->maxiters < 16 ? 16 : state->maxiters;
        fprintf(stderr, "N = %d\n", state->maxiters);
        state->should_restart = true;
        break;
      case GLFW_KEY_EQUAL:
        state->maxiters <<= 1;
        state->maxiters = state->maxiters > 1 << 30 ? 1 << 30 : state->maxiters;
        fprintf(stderr, "N = %d\n", state->maxiters);
        state->should_restart = true;
        break;
      case GLFW_KEY_PAGE_UP:
        mpfr_mul_2si(state->radius, state->radius, -1, MPFR_RNDN);
        state->precision += 1;
        mpfr_prec_round(state->centerx, state->precision, MPFR_RNDN);
        mpfr_prec_round(state->centery, state->precision, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 0.5 * (srcx0 - x) + x;
        srcx1 = 0.5 * (srcx1 - x) + x;
        srcy0 = 0.5 * (srcy0 - y) + y;
        srcy1 = 0.5 * (srcy1 - y) + y;
        capture = true;
        state->should_restart = true;
        break;
      case GLFW_KEY_PAGE_DOWN:
        mpfr_mul_2si(state->radius, state->radius, 1, MPFR_RNDN);
        state->precision -= 1;
        mpfr_prec_round(state->centerx, state->precision, MPFR_RNDN);
        mpfr_prec_round(state->centery, state->precision, MPFR_RNDN);
        state->should_restart = true;
        srcx0 = 2.0 * (srcx0 - x) + x;
        srcx1 = 2.0 * (srcx1 - x) + x;
        srcy0 = 2.0 * (srcy0 - y) + y;
        srcy1 = 2.0 * (srcy1 - y) + y;
        capture = true;
        state->should_restart = true;
        break;
    }
  }
  if (capture)
  {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, state->fbo);
    glBlitFramebuffer(0, 0, state->width, state->height, 0, 0, state->width, state->height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    state->srcx0 = srcx0;
    state->srcy0 = srcy0;
    state->srcx1 = srcx1;
    state->srcy1 = srcy1;
  }

(void) scancode;
}

static void refresh_callback(void *user_pointer) {
  state_t *state = user_pointer;
  assert(state);
  glBindVertexArray(state->blit_vao);
  glUseProgram(state->blit_p);
  glUniform4f(state->bounds_u, state->srcx0 / state->width, state->srcy0 / state->height, state->srcx1 / state->width, state->srcy1 / state->height);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(state->colourize_vao);
  glUseProgram(state->colourize_p);
  glActiveTexture(GL_TEXTURE0);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RGBA, GL_FLOAT, et_get_output(state->context));
  glUniform1i(state->show_lines_u, state->show_lines);
  glUniform1f(state->weight_u, exp2f(state->log2weight));
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glfwSwapBuffers(state->window);
}

static void handle_view_morph(struct et *context, state_t *state) {
  (void) context;
  if (state->should_view_morph) {
    state->should_view_morph = false;
    volatile int running = 1;
    int p = formula->period(state->maxiters, state->escape_radius_2, state->centerx, state->centery, state->radius, &running);
    if (p > 0)
    {
      fprintf(stderr, "p = %d\n", p);
      int precision = mpfr_get_prec(state->centerx);
      mpfr_t a, b, r, d;
      mpfr_init2(a, 4 * precision);
      mpfr_init2(b, 4 * precision);
      mpfr_init2(r, 53);
      mpfr_init2(d, 53);
      mpfr_set(a, state->centerx, MPFR_RNDN);
      mpfr_set(b, state->centery, MPFR_RNDN);
      bool ok = formula->newton(64, p, a, b, &running);
      if (ok)
      {
        ok = formula->size(p, a, b, r, &running);
        if (ok)
        {
          mpfr_fprintf(stderr, "s = %Re\n", r);
          if (state->should_view_morph_julia)
          {
            double n = formula->degree;
            mpfr_set_d(d, (n + 1) * (n - 1) / (n * n), MPFR_RNDN);
            mpfr_pow(r, r, d, MPFR_RNDN);
          }
          mpfr_mul_si(r, r, 4, MPFR_RNDN);
          mpfr_fprintf(stderr, "r = %Re\n", r);
          int e = mpfr_get_exp(r);
          int prec = max(53, 53 - e);
          state->precision = prec;
          mpfr_set_prec(state->centerx, prec);
          mpfr_set_prec(state->centery, prec);
          mpfr_set(state->centerx, a, MPFR_RNDN);
          mpfr_set(state->centery, b, MPFR_RNDN);
          mpfr_set(state->radius, r, MPFR_RNDN);
          state->should_restart = true;
        }
      }
      mpfr_clear(a);
      mpfr_clear(b);
      mpfr_clear(r);
      mpfr_clear(d);
    }
  }
}

void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_GEOMETRY_SHADER: tname = "geometry"; break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, NULL);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glBindAttribLocation(program, 0, "vertexID");
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}


bool read_ppm_comment(state_t *state, const char *filename)
{
  FILE *file = fopen(filename, "rb");
  if (! file) return false;
  bool ok = false;
  char *sx = 0, *sy = 0, *sz = 0;
  if ((ok = (3 == fscanf(file, "P6\n# Re: %ms\n# Im: %ms\n# Size: %ms\n", &sx, &sy, &sz))))
  {
    if ((ok = (0 == mpfr_set_str(state->radius, sz, 0, MPFR_RNDN))))
    {
      int prec = 53 - mpfr_get_exp(state->radius);
      if (prec < 53) prec = 53;
      mpfr_set_prec(state->centerx, prec);
      mpfr_set_prec(state->centery, prec);
      ok &= (0 == mpfr_set_str(state->centerx, sx, 0, MPFR_RNDN));
      ok &= (0 == mpfr_set_str(state->centery, sy, 0, MPFR_RNDN));
    }
  }
  if (sx) free(sx);
  if (sy) free(sy);
  if (sz) free(sz);
  fclose(file);
  return ok;
}

void save_screenshot(state_t *state, bool to_stdout)
{
  FILE *file = stdout;
  if (! to_stdout)
  {
    char filename[100];
    snprintf(filename, 100, "%08x_%04d.ppm", state->starttime, state->saved++);
    file = fopen(filename, "wb");
    fprintf(stderr, "saving: '%s'\n", filename);
  }
  glReadPixels(0, 0, state->width, state->height, GL_RGB, GL_UNSIGNED_BYTE, state->ppm);
  if (to_stdout)
    fprintf(file, "P6\n%d %d\n255\n", state->width, state->height);
  else
    mpfr_fprintf(file, "P6\n# Re: %Re\n# Im: %Re\n# Size: %Re\n%d %d\n255\n", state->centerx, state->centery, state->radius, state->width, state->height);
  for (int y = state->height - 1; y >= 0; --y) {
    fwrite(state->ppm + y * state->width * 3, state->width * 3, 1, file);
  }
  fflush(file);
  if (! to_stdout)
  {
    fclose(file);
  }
}

extern int main(int argc, char **argv) {
  state_t *state = &state_d;
  memset(state, 0, sizeof(*state));
  state->starttime = time(0);

  const char *name = envs("formula", "Mandelbrot");
  int p = envi("p", 2);
  int q = envi("q", 1);
  char libso[1024];
  snprintf(libso, 1000, "./lib/%s_p%d_q%d.so", name, p, q);
  void *library = dlopen(libso, RTLD_NOW);
  assert(library);
  formula = dlsym(library, "et");
  assert(formula);
  assert(formula->magic == MAGIC);
  assert(formula->ssize == SIZE);
  assert(formula->version == VERSION);

  int width = envi("width", 640);
  int height = envi("height", 360);
  int maxiters = envi("maxiters", 1 << 8);
  double escape_radius = envd("escaperadius", 10000);
  int precision = envi("precision", 53);
  int zoom_start = envi("zoom_start", 0);
  int zoom_count = envi("zoom_count", 0);
  double weight = envd("weight", 0);

  mpfr_init2(state->radius, 53);
  envr(state->radius, "radius", "2.0");

  int e = max(53, 53 - mpfr_get_exp(state->radius));
  if (e > precision) {
    fprintf(stderr, "WARNING: increasing precision to %d\n", e);
    precision = e;
  }

  mpfr_init2(state->centerx, precision);
  mpfr_init2(state->centery, precision);
  envr(state->centerx, "real", "0.0");
  envr(state->centery, "imag", "0.0");

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  state->window = glfwCreateWindow(width, height, "et", 0, 0);
  glfwMakeContextCurrent(state->window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew

  glClearColor(1.0, 0.0, 0.0, 1.0);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  GLuint ftex;
  glGenTextures(1, &ftex);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, ftex);
//  printf("%dx%d GL_RGBA\n", width, height);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glGenFramebuffers(1, &state->fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, state->fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ftex, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  GLuint tex;
  glGenTextures(1, &tex);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex);
//  printf("%dx%d GL_RGBA32F\n", width, height);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  GLuint vbo;
  GLubyte vbo_data[4] = { 0, 1, 2, 3 };
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_data), vbo_data, GL_STATIC_DRAW);

  glGenVertexArrays(1, &state->blit_vao);
  glBindVertexArray(state->blit_vao);
  state->blit_p = compile_program("blit", blit_vert, blit_frag);
  glUseProgram(state->blit_p);
  glUniform1i(glGetUniformLocation(state->blit_p, "tex"), 1);
  state->bounds_u = glGetUniformLocation(state->blit_p, "bounds");
  GLint attr = glGetAttribLocation(state->blit_p, "vertexID");
  glVertexAttribPointer(attr, 1, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(attr);
  glBindVertexArray(0);

  glGenVertexArrays(1, &state->colourize_vao);
  glBindVertexArray(state->colourize_vao);
  state->colourize_p = compile_program("colourize", simple_vert, simple_frag);
  glUseProgram(state->colourize_p);
  state->weight_u = glGetUniformLocation(state->colourize_p, "weight");
  attr = glGetAttribLocation(state->colourize_p, "vertexID");
  glVertexAttribPointer(attr, 1, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(attr);

  state->ppm = malloc(width * height * 3);
  assert(state->ppm);

  state->should_stop = false;
  state->should_restart = false;
  state->should_redraw = false;
  state->should_save_now = false;
  state->should_save_when_done = false;
  state->width = width;
  state->height = height;
  state->precision = precision;
  state->log2weight = weight;
  state->maxiters = maxiters;
  state->escape_radius_2 = escape_radius * escape_radius;
  state->srcx0 = 0;
  state->srcy0 = 0;
  state->srcx1 = state->width;
  state->srcy1 = state->height;

  state->context = et_new(width, height);

  glfwSetWindowUserPointer(state->window, state);

  if (argc > 1)
  {
    for (int a = 1; a < argc; ++a)
    {
      if (read_ppm_comment(state, argv[a]))
      {
        fprintf(stderr, "START '%s'\n", argv[a]);
        et_start(state->context, state->centerx, state->centery, state->radius, state->maxiters);
        et_stop(state->context, false);
        refresh_callback(state);
        save_screenshot(state, false);
      }
    }
  } else if (zoom_count) {
    mpfr_set_d(state->radius, 256, MPFR_RNDN);
    mpfr_div_2exp(state->radius, state->radius, zoom_start, MPFR_RNDN);
    for (int z = zoom_start; z < zoom_count; ++z) {
      glfwPollEvents();
      if (glfwWindowShouldClose(state->window)) {
        break;
      }
      fprintf(stderr, "%8d FRAME\n", z);
      et_start(state->context, state->centerx, state->centery, state->radius, state->maxiters);
      et_stop(state->context, false);
      refresh_callback(state);
      save_screenshot(state, true);
      mpfr_div_2exp(state->radius, state->radius, 1, MPFR_RNDN);
    }

  } else {
    glfwSetMouseButtonCallback(state->window, button_handler);
    glfwSetKeyCallback(state->window, key_handler);

    bool first = true;
    while (! glfwWindowShouldClose(state->window)) {
      int e = glGetError();
      if (e)
        fprintf(stderr, "E: %d\n", e);

      state->should_restart = false;
  //    fprintf(stderr, "start\n");
      if (! first) {
        et_stop(state->context, true);
      }
      first = false;
      et_start(state->context, state->centerx, state->centery, state->radius, state->maxiters);

  //    fprintf(stderr, "wait_timeout\n");
      while (et_active(state->context)) {
        struct timespec delta = { 0, 33333333 };
        nanosleep(&delta, 0);
  //      fprintf(stderr, "refresh\n");
        glActiveTexture(GL_TEXTURE0);
//        printf("%dx%d\n", state->width, state->height);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, state->width, state->height, GL_RGBA, GL_FLOAT, et_get_output(state->context));
        refresh_callback(state);

        glfwPollEvents();
        if (glfwWindowShouldClose(state->window)) {
          break;
        }

        if (state->should_save_now) {
          state->should_save_now = false;
          save_screenshot(state, false);
        }

        handle_view_morph(state->context, state);

        if (state->should_restart) {
          state->should_restart = false;
  //        fprintf(stderr, "start\n");
          et_stop(state->context, true);
          et_start(state->context, state->centerx, state->centery, state->radius, state->maxiters);
        }
  //      fprintf(stderr, "wait_timeout\n");
        if (state->should_stop) {
          et_stop(state->context, true);
          state->should_stop = false;
        }
      }

      if (glfwWindowShouldClose(state->window)) {
        break;
      }

      refresh_callback(state);

      while (! state->should_restart) {

        if (state->should_save_now || state->should_save_when_done) {
          state->should_save_now = false;
          state->should_save_when_done = false;
          save_screenshot(state, false);
        }

        glfwWaitEvents();
        if (glfwWindowShouldClose(state->window)) {
          break;
        }

  //      if (state->should_redraw) {
  //        state->should_redraw = false;
  //        fprintf(stderr, "refresh\n");
          refresh_callback(state);
//          glfwSwapBuffers(state->window);
  //      }

        handle_view_morph(state->context, state);

      }

    }

  //  fprintf(stderr, "delete\n");
    et_stop(state->context, true);
  }


  et_delete(state->context);
  free(state->ppm);
  glfwDestroyWindow(state->window);
  glfwTerminate();

  return 0;
(void) argc;
(void) argv;
}
