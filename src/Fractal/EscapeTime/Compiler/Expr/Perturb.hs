{-
et -- escape time fractals
Copyright (C) 2018,2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.Expr.Perturb where

import Numeric

import Fractal.EscapeTime.Compiler.Expr.AST

import Numeric (expm1, log1p)

widenE :: (Eq a, Show a) => [(a, (Expr a, Expr a))] -> Expr a -> Expr a
widenE _ n@(EInt _) = n
widenE _ n@(ERat _) = n
widenE vs (EVar _ v) = case lookup v vs of Just (x, y) -> x + y ; Nothing -> error $ "widenE " ++ show v
widenE vs (ENeg _ a) = negate (widenE vs a)
widenE vs (EInv _ a) = recip (widenE vs a)
widenE vs (EAdd _ a b) = widenE vs a + widenE vs b
widenE vs (EMul _ a b) = widenE vs a * widenE vs b
widenE vs (EAbs _ a) = abs (widenE vs a)
widenE vs (ESin _ a) = sin (widenE vs a)
widenE vs (ECos _ a) = cos (widenE vs a)
widenE vs (ETan _ a) = tan (widenE vs a)
widenE vs (ESinh _ a) = sinh (widenE vs a)
widenE vs (ECosh _ a) = cosh (widenE vs a)
widenE vs (ETanh _ a) = tanh (widenE vs a)
widenE vs (EExp _ a) = exp (widenE vs a)
widenE vs (EExpM1 _ a) = expm1 (widenE vs a)
widenE vs (ESqrt _ a) = sqrt (widenE vs a)
widenE vs (ELog _ a) = log (widenE vs a)
widenE vs (ELog1P _ a) = log1p (widenE vs a)
widenE vs n@(EPi _) = n
widenE _ e = error $ "widenE " ++ show e

biggenE :: (Eq a, Show a) => [(a, (Expr a, Expr a))] -> Expr a -> Expr a
biggenE _ n@(EInt _) = n
biggenE _ n@(ERat _) = n
biggenE vs (EVar _ v) = case lookup v vs of Just (x, _) -> x ; Nothing -> error $ "biggenE " ++ show v
biggenE vs (ENeg _ a) = negate (biggenE vs a)
biggenE vs (EInv _ a) = recip (biggenE vs a)
biggenE vs (EAdd _ a b) = biggenE vs a + biggenE vs b
biggenE vs (EMul _ a b) = biggenE vs a * biggenE vs b
biggenE vs (EAbs _ a) = abs (biggenE vs a)
biggenE vs (ESin _ a) = sin (biggenE vs a)
biggenE vs (ECos _ a) = cos (biggenE vs a)
biggenE vs (ETan _ a) = tan (biggenE vs a)
biggenE vs (ESinh _ a) = sinh (biggenE vs a)
biggenE vs (ECosh _ a) = cosh (biggenE vs a)
biggenE vs (ETanh _ a) = tanh (biggenE vs a)
biggenE vs (EExp _ a) = exp (biggenE vs a)
biggenE vs (EExpM1 _ a) = expm1 (biggenE vs a)
biggenE vs (ESqrt _ a) = sqrt (biggenE vs a)
biggenE vs (ELog _ a) = log (biggenE vs a)
biggenE vs (ELog1P _ a) = log1p (biggenE vs a)
biggenE _ n@(EPi _) = n
biggenE _ e = error $ "biggenE " ++ show e

perturbE :: [(String, (Expr String, Expr String))] -> Expr String -> Expr String
perturbE _ (EInt _) = 0
perturbE _ (ERat _) = 0
perturbE vs (EVar _ v) = case lookup v vs of Just (_, y) -> y ; Nothing -> error $ "perturbE " ++ show v
perturbE vs (ENeg _ a  ) = negate (perturbE vs a)
perturbE vs (EInv _ a  ) = negate (perturbE vs a) / (biggenE vs a * widenE vs a)
perturbE vs (EAdd _ a b) = perturbE vs a + perturbE vs b
perturbE vs (EMul _ a b) = perturbE vs a * widenE vs b + biggenE vs a * perturbE vs b
perturbE vs (EAbs _ a  ) = diffabs (biggenE vs a) (perturbE vs a)
perturbE vs (ESin _ a  ) =  2 * sin (perturbE vs a * 0.5) * cos ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (ECos _ a  ) = -2 * sin (perturbE vs a * 0.5) * sin ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (ETan _ a  ) = sin (perturbE vs a) / (cos (widenE vs a) * cos (biggenE vs a))
perturbE vs (ESinh _ a ) = 2 * sinh (perturbE vs a * 0.5) * cosh ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (ECosh _ a ) = 2 * sinh (perturbE vs a * 0.5) * sinh ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (ETanh _ a ) = sinh (perturbE vs a) / (cosh (widenE vs a) * cosh (biggenE vs a))
perturbE vs (EExp   _ a ) = 2 * sinh (perturbE vs a * 0.5) * exp ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (EExpM1 _ a ) = 2 * sinh (perturbE vs a * 0.5) * exp ((widenE vs a + biggenE vs a) * 0.5)
perturbE vs (ESqrt _ a ) = perturbE vs a / (sqrt (widenE vs a) + sqrt (biggenE vs a))
perturbE vs (ELog _ a) = log1p (perturbE vs a / biggenE vs a)
perturbE vs (ELog1P _ a) = log1p (perturbE vs a / (1 + biggenE vs a))
perturbE _ (EPi _) = 0
perturbE _ e = error $ "perturbE " ++ show e

perturb :: [(String, (Expr String, Expr String))] -> Expr String -> Expr String
perturb vs = perturbE vs
