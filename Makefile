# et -- escape time fractals
# Copyright (C) 2018 Claude Heiland-Allen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CABAL ?= cabal
MAKE ?= make
C99 ?= gcc -std=c99 -Wall -Wextra -pedantic -fPIC -O3 -I./src/

EXE_SUFFIX ?=
LIB_SUFFIX ?= .so

FORMULAS := $(patsubst %.c,%$(LIB_SUFFIX),$(wildcard lib/*.c))

all: bin/et$(EXE_SUFFIX) $(FORMULAS)

formulas: formulas.stamp

bin/et$(EXE_SUFFIX): src/et.c
	$(C99) -fopenmp -o $@ $< -lmpfr -lgmp -lglfw -lGLEW -lGL -lm -ldl

lib/%$(LIB_SUFFIX): lib/%.c
	$(C99) -shared -o $@ $<

formulas.stamp: .cabal-sandbox/bin/et src/formulas.et
	.cabal-sandbox/bin/et < src/formulas.et ./lib && touch formulas.stamp

.cabal-sandbox/bin/et: et.cabal .cabal-sandbox
	$(CABAL) install

.cabal-sandbox:
	$(CABAL) sandbox init

.PHONY: all formulas
