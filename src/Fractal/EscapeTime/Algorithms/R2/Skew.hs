{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-
<https://fractalforums.org/fractal-mathematics-and-new-theories/28/automatic-skew-adjustment-for-burning-ship-etc/1777/msg9133#msg9133>
Re: automatic skew adjustment for Burning Ship etc
gerrit
2018-09-05 19:01
If c0 is your escaping reference location, compute orbit and
matrices dfdc and dfdz (z = (x0,y0)).
Skew matrix is then S = inv(dfdc)*dfdz, S -> S/sqrt(abs(det(S)))
evaluated at escape iteration.
Then c <-- S*(c-c0)+c0.
-}

module Fractal.EscapeTime.Algorithms.R2.Skew (skew) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

skew :: [(Int, R2Formula (Expr String))] -> String
skew fs = runCompile_ FDouble . function NBool "skew" [(NInt, "N"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NBool, "DZ"), (NFloatPtr, "M"), (NVolatileIntPtr, "RUNNING")] $ \s -> do
  let n = EVar NInt "N"
      d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
      a = EVar NReal "A"
      b = EVar NReal "B"
      m = EVar NFloatPtr "M"
      useDZ = EVar NInt "DZ"
      r = 65536 -- FIXME take escape radius as parameter?
      runningP = EVar NVolatileIntPtr "RUNNING"
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  x   <- real
  y   <- real
  dxa <- real
  dxb <- real
  dya <- real
  dyb <- real
  dxx <- real
  dxy <- real
  dyx <- real
  dyy <- real
  x .= a
  y .= b
  dxa .= 1
  dxb .= 0
  dya .= 0
  dyb .= 1
  dxx .= 1
  dxy .= 0
  dyx .= 0
  dyy .= 1
  -- loop one period
  j <- int
  j .= 1
  k <- int
  k .= 0
  running <- bool
  running .= read runningP 0
  z2 <- real
  z2 .= x * x + y * y
  while_ ((j `ELess` n) `EAnd` (z2 `ELess` r) `EAnd` running) $ do
    -- alloc next
    xn <- real
    yn <- real
    dxan <- real
    dxbn <- real
    dyan <- real
    dybn <- real
    dxxn <- real
    dxyn <- real
    dyxn <- real
    dyyn <- real
    -- step
    let g (_, f) j0 kn = (,) j0 $ do
          let R2 gx gy = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize gx
              fy = optimize gy
              d v u
                | u == x && v == a = dxa
                | u == x && v == b = dxb
                | u == y && v == a = dya
                | u == y && v == b = dyb
                | u == x && v == x = dxx
                | u == x && v == y = dxy
                | u == y && v == x = dyx
                | u == y && v == y = dyy
                | u == v = 1
                | otherwise = 0
          -- calc next
          xn .= fx
          yn .= fy
          dxan .= optimize (ddx d a fx)
          dxbn .= optimize (ddx d b fx)
          dyan .= optimize (ddx d a fy)
          dybn .= optimize (ddx d b fy)
          _ <- if_ useDZ (do
            dxxn .= optimize (ddx d x fx)
            dxyn .= optimize (ddx d y fx)
            dyxn .= optimize (ddx d x fy)
            dyyn .= optimize (ddx d y fy)) (return ())
          -- step vars
          x .= xn
          y .= yn
          dxa .= dxan
          dxb .= dxbn
          dya .= dyan
          dyb .= dybn
          _ <- if_ useDZ (do
            dxx .= dxxn
            dxy .= dxyn
            dyx .= dyxn
            dyy .= dyyn) (return ())
          z2 .= x * x + y * y
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- loop
    j .= j + 1
    running .= read runningP 0
  _ <- if_ running (do
    -- s = normalize(dc^-1 dz)
    sii <- real
    sij <- real
    sji <- real
    sjj <- real
    _ <- if_ useDZ (do
      sii .= dyb * dxx - dxb * dyx
      sij .= dyb * dxy - dxb * dyy
      sji .= dxa * dyx - dya * dxx
      sjj .= dxa * dyy - dya * dxy) (do
      sii .=   dyb
      sij .= - dxb
      sji .= - dya
      sjj .=   dxa)
    det <- real
    det .= sqrt (abs (sii * sjj - sij * sji))
    sii .= sii / det
    sij .= sij / det
    sji .= sji / det
    sjj .= sjj / det
    write m 0 sii
    write m 1 sij
    write m 2 sji
    write m 3 sjj) (return ())
  s .= running
  return_ s
