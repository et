{-
et -- escape time fractals
Copyright (C) 2018,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.CodeGen.Expr where

import Fractal.EscapeTime.Compiler.CodeGen.Core
import Fractal.EscapeTime.Compiler.CodeGen.OpCode
import Fractal.EscapeTime.Compiler.Expr.AST

read :: Expr String -> Expr String -> Expr String
read = ERead

withVarAs :: NType -> Expr String -> (Expr String -> CompileM a) -> CompileM a
withVarAs s e@(EVar t _) a | s == t = a e
withVarAs s e a = do
  x <- alloc s
  expr x e
  r <- a x
  free x
  return r

withVar :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withVar e@(EVar _ _) a = a e
withVar e a = do
  x <- alloc (eType e)
  expr x e
  r <- a x
  free x
  return r

withBool :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withBool = withVarAs NBool

withInt :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withInt = withVarAs NInt

withFloat :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withFloat = withVarAs NFloat

withReal :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withReal = withVarAs NReal

withFloatPtr :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withFloatPtr = withVarAs NFloatPtr

withIntPtr :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withIntPtr = withVarAs NIntPtr

withVolatileIntPtr :: Expr String -> (Expr String -> CompileM a) -> CompileM a
withVolatileIntPtr = withVarAs NVolatileIntPtr

write :: Expr String -> Expr String -> Expr String -> CompileM ()
write p a b =
  withFloatPtr p $ \q ->
  withInt a $ \x ->
  withFloat b $ \y ->
  op_write q x y

writeI :: Expr String -> Expr String -> Expr String -> CompileM ()
writeI p a b =
  withIntPtr p $ \q ->
  withInt a $ \x ->
  withInt b $ \y ->
  op_write q x y

(.=) :: Expr String -> Expr String -> CompileM ()
infix 0 .=
v .= e = expr v e

expr :: Expr String -> Expr String -> CompileM ()
expr v (EIf p t f) =
  withBool p $ \x ->
  withVarAs (eType v) t $ \y ->
  withVarAs (eType v) f $ \z ->
  op3 O'ifte v x y z
expr v (ERead p a) =
  withVar p $ \q ->
  withInt a $ \x ->
  op2 O'read v q x
expr v (EFloat a) =
  withFloat a $ \x ->
  op1 O'set v x
expr v (EInt n) = op_lit v n
expr v (ERat r) = op_litf v r
expr v (EPi _) = op0 O'pi v
expr v (EAdd _ a (EMul _ (-1) b)) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'sub v x y
expr v (EAdd _ a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'add v x y
expr v (EMul _ a b)
  | a == 1 = expr v b
  | b == 1 = expr v a
  | a == fromInteger (-1) = expr v b >> op1 O'neg v v
  | b == fromInteger (-1) = expr v a >> op1 O'neg v v
  | a == 2 = expr v b >> op1 O'mul2 v v
  | b == 2 = expr v a >> op1 O'mul2 v v
  | a == fromInteger (-2) = expr v b >> op1 O'mul2 v v >> op1 O'neg v v
  | b == fromInteger (-2) = expr v a >> op1 O'mul2 v v >> op1 O'neg v v
  | a == b = withVar a $ \x -> op1 O'sqr v x
  | otherwise = withVar a $ \x -> withVar b $ \y -> op2 O'mul v x y
expr v (ENeg _ a) = do
  v .= a
  op1 O'neg v v
expr v (EAbs _ a) = do
  v .= a
  op1 O'abs v v
expr v (ESgn   a) =
  withVar a $ \x ->
  op1 O'sgn v x
expr v a@(EVar _ _) =
  op1 O'set v a
expr v (ESqrt _ a) =
  withVar a $ \x ->
  op1 O'sqrt v x
expr v (EExpM1 _ a) =
  withVar a $ \x ->
  op1 O'expm1 v x
expr v (ELog1P _ a) =
  withVar a $ \x ->
  op1 O'log1p v x
expr v (EExp _ a) =
  withVar a $ \x ->
  op1 O'exp v x
expr v (ELog _ a) =
  withVar a $ \x ->
  op1 O'log v x
expr v (EFloor _ a) =
  withVar a $ \x ->
  op1 O'floor v x
expr v (EInv _ a) =
  withVar a $ \x ->
  op1 O'inv v x

expr v (ESin _ a) =
  withVar a $ \x ->
  op1 O'sin v x
expr v (ECos _ a) =
  withVar a $ \x ->
  op1 O'cos v x
expr v (ETan _ a) =
  withVar a $ \x ->
  op1 O'tan v x
expr v (ESinh _ a) =
  withVar a $ \x ->
  op1 O'sinh v x
expr v (ECosh _ a) =
  withVar a $ \x ->
  op1 O'cosh v x
expr v (ETanh _ a) =
  withVar a $ \x ->
  op1 O'tanh v x
expr v (EASin _ a) =
  withVar a $ \x ->
  op1 O'asin v x
expr v (EACos _ a) =
  withVar a $ \x ->
  op1 O'acos v x
expr v (EATan _ a) =
  withVar a $ \x ->
  op1 O'atan v x
expr v (EASinh _ a) =
  withVar a $ \x ->
  op1 O'asinh v x
expr v (EACosh _ a) =
  withVar a $ \x ->
  op1 O'acosh v x
expr v (EATanh _ a) =
  withVar a $ \x ->
  op1 O'atanh v x
expr v (ENot a) =
  withVar a $ \x ->
  op1 O'not v x

expr v (EDiffAbs _ a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'diffabs v x y
expr v (ELess a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'lt v x y
expr v (ELessEqual a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'le v x y
expr v (EEqual a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'eq v x y
expr v (EAnd a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'and v x y
expr v (EOr a b) =
  withVar a $ \x ->
  withVar b $ \y ->
  op2 O'or v x y
