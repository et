{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Buffer where

import qualified Data.Vector.Storable.Mutable as V

data Buffer t = Buffer{ width, height, channels :: Int, contents :: V.IOVector t }

buffer :: V.Storable a => Int -> Int -> Int -> a -> IO (Buffer a)
buffer w h c i = do
  v <- V.replicate (w * h * c) i
  return (Buffer w h c v)
