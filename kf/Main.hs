{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

import Control.Monad (forM_, when)
import System.Environment (getArgs)
import Data.Set (Set)
import qualified Data.Set as S
import Control.Concurrent.Async (forConcurrently_)

import Fractal.EscapeTime.Formulas.Types (R2(..), R2Formula)
import Fractal.EscapeTime.Compiler.Parser (formulas)
import Fractal.EscapeTime.Compiler.Expr.AST (Expr(EVar), NType(NFloat), interpret)
import Fractal.EscapeTime.Compiler.Expr.Deriv (ddx)
import Fractal.EscapeTime.Compiler.Expr.Optimize (superoptimize, metric)
import Fractal.EscapeTime.Compiler.Expr.Perturb (perturb)
import Fractal.EscapeTime.Compiler.Expr.Pretty (pretty)
import Fractal.EscapeTime.Algorithms.R2 (period_tri, period_jsk, newton, size, skew)

import Perturbation (perturbation)
import Reference (reference)

type Formula = R2 (Expr String) -> R2 (Expr String) -> R2 (Expr String) -> R2 (Expr String)

main :: IO ()
main = do
  [dir] <- getArgs
  e <- formulas <$> getContents
  case e of
    Left msg -> error msg
    Right fs -> do

{-

      forConcurrently_ (zip [0..] fs) $ \(n, (name, vars, f)) -> do
        let q = 0
        if S.member "p" vars
        then forConcurrently_ [2 .. if n == 0 then 5{-10-} else 5] $ \p -> case f p q of
          [g] -> main' dir n name (Just p) g
        else let p = 0 in case f p q of
          [g] -> main' dir n name Nothing g

      writeFile (dir ++ "/combo5_addstrings.c") . unlines $
        [ "#include <windows.h>"
        , "void combo5_addstrings(HWND hWnd, const int IDC_COMBO5)"
        , "{"
        ] ++
        [ "  SendDlgItemMessage(hWnd,IDC_COMBO5,CB_ADDSTRING,0,(LPARAM)" ++ show name ++ "); // " ++ show n
        | (n, (name, _, _)) <- zip [0..] fs
        ] ++
        [ "}"
        ]

      writeFile (dir ++ "/update_power_dropdown_for_fractal_type.c") . unlines $
        [ "#include <windows.h>"
        , "void update_power_dropdown_for_fractal_type(HWND hWnd, const int IDC_COMBO3, const int m_nFractalType, const int m_nPower)"
        , "{"
        , "  SendDlgItemMessage(hWnd,IDC_COMBO3,CB_RESETCONTENT,0,0);"
        , "  int selected = 0;"
        , "  int ix = 0;"
        , "  switch (m_nFractalType)"
        , "  {"
        ] ++
        [ unlines
          [ "    case " ++ show n ++ ": // " ++ name
          , "    {"
          , unlines . concat $
            [ [ "       if (" ++ show power ++ " == m_nPower) selected = ix;"
              , "       SendDlgItemMessage(hWnd,IDC_COMBO3,CB_ADDSTRING,0,(LPARAM)" ++ show (show power) ++ ");"
              , "       ix++;"
              ]
            | power <- powers_of n vars f
            ]
          , "       break;"
          , "    }"
          ]
        | (n, (name, vars, f)) <- zip [0..] fs
        ] ++
        [ "  }"
        , "  SendDlgItemMessage(hWnd,IDC_COMBO3,CB_SETCURSEL,selected,0);"
        , "  EnableWindow(GetDlgItem(hWnd,IDC_COMBO3), ix > 1);"
        , "}"
        ]

      writeFile (dir ++ "/validate_power_for_fractal_type.c") . unlines $
        [ "int validate_power_for_fractal_type(const int m_nFractalType, const int m_nPower)"
        , "{"
        , "  switch (m_nFractalType)"
        , "  {"
        ] ++
        [ unlines
          [ "    case " ++ show n ++ ": // " ++ name
          , "      switch (m_nPower)"
          , "      {"
          , "        default:"
          , unlines $
            [ "        case " ++ show power ++ ": return " ++ show power ++ ";"
            | power <- powers_of n vars f
            ]
          , "      }"
          , "      break;"
          ]
        | (n, (name, vars, f)) <- zip [0..] fs
        ] ++
        [ "    default:"
        , "      return 2;"
        , "  }"
        , "}"
        ]
-}

      forM_ (zip [0..] fs) $ \(n, (name, vars, f)) -> when (n > -1) $ do
        putStrLn$"  <group type=\"" ++ show n ++ "\" name=\"" ++ name ++ "\">"
        let formula g = do
              let Just p = compute_degree g
                  glitch = 0.1 ** ([7,6,5,4,4,3,3,2,2] !! (fromIntegral p - 2))
                  -- variable
                  v = EVar NFloat
                  aar = v "Ar"
                  aai = v "Ai"
                  ccr = v "Cr"
                  cci = v "Ci"
                  xxr = v "Xr"
                  xxi = v "Xi"
                  cr = v "cr"
                  ci = v "ci"
                  xr = v "xr"
                  xi = v "xi"
                  xxxr = v "Xxr"
                  xxxi = v "Xxi"
                  cccr = v "Ccr"
                  ccci = v "Cci"
                  daa = v "daa"
                  dab = v "dab"
                  dba = v "dba"
                  dbb = v "dbb"
                  dxa = v "dxa"
                  dxb = v "dxb"
                  dya = v "dya"
                  dyb = v "dyb"
                  optimize' = superoptimize 1000 100
                  -- reference
                  R2 xxrn0 xxin0 = g (R2 aar aai) (R2 ccr cci) (R2 xxr xxi)
                  xxrn = optimize' xxrn0
                  xxin = optimize' xxin0
                  -- perturbation
                  vs =
                    [ ("Ar", (aar, 0))
                    , ("Ai", (aai, 0))
                    , ("Cr", (ccr, cr))
                    , ("Ci", (cci, ci))
                    , ("Xr", (xxr, xr))
                    , ("Xi", (xxi, xi))
                    ]
                  xrn = optimize' $ perturb vs xxrn
                  xin = optimize' $ perturb vs xxin
                  -- derivative
                  R2 xxxrn0 xxxin0 = g (R2 aar aai) (R2 cccr ccci) (R2 xxxr xxxi)
                  xxxrn = optimize' xxxrn0
                  xxxin = optimize' xxxin0
                  dxan = optimize' $ ddx d cccr xxxrn
                  dxbn = optimize' $ ddx d ccci xxxrn
                  dyan = optimize' $ ddx d cccr xxxin
                  dybn = optimize' $ ddx d ccci xxxin
                  d v u
                    | u == xxxr && v == cccr = dxa
                    | u == xxxr && v == ccci = dxb
                    | u == xxxi && v == cccr = dya
                    | u == xxxi && v == ccci = dyb
                    | u == cccr && v == cccr = daa
                    | u == cccr && v == ccci = dab
                    | u == ccci && v == cccr = dba
                    | u == ccci && v == ccci = dbb
                    | u == v = 1
                    | otherwise = 0
              putStrLn$"    <formula power=\"" ++ show p ++ "\" glitch=\"" ++ show glitch ++ "\">"
              putStrLn "      <reference t='R'>"
              putStrLn$"        Xrn = " ++ pretty xxrn ++ "; //" ++ pretty xxrn0 ++ "; //" ++ show (metric xxrn)
              putStrLn$"        Xin = " ++ pretty xxin ++ "; //" ++ pretty xxin0 ++ "; //" ++ show (metric xxin)
              putStrLn "      </reference>"
              putStrLn "      <perturbation t='R'>"
              putStrLn$"        xrn = " ++ pretty xrn ++ "; //" ++ show (metric xrn)
              putStrLn$"        xin = " ++ pretty xin ++ "; //" ++ show (metric xin)
              putStrLn "      </perturbation>"
              putStrLn "      <derivative t='M'>"
              putStrLn$"        dxan = " ++ pretty dxan ++ "; //" ++ show (metric dxan)
              putStrLn$"        dxbn = " ++ pretty dxbn ++ ": //" ++ show (metric dxbn)
              putStrLn$"        dyan = " ++ pretty dyan ++ "; //" ++ show (metric dyan)
              putStrLn$"        dybn = " ++ pretty dybn ++ "; //" ++ show (metric dybn)
              putStrLn "      </derivative>"
              putStrLn "    </formula>"
        if S.member "p" vars
        then forM_ [2 .. if n == 0 then 5{-10-} else 5] $ \p -> case f p 0 of
          [g] -> formula g
        else let p = 0 in case f p 0 of
          [g] -> formula g
        putStrLn "  </group>"
        putStrLn ""

powers_of :: Int -> Set String -> (Int -> Int -> [Formula]) -> [Integer]
powers_of n vars f =
  if S.member "p" vars
    then [2 .. if n == 0 then 10 else 5]
    else case compute_degree (case f 0 0 of [g] -> g :: Formula) of Just p -> [p]

compute_degree :: Formula -> Maybe Integer
compute_degree f = do
  let er :: Double
      er = recip 10000
      v = EVar NFloat
      vs = [("a", 0), ("b", 0), ("d", 1), ("e", 0), ("x", er), ("y", 0)]
      R2 fx fy = f (R2 (v "d") (v "e") ) (R2 (v "a") (v "b")) (R2 (v "x") (v "y"))
  x <- interpret vs fx
  y <- interpret vs fy
  let z = sqrt $ x * x + y * y
      p = round $ log z / log er
  return $ max p 2

main' :: String -> Int -> String -> Maybe Int -> Formula -> IO ()
main' dir index human_name mp f = do
  let p = case mp of
        Nothing -> case compute_degree f of
          Just q -> fromInteger q
        Just q -> q
      symbol_name = "formula_" ++ show index ++ case mp of Nothing -> "" ; Just q -> "_" ++ show q
      file_name = dir ++ "/" ++ symbol_name ++ ".c"
      source = "#define et " ++ symbol_name ++ "\n" ++ compile index p human_name [(p, f)]
  writeFile file_name source

compile :: Int -> Int -> String -> [(Int, R2Formula (Expr String))] -> String
compile typ powr name fs =
  let header = "#include \"formula.h\"\n"
      refe = reference False typ powr fs
      refd = reference True  typ powr fs
      pert = perturbation False typ powr fs
      perd = perturbation True  typ powr fs
      per1 = period_tri fs
      per2 = period_jsk fs
      newt = newton fs
      siz' = size fs
      ske' = skew fs
      --dsiz = domain_size fs
      degr = exp $ sum (map (log . fromIntegral . fst) fs) / fromIntegral (length fs) :: Double
      footer = "FORMULA(" ++ show name ++ ",\"(source code unknown)\"," ++ show (if isNaN degr || isInfinite degr then 0 else degr) ++ ")\n"
  in  header ++ refe ++ refd ++ pert ++ perd ++ per1 ++ per2 ++ newt ++ siz' ++ ske' ++ footer
