{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE RecordWildCards #-}

#include "formula.h"

module Fractal.EscapeTime.Runtime.Loader.Raw where

import Foreign
import Foreign.C.Types

import Numeric.MPFR.Types (MPFR)
import Numeric.LongDouble (LongDouble)
--import Numeric.Compensated (Compensated)

type Plain t = CInt -> t -> Ptr t -> t -> t -> t -> t -> Ptr t -> Ptr CInt -> IO CInt
type WrapPlain t = F_PlainP t -> CInt -> Ptr t -> Ptr t -> Ptr t -> Ptr t -> Ptr t -> Ptr t -> Ptr t -> Ptr CInt -> IO CInt
type Reference t = CInt -> t -> t -> t -> Ptr MPFR -> Ptr MPFR -> Ptr t -> Ptr CInt -> IO CInt
--type WrapReferenceL = F_ReferenceL -> CInt -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr MPFR -> Ptr MPFR -> Ptr LongDouble -> Ptr CInt -> IO CInt
type Perturbation t = CInt -> CInt -> t -> Ptr t -> t -> t -> t -> t -> Ptr t -> Ptr t -> Ptr CInt -> IO CInt
--type WrapPerturbationL = F_PerturbationL -> CInt -> CInt -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr CInt -> IO CInt
type PeriodTri = CInt -> CDouble -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> Ptr CInt -> IO CInt
type PeriodJSK = CInt -> CDouble -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> Ptr CDouble -> Ptr CInt -> IO CInt
type Newton = CInt -> CInt -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr CInt -> IO CInt
type Misiurewicz = CInt -> CInt -> CInt -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr CInt -> IO CInt
type Size = CInt -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> Ptr CDouble -> Ptr CInt -> IO CInt
type Skew = CInt -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> CInt -> Ptr CDouble -> Ptr CInt -> IO CInt
type DomainSize = CInt -> CDouble -> CDouble -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> Ptr CInt -> IO CInt

type F_PlainP t = FunPtr (Plain t)
--type F_PlainF = FunPtr (Plain CFloat)
type F_Plain  = FunPtr (Plain CDouble)
type F_PlainL = FunPtr (Plain LongDouble)
--type F_PlainC = FunPtr (Plain (Compensated Double))
--type F_ReferenceF = FunPtr (Reference CFloat)
type F_Reference  = FunPtr (Reference CDouble)
--type F_ReferenceL = FunPtr (Reference LongDouble)
--type F_PerturbationF = FunPtr (Perturbation CFloat)
type F_Perturbation  = FunPtr (Perturbation CDouble)
--type F_PerturbationL = FunPtr (Perturbation LongDouble)
type F_PeriodTri = FunPtr PeriodTri
type F_PeriodJSK = FunPtr PeriodJSK
type F_Newton = FunPtr Newton
type F_Misiurewicz = FunPtr Misiurewicz
type F_Size = FunPtr Size
type F_Skew = FunPtr Skew
type F_DomainSize = FunPtr DomainSize

--foreign import ccall safe "dynamic" mkPlainF :: F_PlainF -> Plain CFloat
foreign import ccall safe "dynamic" mkPlain  :: F_Plain  -> Plain CDouble
foreign import ccall safe "formula.h et_wrap_plainl" mkPlainL :: WrapPlain LongDouble
--foreign import ccall safe "formula.h et_wrap_plainc" mkPlainC :: WrapPlain (Compensated Double)
--foreign import ccall safe "dynamic" mkReferenceF :: F_ReferenceF -> Reference CFloat
foreign import ccall safe "dynamic" mkReference  :: F_Reference  -> Reference CDouble
--foreign import ccall safe "formula.h et_wrap_referencel" mkReferenceL :: WrapReferenceL
--foreign import ccall safe "dynamic" mkPerturbationF :: F_PerturbationF -> Perturbation CFloat
foreign import ccall safe "dynamic" mkPerturbation  :: F_Perturbation  -> Perturbation CDouble
--foreign import ccall safe "formula.h et_wrap_perturbationl" mkPerturbationL :: WrapPerturbationL
foreign import ccall safe "dynamic" mkPeriodTri :: F_PeriodTri -> PeriodTri
foreign import ccall safe "dynamic" mkPeriodJSK :: F_PeriodJSK -> PeriodJSK
foreign import ccall safe "dynamic" mkNewton :: F_Newton -> Newton
foreign import ccall safe "dynamic" mkMisiurewicz :: F_Misiurewicz -> Misiurewicz
foreign import ccall safe "dynamic" mkSize   :: F_Size   -> Size
foreign import ccall safe "dynamic" mkSkew   :: F_Skew   -> Skew
foreign import ccall safe "dynamic" mkDomainSize   :: F_DomainSize   -> DomainSize

data S_Formula = S_Formula
  { magic :: CInt
  , ssize :: CInt
  , version :: CInt
  , name :: Ptr CChar
  , source :: Ptr CChar
  , degree :: CDouble
--  , plainf :: F_PlainF
  , plain  :: F_Plain
  , plainl :: F_PlainL
--  , plainc :: F_PlainC
--  , referencef :: F_ReferenceF
  , reference  :: F_Reference
--  , referencel :: F_ReferenceL
--  , perturbationf :: F_PerturbationF
  , perturbation  :: F_Perturbation
--  , perturbationl :: F_PerturbationL
  , period_tri :: F_PeriodTri
  , period_jsk :: F_PeriodJSK
  , newton :: F_Newton
  , misiurewicz :: F_Misiurewicz
  , size :: F_Size
  , skew :: F_Skew
  , domain_size :: F_DomainSize
  }

instance Storable S_Formula where

  sizeOf _ = (#size struct formula)

  alignment _ = (#alignment struct formula)

  peek p = do
    magic <- (#peek struct formula, magic) p
    ssize <- (#peek struct formula, ssize) p
    version <- (#peek struct formula, version) p
    name <- (#peek struct formula, name) p
    source <- (#peek struct formula, source) p
    degree <- (#peek struct formula, degree) p
--    plainf <- (#peek struct formula, plainf) p
    plain  <- (#peek struct formula, plain ) p
    plainl <- (#peek struct formula, plainl) p
--    plainc <- (#peek struct formula, plainc) p
--    referencef <- (#peek struct formula, referencef) p
    reference  <- (#peek struct formula, reference ) p
--    referencel <- (#peek struct formula, referencel) p
--    perturbationf <- (#peek struct formula, perturbationf) p
    perturbation  <- (#peek struct formula, perturbation ) p
--    perturbationl <- (#peek struct formula, perturbationl) p
    period_tri <- (#peek struct formula, period_tri) p
    period_jsk <- (#peek struct formula, period_jsk) p
    newton <- (#peek struct formula, newton) p
    misiurewicz <- (#peek struct formula, misiurewicz) p
    size <- (#peek struct formula, size) p
    skew <- (#peek struct formula, skew) p
    domain_size <- (#peek struct formula, domain_size) p
    return $ S_Formula{..}

  poke p S_Formula{..} = do
    (#poke struct formula, magic) p magic
    (#poke struct formula, ssize) p ssize
    (#poke struct formula, version) p version
    (#poke struct formula, name) p name
    (#poke struct formula, source) p source
    (#poke struct formula, degree) p degree
--    (#poke struct formula, plainf) p plainf
    (#poke struct formula, plain ) p plain
    (#poke struct formula, plainl) p plainl
--    (#poke struct formula, plainc) p plainc
--    (#poke struct formula, referencef) p referencef
    (#poke struct formula, reference ) p reference
--    (#poke struct formula, referencel) p referencel
--    (#poke struct formula, perturbationf) p perturbationf
    (#poke struct formula, perturbation ) p perturbation 
--    (#poke struct formula, perturbationl) p perturbationl
    (#poke struct formula, period_tri) p period_tri
    (#poke struct formula, period_jsk) p period_jsk
    (#poke struct formula, newton) p newton
    (#poke struct formula, misiurewicz) p misiurewicz
    (#poke struct formula, size) p size
    (#poke struct formula, skew) p skew
    (#poke struct formula, domain_size) p domain_size
