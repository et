{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.PeriodTri (period_tri) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

period_tri :: [(Int, R2Formula (Expr String))] -> String
period_tri fs = runCompile_ FDouble . function NInt "period_tri" [(NInt, "N"), (NFloat, "R2"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NReal, "H"), (NVolatileIntPtr, "RUNNING")] $ \i -> do
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  n <- int
  r2 <- float
  a <- real
  b <- real
  h <- real
  n .= EVar NInt "N"
  r2 .= EVar NFloat "R2"
  let d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
  a .= EVar NReal "A"
  b .= EVar NReal "B"
  h .= EVar NReal "H"
  a0 <- real
  b0 <- real
  a1 <- real
  b1 <- real
  a2 <- real
  b2 <- real
  a0 .= a - h
  b0 .= b - h
  a1 .= a + h
  b1 .= b - h
  a2 .= a
  b2 .= b + h
  x0 <- real
  y0 <- real
  x1 <- real
  y1 <- real
  x2 <- real
  y2 <- real
  x0 .= 0
  y0 .= 0
  x1 .= 0
  y1 .= 0
  x2 .= 0
  y2 .= 0
  xf <- float
  yf <- float
  z2 <- float
  i .= 0
  z2 .= 0
  p <- bool
  p .= 1
  k <- int
  k .= 0
  running <- bool
  running .= read (EVar NVolatileIntPtr "RUNNING") 0
  while_ (ELess i n `EAnd` ELess z2 r2 `EAnd` p `EAnd` running) $ do
    -- allocate next
    xn <- real
    yn <- real
    z2 .= 0
    -- do formula
    let g (_, f) j kn = (,) j $ do
          let R2 f0x f0y = f (R2 d_ e_) (R2 a0 b0) (R2 x0 y0)
              R2 f1x f1y = f (R2 d_ e_) (R2 a1 b1) (R2 x1 y1)
              R2 f2x f2y = f (R2 d_ e_) (R2 a2 b2) (R2 x2 y2)
          xn .= optimize f0x
          yn .= optimize f0y
          x0 .= xn
          y0 .= yn
          xf .= xn
          yf .= yn
          z2 .= z2 + xf*xf + yf*yf
          xn .= optimize f1x
          yn .= optimize f1y
          x1 .= xn
          y1 .= yn
          xf .= xn
          yf .= yn
          z2 .= z2 + xf*xf + yf*yf
          xn .= optimize f2x
          yn .= optimize f2y
          x2 .= xn
          y2 .= yn
          xf .= xn
          yf .= yn
          z2 .= z2 + xf*xf + yf*yf
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- check period
    p01 <- int
    p12 <- int
    p20 <- int
    let crosses_axis x y u v = EIf (signum y `EEqual` signum v) 0 $
          let a' = u - x
              b' = v - y
              s = signum b'
              t = signum (b' * x - a' * y)
          in  s `EEqual` t
    p01 .= crosses_axis x0 y0 x1 y1
    p12 .= crosses_axis x1 y1 x2 y2
    p20 .= crosses_axis x2 y2 x0 y0
    ps <- int
    ps .= p01 + p12 + p20
    p .= EEqual ps 0 `EOr` EEqual ps 2
    -- update loop
    i .= i + 1
    running .= read (EVar NVolatileIntPtr "RUNNING") 0
  _ <- if_ (EEqual i n `EOr` ELessEqual r2 z2 `EOr` p) (i .= -1) (return ())
  return_ i

