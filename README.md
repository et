# et -- escape-time fractals

Escape-time fractals powered by a formula compiler.  Graphics are visualized
using distance estimation, normalized iteration count, and/or atom-domain
colouring.  Navigation is enhanced by Newton-Raphson zooming: a single action
can bring you to a mini-set or an embedded Julia.

<https://mathr.co.uk/et>

## Quick Start

### Prerequisites

    sudo apt-get install \
      build-essential \
      ghc \
      cabal-install \
      libmpfr-dev \
      libgirepository1.0-dev \
      libwebkit2gtk-4.0-dev \
      libgtksourceview-3.0-dev

### Build

    cabal v2-install et.cabal --overwrite-policy=always

### Run

    et-gtk
    et-cli
    et-kf < kf/formulas.et /path/to/output

## et-gtk

### Mouse Controls

- Left

  Zoom in.

- Right

  Zoom out.

- Middle

  Center view.

- Scroll

  Zoom.

### Keyboard Controls

- Escape

  Stop rendering.

- F5

  Restart rendering.

- 1 2 3 4 5 6 7 8

  Set supersampling to NxN.

- 9 0

  Adjust colouring weight.

- - =

  Adjust maximum iteration count.

- s

  Save timestamped image to ~/Pictures/et/ or similar.

- m

  Zoom to mini-set and auto-skew.

- j

  Zoom to embedded Julia set and auto-skew.

- k l

  Auto-skew.

- u

  Reset skew.

- , .

  Rotate.

- ← ↓ → ↑

  Translate by large amounts.

- 4 2 6 8 (numeric keypad)

  Translate by small amounts.

- PageUp PageDown

  Zoom by large amounts.

- - + (numeric keypad)

  Zoom by small amounts.

- Home

  Reset view.

## Legal

et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
