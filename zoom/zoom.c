/*
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

this file is based on extra/zoom.c from:

mightymandel -- GPU-based Mandelbrot Set explorer
Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
License GPL3+ http://www.gnu.org/licenses/gpl.html
*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

// image sizes
static int IWIDTH = 0;
static int IHEIGHT = 0;
static int OWIDTH = 0;
static int OHEIGHT = 0;
static int WWIDTH = 0;
static int WHEIGHT = 0;

// speed of zooming
static int IFRAMES = 0;
static int OFPS = 25;
static double OLENGTH = 0;
static double increment = 0;

// motion blur
static double shutter = 0.0;
static int motion_count = 1;

// image buffers
static float *ibuffer[2] = { 0, 0 };
static unsigned char *ybuffer = 0;

static char oheader[1024];

// variables
static double phase = 0;
static int which = 0;

// textures
enum texid {
  texi11a = 0,
  texi11b = 1,
  tex11a,
  texo,
  // total number of textures
  texCount
};
static GLuint tex[texCount];

// frame buffer
static GLuint fbo = 0;
static GLuint fbo2 = 0;

static int combine_prog  = 0;
static int combine_tex0  = 0;
static int combine_tex1  = 0;
static int combine_phase = 0;
static int combine_alpha = 0;

static int draw_prog     = 0;
static int draw_tex      = 0;
static int draw_inverse  = 0;

// read raw image data
static int read_image(void) {
  return 1 == fread(ibuffer[which], IHEIGHT * IWIDTH * sizeof(float), 1, stdin);
}

// upload an image to the GPU
static void upload_image(int t, int w, int h) {
  glActiveTexture(GL_TEXTURE0 + t);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED, GL_FLOAT, ibuffer[which]);
}

// download Y data from the GPU
static void download_y(void) {
  glActiveTexture(GL_TEXTURE0 + texo);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_UNSIGNED_BYTE, ybuffer);
//  glReadPixels(0, 0, OWIDTH, OHEIGHT, GL_RED, GL_UNSIGNED_BYTE, ybuffer);
}

// write planar Y
static int write_image(void) {
  if (1 != fwrite("FRAME\n", 6, 1, stdout)) { return 0; }
  if (1 != fwrite(ybuffer, OWIDTH * OHEIGHT    , 1, stdout)) { return 0; }
  return 1;
}

static const char *combine_frag_src =
"#version 130\n"
"uniform sampler2D tex0;\n"
"uniform sampler2D tex1;\n"
"uniform float phase;\n"
"uniform float alpha;\n"
"\n"
"void main() {\n"
"  vec2 de = vec2(texture(tex0, 0.5 * (gl_TexCoord[0].xy - vec2(0.5)) + vec2(0.5)).r, texture(tex1, gl_TexCoord[0].xy).r);\n"
"  de *= pow(vec2(2.0), vec2(phase + 1.0, phase));\n"
"  vec2 g = tanh(clamp(vec2(4.0) + log2(max(de, vec2(1e-10))), 0.0, 8.0));\n"
"  float y = mix(g.x, g.y, phase);\n"
"  gl_FragColor = vec4(vec3(y), alpha);\n"
"}\n"
;

static const char *draw_frag_src =
"#version 130\n"
"uniform sampler2D tex;\n"
"uniform bool inverse;\n"
"\n"
"void main() {\n"
"  vec4 c = texture(tex, gl_TexCoord[0].xy);\n"
"  gl_FragColor = inverse ? vec4(vec3(1.0) - c.rgb, c.a) : c;\n"
"}\n"
;

// display callback
static void display(void) {
  int w = IWIDTH;
  int h = IHEIGHT;
  int motion = 0;
  glViewport(0, 0, w, h);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex[tex11a], 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgramObjectARB(combine_prog);
  for (motion = 0; motion < motion_count; ++motion) {
    double motion_alpha = 1.0 / (motion + 1.0);
    while (phase >= 1) {
      // read next image
      if (read_image()) {
        upload_image(which, w, h);
        which = 1 - which;
        phase -= 1;
      } else {
        fprintf(stderr, "couldn't read image\n");
        exit(0);
      }
    }
    if (motion <= shutter * motion_count) {
      // scale and blend
      glUniform1iARB(combine_tex0, which);
      glUniform1iARB(combine_tex1, 1 - which);
      glUniform1fARB(combine_phase, phase);
      glUniform1fARB(combine_alpha, motion_alpha);
      double x = 0.5*pow(0.5, phase);
      double y = 0.5*pow(0.5, phase);
      glBegin(GL_QUADS); {
        glTexCoord2f(0.5 - x, 0.5 + y); glVertex2f(0, 1);
        glTexCoord2f(0.5 + x, 0.5 + y); glVertex2f(1, 1);
        glTexCoord2f(0.5 + x, 0.5 - y); glVertex2f(1, 0);
        glTexCoord2f(0.5 - x, 0.5 - y); glVertex2f(0, 0);
      } glEnd();  
    }
    // advance
    phase += increment / motion_count;
  }
  glUseProgramObjectARB(0);
  // download output
  glActiveTexture(GL_TEXTURE0 + tex11a);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glViewport(0, 0, OWIDTH, OHEIGHT);
  glUseProgramObjectARB(draw_prog);
  glBegin(GL_QUADS); {
    glTexCoord2f(0, 1); glVertex2f(0, 1);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
  } glEnd();  
  glUseProgramObjectARB(0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  download_y();
  write_image();
  // display current output
  glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo2);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glViewport(0, 0, WWIDTH, WHEIGHT);
  glClear(GL_COLOR_BUFFER_BIT);
  glBlitFramebuffer(0, 0, OWIDTH, OHEIGHT, 0, 0, WWIDTH, WHEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);
  glutSwapBuffers();
  glutReportErrors();
}

// allocate an RGB texture
static void textureb(int tid, int w, int h, int r) {
  glActiveTexture(GL_TEXTURE0 + tid);
  glBindTexture(GL_TEXTURE_2D, tex[tid]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, r ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

// initialize a shader
static int shader(const char *src) {
  int p = glCreateProgramObjectARB();
#if 0
  int v = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
  glShaderSourceARB(v, 1, (const GLcharARB **) &shader_vert_src, 0);
  glCompileShaderARB(v);
  glAttachObjectARB(p, v);
#endif
  int f = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(f, 1, (const GLcharARB **) &src, 0);
  glCompileShaderARB(f);
  glAttachObjectARB(p, f);
  glLinkProgramARB(p);
  int success;
  glGetObjectParameterivARB(p, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (!success) { fprintf(stderr, "failed to compile shader\n%s\n", src); exit(1); }
  return p;
}

// GLUT timer callback
static void timerf(int x) {
  glutTimerFunc(10, timerf, x+1);
  glutPostRedisplay();
}

// main program
extern int main(int argc, char **argv) {
  if (! (argc >= 5)) {
    fprintf(stderr, "usage: %s iwidth iheight iframes olength [ofps [oshutter [header [inverse]]]] < stream.ppm > stream.y4m\n", argv[0]);
    return 1;
  }
  IWIDTH = atoi(argv[1]);
  IHEIGHT = atoi(argv[2]);
  IFRAMES = atoi(argv[3]);
  OWIDTH = 1920;
  OHEIGHT = 1080;
  OLENGTH = atof(argv[4]);
  OFPS = 25;
  if (argc >= 6) {
    OFPS = atoi(argv[5]);
  }
  if (argc >= 7) {
    shutter = fmin(fmax(atof(argv[6]), 0.0), 1.0);
  }
  int header = 1;
  if (argc >= 8) {
    header = atoi(argv[7]);
  }
  int inverse = 0;
  if (argc >= 9) {
    inverse = atoi(argv[8]);
  }
  increment = (IFRAMES - 1.0) / (OLENGTH * OFPS);
  /*
  zoom0 = 0.5 ** phase
  zoom1 = 0.5 ** (phase + increment / motion_count)
  texcoord = (width/2 + width/2 * zoom , height/2 + height/2 * zoom)
  assume phase = 0
  texcoord0 = (width, height)
  texcoord1 = (width, height) * (1 + 0.5 ** (increment / motion_count)) / 2
  texcoord1 - texcoord2 = (width, height) * [(1 + 0.5 ** (increment / motion_count)) / 2 - 1]
  | tex1 - tex2 | = sqrt(w^2+h^2) * [(1 + 0.5 ** (inc / mot)) / 2 - 1]
  | tex1 - tex2 | == 1  for smooth motion blur
  2*[1/sqrt(w^2+h^2) + 1] - 1 = 0.5 ** (inc / mot)
  2/sqrt(w^2 + h^2) + 1 = 0.5**(inc/mot)
  log (2 / sqrt (w^2 + h^2) + 1) = (inc / mot) log 0.5
  mot = inc * log 0.5 / log (2 / sqrt(w^2 + h^2) + 1)
  */
  motion_count = fmax(1.0, ceil(fabs(increment * log(0.5) / log(2.0 / sqrt(OWIDTH * OWIDTH + OHEIGHT * OHEIGHT) + 1.0))));
  fprintf(stderr, "zoom: motion_count = %d (%d)\n", motion_count, (int) ceil(shutter * motion_count));
  ibuffer[0] = calloc(1, IWIDTH * IHEIGHT * sizeof(float));
  ibuffer[1] = calloc(1, IWIDTH * IHEIGHT * sizeof(float));
  ybuffer = malloc(OWIDTH * OHEIGHT);
  snprintf(oheader, 1024, "YUV4MPEG2 W%d H%d F%d:1 Ip A1:1 Cmono\n", OWIDTH, OHEIGHT, OFPS);
  // initialize
  double aspect = OWIDTH / (double) OHEIGHT;
  WWIDTH = 640;
  WHEIGHT = 640 / aspect;
  glutInitWindowSize(WWIDTH, WHEIGHT);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("zoom");
  glewInit();
  // shaders
  combine_prog  = shader(combine_frag_src);
  combine_tex0  = glGetUniformLocationARB(combine_prog, "tex0");
  combine_tex1  = glGetUniformLocationARB(combine_prog, "tex1");
  combine_phase = glGetUniformLocationARB(combine_prog, "phase");
  combine_alpha = glGetUniformLocationARB(combine_prog, "alpha");
  draw_prog     = shader(draw_frag_src);
  draw_tex      = glGetUniformLocationARB(draw_prog, "tex");
  draw_inverse  = glGetUniformLocationARB(draw_prog, "inverse");
  glUseProgramObjectARB(draw_prog);
  glUniform1iARB(draw_tex, tex11a);
  glUniform1iARB(draw_inverse, inverse);
  glUseProgramObjectARB(0);
  // universal view
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  // textures
  glGenTextures(texCount, tex);
  int w = IWIDTH;
  int h = IHEIGHT;
  textureb(texi11a, w * 1, h * 1, 0);
  textureb(texi11b, w * 1, h * 1, 0);
  textureb(tex11a, w * 1, h * 1, 1);
  textureb(texo, OWIDTH, OHEIGHT, 0);
  // frame buffer
  glGenFramebuffers(1, &fbo);
  glGenFramebuffers(1, &fbo2);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex[texo], 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  // initialize state
  which = 0;
  phase = 2;
  // write output header
  if (header)
  {
    if (1 != fwrite(oheader, strlen(oheader), 1, stdout)) { fprintf(stderr, "failed to write header\n"); return 1; }
  }
  // start processing frames
  glutDisplayFunc(display);
  glutTimerFunc(10, timerf, 0);
  glutReportErrors();
  glutMainLoop();
  return 0;
}
