# et notes

## gerrit's glitch condition for R2 formulas

gerrit "Re: Perturbation theory" (Reply #102, Last Edit: 2017-12-27, 04:56:09)
<https://fractalforums.org/fractal-mathematics-and-new-theories/28/perturbation-theory/487/msg3212#msg3212>

Here's a general analysis, to implement you'll have to get the actual formulas
for the whatever fractal and plug them in.

Write the perturbed iteration as

    w←f(w,X,c)

where the actual orbit is z+w, X is reference orbit, and c is the delta c.
w, c, X are now just real 2-vectors so there is no advantage to think in terms
of complex numbers. Using index notation:

    wi(n+1)=fi(w,X,c)

where iteration is denoted by (n+1), and if absent it's understood to be at
iteration n. w, X, c with no indices are vectors, so w=(w1,w2). Any f without
argument is supposed to be at w,X,c.
Define 3 2X2 matrices  in index notation:

    Mij=∂fi/∂cj
    Kij=∂fi/∂Xj
    Lij=∂fi/∂wj

Error in wi(n+1) is obtained by replacing every variable pi with pi+|pi|ϵ, and
linearizing in ϵ. I think the error in c is not relevant but I'll keep it just
in case.

The shift (error) in w(n+1) is now

    Δwi(n+1)=(|Lij||wj|+|Kij||Xj|+|Mij||cj|)ϵ  (1)

where I use Einstein summation convention, meaning you have to sum over every
index that is repeated in a product (so qiri is shorthand for ∑2i=1piqi).

The relation between a shift in w and a shift in c is

    Δwi(n+1)=Jij(n+1)Δcj, with Jij=∂wi/∂cj, so
    Δci=(J(n+1))−1ijΔwj(n+1) (2)

Error condition is now just |Δc|<h.

For any particular formula you have to work out formulas for all these
matrices, reducing eq (2) to iteration n using the iteration formula,
substitute (1) in (2) and you'll get a condition.

Hope I got everything right.

Edit: I don't think you need advice on implementation issues, but my approach
would be to decide on a canonical form for f(), applicable to all 50 fractals,
then use some symbolic software to generate the Jacobians, inverses,
substitutions, and so on.

If you could make this "online" so you can enter you own formula, that would be
probably considered great (Ultra Fractal style), though I personally don't care
about all these "artificial" fractals.

## realflow100's hybrid technique

Loop with some Mandlebrot and some Burning Ship or other.  Suggested syntax
for formula compiler:

    z := z^2 + c
    z := (|x| + i |y|)^2 + c
    z := z^2 + c
    z := z^2 + c
