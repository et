{-
et -- escape time fractals
Copyright (C) 2018,2019,2020,2021 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module ET where

import Data.List (group, groupBy, sort)
import Data.Function (on)

type Literal = Integer

type Variable = String

data Expr
  = Lit Literal
  | Var Variable
  | Neg Expr
  | Add Expr Expr
  | Mul Expr Expr
  | Abs Expr
  | Sgn Expr
  deriving (Eq, Ord, Read, Show)

z  = Var "z"
c  = Var "c"
zz = Var "Z"
cc = Var "C"
s  = Var "s"
a = Var "a"
b = Var "b"
x = Var "x"
y = Var "y"
aa = Var "A"
bb = Var "B"
xx = Var "X"
yy = Var "Y"
i = Var "i"

pretty :: Expr -> String
pretty = p False
  where
   p _ (Lit l) = show l
   p _ (Var v) = v
   p False (Add e (Neg f)) = p False e ++ "-" ++ p False f
   p True (Add e (Neg f)) = "(" ++ p False e ++ "-" ++ p False f ++ ")"
   p False (Add e f) = p False e ++ "+" ++ p False f
   p True (Add e f) = "(" ++ p False e ++ "+" ++ p False f ++ ")"
   p _ (Mul e f) = p True e ++ "*" ++ p True f
   p _ (Neg e) = "-" ++ p True e

instance Num Expr where
  fromInteger i = Lit (fromInteger i)
  negate (Lit l) = Lit (negate l)
  negate e = Neg e
  e + f = Add e f
  e - f = Add e (negate f)
  e * f = Mul e f
  abs = error "abs :: Expr -> Expr: not implemented"
  signum = error "signum :: Expr -> Expr: not implemented"

substitute :: [(Variable, Expr)] -> Expr -> Expr
substitute vars v@(Var s) = case lookup s vars of
  Just e -> e
  Nothing -> v
substitute _ l@(Lit _) = l
substitute vars (Neg e) = Neg (substitute vars e)
substitute vars (Add e f) = Add (substitute vars e) (substitute vars f)
substitute vars (Mul e f) = Mul (substitute vars e) (substitute vars f)

data Product = Product{ variables :: [[Variable]], literal :: Literal }
  deriving (Eq, Read, Show)

instance Ord Product where
  compare (Product uss a) (Product vss b) = compare (concat uss, a) (concat vss, b)

product_ vs l = Product (group . sort . concat $ vs) l

data Sum = Sum [Product]
  deriving (Eq, Ord, Read, Show)

sum_ ps = Sum (sort ps)

expr :: Sum -> Expr
expr (Sum ps) = sum1 (map expr_p ps)

sum1 [] = 0
sum1 xs = foldr1 (+) xs

expr_p :: Product -> Expr
expr_p (Product _ 0) = 0
expr_p (Product vss 1) = product1 . map Var . concat $ vss
expr_p (Product vss l) = (Lit l *) . product1 . map Var . concat $ vss

product1 [] = 1
product1 xs = foldr1 (*) xs

sum_of_products :: Expr -> Sum
sum_of_products (Lit l) = sum_ [product_ [] l]
sum_of_products (Var v) = sum_ [product_ [[v]] 1]
sum_of_products (Neg e) = case sum_of_products e of
  Sum ps -> Sum (map negate_p ps)
    where negate_p (Product vs l) = product_ vs (negate l)
sum_of_products (Add e f) = case (sum_of_products e, sum_of_products f) of
  (Sum es, Sum fs) -> sum_ (es ++ fs)
sum_of_products (Mul e f) = case (sum_of_products e, sum_of_products f) of
  (Sum es, Sum fs) -> sum_ [ mul_p e' f' | e' <- es, f' <- fs ]
    where mul_p (Product us a) (Product vs b) = product_ (us ++ vs) (a * b)

cancelled :: Sum -> Sum
cancelled (Sum ps)
  = sum_
      [ product_ vs l
      | p@(Product vs _ : _) <- groupBy ((==) `on` variables) ps
      , let l = sum (map literal p)
      , l /= 0
      ]

cancel :: Variable -> Sum -> Maybe Sum
cancel v (Sum ss) = sum_ <$> mapM (cancel_p v) ss

cancel_p :: Variable -> Product -> Maybe Product
cancel_p v (Product vss l) = do
    us <- go (concat vss)
    pure (product_ [us] l)
  where go (u:us)
          | u == v = Just us
          | otherwise = (u :) <$> go us
        go [] = Nothing

complex :: Variable -> Sum -> Sum
complex i (Sum ss) = sum_ (map (complex_p i) ss)

complex_p :: Variable -> Product -> Product
complex_p i (Product vss l) =
  product_
    [(if odd n then (i :) else id) (filter (i /=) (concat vss))]
    ((if odd (n `div` 2) then negate else id) l)
  where
    n = length (filter (i ==) (concat vss))

re, im :: Variable -> Sum -> Sum
re i (Sum ss) = sum_ (filter (real i) ss)
im i (Sum ss) = case cancel "i" (sum_ (filter (imaginary i) ss)) of
  Nothing -> sum_ []
  Just s -> s

real, imaginary :: Variable -> Product -> Bool
real i = even . length . filter (i ==) . concat . variables
imaginary i = odd . length . filter (i ==) . concat . variables

fC = z^3 + c
gC = perturbC fC
hC = rescaleC gC

wideC = substitute [ ("z", zz + z), ("c", cc + c) ]
bigC = substitute [ ("z", zz), ("c", cc) ]
perturbC f = expr . cancelled . sum_of_products $ (wideC f - bigC f)
scaleC = substitute [ ("z", z * s), ("c", c * s) ]
rescaleC = fmap expr . cancel "s" . sum_of_products . scaleC

fR2 = (x + i * y)^3 + (a + i * b)
gR2 = perturbR2 fR2
hR2 = rescaleR2 gR2

exprR2 s = expr (re "i" s) + i * expr (im "i" s)

wideR2 = substitute [ ("x", xx + x), ("y", yy + y), ("a", aa + a), ("b", bb + b) ]
bigR2 = substitute [ ("x", xx), ("y", yy), ("a", aa), ("b", bb) ]
perturbR2 f = exprR2 . cancelled . complex "i" . sum_of_products $ (wideR2 f - bigR2 f)
scaleR2 = substitute [ ("x", x * s), ("y", y * s), ("a", a * s), ("b", b * s) ]
rescaleR2 = fmap exprR2 . cancel "s" . sum_of_products . scaleR2

main = do
  print (pretty fC)
  print (pretty gC)
  print (pretty <$> hC)
  print (pretty fR2)
  print (pretty gR2)
  print (pretty <$> hR2)
