{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2
  ( module Fractal.EscapeTime.Algorithms.R2.Plain
  , module Fractal.EscapeTime.Algorithms.R2.Reference
  , module Fractal.EscapeTime.Algorithms.R2.Perturbation
  , module Fractal.EscapeTime.Algorithms.R2.PeriodTri
  , module Fractal.EscapeTime.Algorithms.R2.PeriodJSK
  , module Fractal.EscapeTime.Algorithms.R2.Newton
  , module Fractal.EscapeTime.Algorithms.R2.Misiurewicz
  , module Fractal.EscapeTime.Algorithms.R2.Size
  , module Fractal.EscapeTime.Algorithms.R2.Skew
  , module Fractal.EscapeTime.Algorithms.R2.DomainSize
  ) where

import Fractal.EscapeTime.Algorithms.R2.Plain
import Fractal.EscapeTime.Algorithms.R2.Reference
import Fractal.EscapeTime.Algorithms.R2.Perturbation
import Fractal.EscapeTime.Algorithms.R2.PeriodTri
import Fractal.EscapeTime.Algorithms.R2.PeriodJSK
import Fractal.EscapeTime.Algorithms.R2.Newton
import Fractal.EscapeTime.Algorithms.R2.Misiurewicz
import Fractal.EscapeTime.Algorithms.R2.Size
import Fractal.EscapeTime.Algorithms.R2.Skew
import Fractal.EscapeTime.Algorithms.R2.DomainSize
