{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render
  ( module Fractal.EscapeTime.Runtime.Render.View
  , module Fractal.EscapeTime.Runtime.Render.Buffer
  , render
  ) where

import Data.Vector.Storable.Mutable (set)
--import Numeric.Compensated (Compensated)
import Numeric.LongDouble (LongDouble)
import Numeric.Rounded.Simple (exponent')

import Fractal.EscapeTime.Runtime.Render.View
import Fractal.EscapeTime.Runtime.Render.Buffer
import Fractal.EscapeTime.Runtime.Render.Progressive
import Fractal.EscapeTime.Runtime.Render.Dither (defaultDither)
import Fractal.EscapeTime.Runtime.Render.Transform (Transform)
import Fractal.EscapeTime.Runtime.Render.Coord (coord, unCoord)
import qualified Fractal.EscapeTime.Runtime.Render.Plain as Plain
import qualified Fractal.EscapeTime.Runtime.Render.Perturb as Perturb
import Fractal.EscapeTime.Runtime.Loader (ET)

render :: ET -> Double -> Int -> Double -> Double -> View -> Buffer Float -> Transform -> Maybe Progressive -> IO (IO Bool, IO Bool)
render et er maxiters d e v@(View _ _ r) b@(Buffer _ h _ c) tr prog = do
  set c (-1)
  case exponent' r - ceiling (logBase 2 (fromIntegral h :: Double)) of
    e
--      | e >    -21 -> print "f" >> Plain.render   (realToFrac er :: Float             ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither) prog
      | e >    -50 -> print "d" >> Plain.render   (realToFrac er :: Double            ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither) prog
      | e >    -61 -> print "l" >> Plain.render   (realToFrac er :: LongDouble        ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither) prog
--      | e >   -103 -> print "c" >> Plain.render   (realToFrac er :: Compensated Double) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither) prog
--      | e >   -120 -> print "pf" >> Perturb.render (realToFrac er :: Float            ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither, unCoord) prog
      | e >  -1020 -> print "pd" >> Perturb.render (realToFrac er :: Double           ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither, unCoord) prog
--      | e > -16380 -> print "pl" >> Perturb.render (realToFrac er :: LongDouble       ) et maxiters (realToFrac d) (realToFrac e) v b tr (coord defaultDither, unCoord) prog
    _ -> return (return False, return False)
