{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.CodeGen.OpCode
  ( Op0(..), op0
  , Op1(..), op1
  , Op2(..), op2
  , Op3(..), op3
  , op_lit, op_litf
  , op_write
  )
  where

import Control.Monad.RWS (ask)

import Fractal.EscapeTime.Compiler.CodeGen.Core
import Fractal.EscapeTime.Compiler.Expr.AST

typname :: NType -> CompileM String
typname NBool = return "b"
typname NInt = return "i"
typname NIntPtr = return "I"
typname NVolatileIntPtr = return "V"
typname NReal = return "r"
typname NFloat = do
  t <- ask
  return $ case t of
    FSingle -> "f"
    FDouble -> "d"
    FLongDouble -> "l"
    FCompensated -> "c"
    FFloatExp -> "e"
typname NFloatPtr = do
  t <- ask
  return $ case t of
    FSingle -> "F"
    FDouble -> "D"
    FLongDouble -> "L"
    FCompensated -> "C"
    FFloatExp -> "E"

data Op0
  = O'zero
  | O'inf
  | O'ninf
  | O'nan
  | O'pi
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

op0name :: NType -> Op0 -> CompileM String
op0name to op = do
  to' <- typname to
  return $ to' ++ "_" ++ drop 2 (show op)

op0 :: Op0 -> Expr String -> CompileM ()
op0 op (EVar to v) = do
  name <- op0name to op
  emit $ name ++ "(" ++ v ++ ");\n"

data Op1
  = O'set
  | O'abs
  | O'sgn
  | O'neg
  | O'inv
  | O'mul2
  | O'sqr
  | O'sqrt
  | O'exp
  | O'log
  | O'expm1
  | O'log1p
  | O'sin
  | O'cos
  | O'tan
  | O'sinh
  | O'cosh
  | O'tanh
  | O'asin
  | O'acos
  | O'atan
  | O'asinh
  | O'acosh
  | O'atanh
  | O'floor
  | O'not
  | O'comp
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

op1name :: NType -> Op1 -> NType -> CompileM String
op1name to op from = do
  to' <- typname to
  from' <- typname from
  return $ to' ++ "_" ++ drop 2 (show op) ++ "_" ++ from'

op1 :: Op1 -> Expr String -> Expr String -> CompileM ()
op1 op (EVar to v) (EVar from x) = do
  name <- op1name to op from
  emit $ name ++ "(" ++ v ++ "," ++ x ++ ");\n"

data Op2
  = O'add
  | O'sub
  | O'mul
  | O'div
  | O'diffabs
  | O'lt
  | O'le
  | O'gt
  | O'ge
  | O'eq
  | O'ne
  | O'and
  | O'or
  | O'xor
  | O'shl
  | O'shr
  | O'read
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

op2name :: NType -> Op2 -> NType -> NType -> CompileM String
op2name to op l r = do
  to' <- typname to
  l' <- typname l
  r' <- typname r
  return $ to' ++ "_" ++ drop 2 (show op) ++ "_" ++ l' ++ r'

op2 :: Op2 -> Expr String -> Expr String -> Expr String -> CompileM ()
op2 op (EVar to v) (EVar l x) (EVar r y) = do
  name <- op2name to op l r
  emit $ name ++ "(" ++ v ++ "," ++ x ++ "," ++ y ++ ");\n"

data Op3
  = O'ifte
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

op3name :: NType -> Op3 -> NType -> NType -> NType -> CompileM String
op3name to op l r c = do
  to' <- typname to
  l' <- typname l
  r' <- typname r
  c' <- typname c
  return $ to' ++ "_" ++ drop 2 (show op) ++ "_" ++ l' ++ r' ++ c'

op3 :: Op3 -> Expr String -> Expr String -> Expr String -> Expr String -> CompileM ()
op3 op (EVar to v) (EVar l x) (EVar r y) (EVar k z) = do
  name <- op3name to op l r k
  emit $ name ++ "(" ++ v ++ "," ++ x ++ "," ++ y ++ "," ++ z ++ ");\n"

op_lit :: Expr String -> Integer -> CompileM ()
op_lit (EVar NReal v) n = emit $ "mpfr_set_si(" ++ v ++ "," ++ show n ++ ",MPFR_RNDN);\n"
op_lit (EVar NInt v) n = emit $ v ++ "=" ++ show n ++ ";\n"
op_lit (EVar NBool v) n = emit $ v ++ "=" ++ show n ++ ";\n"
op_lit (EVar NFloat v) n = do
  t <- ask
  case t of
    FCompensated -> emit $ "c_set_i(" ++ v ++ "," ++ show n ++ ")" ++ ";\n"
    FFloatExp -> emit $ "e_set_i(" ++ v ++ "," ++ show n ++ ")" ++ ";\n"
    _ -> emit $ v ++ "=" ++ show n ++ ";\n"
op_lit v n = error $ show v ++ " = " ++ show n

op_litf :: Expr String -> Rational -> CompileM ()
op_litf (EVar NReal v) f = emit $ "mpfr_set_d(" ++ v ++ "," ++ show (fromRational f :: Double) ++ ",MPFR_RNDN);\n"
op_litf (EVar NFloat v) f = do
  t <- ask
  case t of
    FCompensated -> emit $ "c_set_d(" ++ v ++ "," ++ show (fromRational f :: Double) ++ ")" ++ ";\n"
    FFloatExp -> emit $ "e_set_d(" ++ v ++ "," ++ show (fromRational f :: Double) ++ ");\n"
    _ -> emit $ v ++ "=" ++ show (fromRational f :: Double) ++ ";\n"

op_write :: Expr String -> Expr String -> Expr String -> CompileM ()
op_write (EVar vt v) (EVar it i) (EVar xt x) = do
  vt' <- typname vt
  it' <- typname it
  xt' <- typname xt
  emit $ "v_write_" ++ vt' ++ it' ++ xt' ++ "(" ++ v ++ "," ++ i ++ "," ++ x ++ ");\n"

op_pi :: Expr String -> CompileM ()
op_pi (EVar NReal v) = emit $ "mpfr_const_pi(" ++ v ++ ",MPFR_RNDN);\n"
op_pi (EVar _     v) = emit $ v ++ "=3.141592653589793238462643383279502884L;\n"
