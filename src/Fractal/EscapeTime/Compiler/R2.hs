{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.R2 (compile) where

import Fractal.EscapeTime.Compiler.Expr.AST ( Expr() )
import Fractal.EscapeTime.Formulas.Types ( R2Formula )
import Fractal.EscapeTime.Algorithms.R2

compile :: [(Int, R2Formula (Expr String))] -> String
compile fs =
  let header = "#include \"formula.h\"\n"
      plai = plain fs
      refe = reference fs
      pert = perturbation fs
      per1 = period_tri fs
      per2 = period_jsk fs
      newt = newton fs
      misi = misiurewicz fs
      siz' = size fs
      ske' = skew fs
      dsiz = domain_size fs
      degr = exp $ sum (map (log . fromIntegral . fst) fs) / fromIntegral (length fs) :: Double
      name = "(unknown)"
      source = "(unknown)"
      footer = "FORMULA(" ++ show name ++ "," ++ show source ++ "," ++ show (if isNaN degr || isInfinite degr then 0 else degr) ++ ")\n"
  in  header ++ plai ++ refe ++ pert ++ per1 ++ per2 ++ newt ++ misi ++ siz' ++ ske' ++ dsiz ++ footer
