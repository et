{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE DataKinds #-}

module Fractal.EscapeTime.Runtime.Render.View where

import System.IO.Unsafe (unsafePerformIO)
import Numeric.Rounded
import qualified Numeric.Rounded.Simple as S

data View = View{ centerX, centerY, radius :: S.Rounded }

readViewIO :: String -> String -> String -> IO View
readViewIO sx sy sr = do
  r <- readIO sr
  let prec = max 53 $ 53 - exponent r
  reifyPrecision prec $ \p -> do
    let readViewIO2 :: Precision p => Rounded TowardNearest p -> Rounded TowardNearest p -> Rounded TowardNearest Double -> proxy p -> IO View
        readViewIO2 x' y' r' _ = return $ View (S.simplify x') (S.simplify y') (S.simplify r')
    x <- readIO sx
    y <- readIO sy
    readViewIO2 x y r p

defaultView :: View
defaultView = unsafePerformIO $ readViewIO "0" "0" "2"
