{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Formulas.Types where

data R2 a = R2 a a deriving (Read, Show, Eq, Ord)

instance (Eq a, Num a) => Num (R2 a) where
  fromInteger n = R2 (fromInteger n) 0
  R2 a b + R2 x y = R2 (a + x) (b + y)
  R2 a b - R2 x y = R2 (a - x) (b - y)
  R2 a b * R2 x y = R2 (a * x - b * y) (a * y + b * x)
  negate (R2 a b) = R2 (negate a) (negate b)
  abs (R2 x _) = R2 (abs x) 0
--  abs _ = error "Fractal.EscapeTime.Formulas.Types.R2.Num.abs: not real"
  signum (R2 x _) = R2 (signum x) 0
--  signum _ = error "Fractal.EscapeTime.Formulas.Types.R2.Num.signum: not real"

instance (Eq a, Fractional a) => Fractional (R2 a) where
  fromRational n = R2 (fromRational n) 0
  c / z = let R2 u v = c * conj z ; w = norm z in R2 (u / w) (v / w)

instance (Eq a, Floating a) => Floating (R2 a) where
  pi = R2 pi 0
  exp (R2 a b) = let r = exp a ; c = cos b ; s = sin b in R2 (r * c) (r * s)
  log z = R2 (log (norm z) / 2) (arg z)

  sqrt z@(R2 x y) = R2 (sqrt $ (magz + x) / 2) (signum' y * (sqrt $ (magz - x) / 2))
    where magz = mag z ; signum' 0 = 1 ; signum' w = signum w

  sin (R2 x y) = R2 (sin x * cosh y) ( cos x * sinh y)
  cos (R2 x y) = R2 (cos x * cosh y) (-sin x * sinh y)

  sinh (R2 x y) = R2 (sinh x * cos y) (cosh x * sin y)
  cosh (R2 x y) = R2 (cosh x * cos y) (sinh x * sin y)

  asin z = -i * log (i * z + sqrt (1 - z * z))
  acos z = -i * log (z + i * sqrt (1 - z * z))
  atan z = (log (1 + i * z) - log (1 - i * z)) / (2 * i)

  asinh z = log (z + sqrt (z * z + 1))
  acosh z = 2 * log (sqrt ((z + 1)/2) + sqrt ((z - 1)/2))
  atanh z = log ((1 + z) / (1 - z)) / 2

  -- FIXME expm1, log1p, ...

i :: Num a => R2 a
i = R2 0 1

conj :: Num a => R2 a -> R2 a
conj (R2 a b) = R2 a (negate b)

norm :: Num a => R2 a -> a
norm (R2 x y) = x * x + y * y

mag :: Floating a => R2 a -> a
mag = sqrt . norm

arg :: (Eq a, Floating a) => R2 a -> a
arg 0 = 0
arg (R2 x 0) = (pi - signum x * pi) / 2
arg (R2 x y) = 2 * atan (y / (sqrt (x * x + y * y) + x))

type R2Formula a = R2 a -> R2 a -> R2 a -> R2 a
