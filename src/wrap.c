/*
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "formula.h"

extern int et_wrap_plainl(f_plainl*z,int a,long double*b,long double*c,long double*d,long double*e,long double*d_,long double*e_,long double*f,volatile int*g)
{
  return z(a,*b,c,*d,*e,*d_,*e_,f,g);
}

/*
extern int et_wrap_plainc(f_plainc*z,int a,compensated*b,compensated*c,compensated*d,compensated*e,compensated*d_,compensated*e_,compensated*f,volatile int*g)
{
  return z(a,*b,c,*d,*e,*d_,*e_,f,g);
}

extern int et_wrap_referencel(f_referencel*z,int a,long double*b,long double*c_,long double*d_,mpfr_t c,mpfr_t d,long double*e,volatile int*f)
{
  return z(a,*b,*c_,*d_,c,d,e,f);
}

extern int et_wrap_perturbationl(f_perturbationl*z,int a,int b,long double*c,long double*d,long double*e,long double*f,long double*e_,long double*f_,long double*g,long double*h,volatile int*i)
{
  return z(a,b,*c,d,*e,*f,*e_,*f_,g,h,i);
}
*/
