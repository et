#include <complex.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fftw3.h>
#define wisdomfile "/run/shm/et-metric.fftw"

#if 1
static int cmp_alpha(const void *a, const void *b)
{
  const float *p = a;
  const float *q = b;
  float x = *p;
  float y = *q;
  return (x > y) - (y > x);
}
#endif

static int cmp_image(const void *a, const void *b)
{
  const uint8_t *p = a;
  const uint8_t *q = b;
  int x = *p;
  int y = *q;
  return (x > y) - (y > x);
}

static float linear(float srgb)
{
  float s = srgb / 255.0f;
  float l;
  if (s < 0.04045f)
  {
    l = s / 12.92f;
  }
  else
  {
    l = powf((s + 0.055f) / 1.055f, 2.4f);
  }
  return l;
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int boxes = 4;
  while (! feof(stdin))
  {
    int64_t width, height;
    if (2 != fscanf(stdin, "P5\n%ld %ld\n255", &width, &height))
    {
      if (feof(stdin))
      {
        break;
      }
      fprintf(stderr, "ERROR: failed to parse PGM header from standard input\n");
      return 1;
    }
    if (fgetc(stdin) != '\n')
    {
      fprintf(stderr, "ERROR: junk after PGM header\n");
      return 1;
    }
    uint8_t *image = malloc(width * height);
    if (! image)
    {
      fprintf(stderr, "ERROR: failed to allocate memory for PGM image data\n");
      return 1;
    }
    if (1 != fread(image, width * height, 1, stdin))
    {
      fprintf(stderr, "ERROR: failed to read PGM image data\n");
      free(image);
      return 1;
    }

#if 0
    float _Complex *ibuf = fftwf_alloc_complex(width * height);
    if (! ibuf)
    {
      fprintf(stderr, "ERROR: failed to allocate FFT input buffer\n");
      free(image);
      return 1;
    }
    float _Complex *obuf = fftwf_alloc_complex(width * height);
    if (! obuf)
    {
      fprintf(stderr, "ERROR: failed to allocate FFT output buffer\n");
      fftwf_free(ibuf);
      free(image);
      return 1;
    }
    fftwf_import_wisdom_from_filename(wisdomfile);
    fftwf_plan plan = fftwf_plan_dft_2d(width, height, ibuf, obuf, FFTW_FORWARD, FFTW_DESTROY_INPUT | FFTW_EXHAUSTIVE);
    fftwf_export_wisdom_to_filename(wisdomfile);
    float windowj[height];
    float windowi[width];
    #pragma omp parallel for schedule(static)
    for (int64_t j = 0; j < height; ++j)
    {
      windowj[j] = (1 - cos(2 * 3.141592653589793 / height * (j + 0.5)));
    }
    #pragma omp parallel for schedule(static)
    for (int64_t i = 0; i < width; ++i)
    {
      windowi[i] = (1 - cos(2 * 3.141592653589793 / width * (i + 0.5)));
    }
    #pragma omp parallel for schedule(static)
    for (int64_t j = 0; j < height; ++j)
    {
      for (int64_t i = 0; i < width; ++i)
      {
        int64_t k = j * width + i;
        ibuf[k] = windowi[i] * windowj[j] * (image[k] / 255.0f - 0.5f);
      }
    }
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_free(ibuf);
    int rBins = 6;
    int tBins = 24;
    double trHistogram[tBins][rBins];
    memset(trHistogram, 0, sizeof(trHistogram));
    float Rx = width/2;
    float Ry = height/2;
    float R = sqrt(Rx * Rx + Ry * Ry);
    float rFactor = rBins / log(R);
    double Esum = 0;
    #pragma omp parallel for schedule(static) reduction(+:Esum)
    for (int64_t j = 0; j < height; ++j)
    {
      float fy = j > height/2 ? j - height : j;
      for (int64_t i = 0; i < width; ++i)
      {
        float fx = i > width/2 ? i - width : i;
        float r = sqrtf(fx * fx + fy * fy);
        float t = atan2f(fy, fx) / 6.283185307179586; t -= floorf(t);
        float _Complex s = obuf[j * width + i];
        float x = creal(s);
        float y = cimag(s);
        float E = sqrtf(x * x + y * y);
        int rBin = fminf(fmaxf(floorf(logf(r + 1.0e-30f) * rFactor), 0), rBins - 1);
        int tBin = fminf(fmaxf(floorf(t * tBins), 0), tBins - 1);
        if (r > 0)
        {
          #pragma omp atomic
          trHistogram[tBin][rBin] += E;
        }
        Esum += r > 0 ? E : 0;
      }
    }
    fftwf_free(obuf);
#if 0
    // earth movers distance
    double rEMDs = 0;
    for (int j = 0; j < tBins; ++j)
    {
      double rEMD = 0;
      for (int i = 0; i < rBins; ++i)
      {
        rEMD += trHistogram[j][i] - trHistogram[(j+(tBins/4))%tBins][i];
        rEMDs += fabs(rEMD);
      }
    }
    double isotropy = 1.0 / (1 + rEMDs / Esum);
#endif
    // deviation from flatness
    double Eavg = Esum / (tBins * rBins);
    double isotropy = 0;
    for (int t = 0; t < tBins; ++t)
    {
      for (int r = 0; r < rBins; ++r)
      {
        double b = trHistogram[t][r] - Eavg;
        isotropy += b * b;
      }
    }
    isotropy /= tBins * rBins;
    isotropy = sqrt(isotropy);
    isotropy /= width * height;
    isotropy = 1 / (1 + isotropy);
#endif

#if 1
  int ndirs = 36; // 5deg accuracy
  double directions[ndirs];
  memset(directions, 0, sizeof(directions));
  static int kernel[5][5] =
{ {  -5,  -4,  0,   4,   5 }
, {  -8, -10,  0,  10,   8 }
, { -10, -20,  0,  20,  10 }
, {  -8, -10,  0,  10,   8 }
, {  -5,  -4,  0,   4,   5 }
};

  double gsum = 0;
  #pragma omp parallel for schedule(static) reduction(+:gsum)
  for (int64_t j = 2; j < height - 2; ++j)
  {
    for (int64_t i = 2; i < width - 2; ++i)
    {
      int mx = 0;
      int my = 0;
      for (int q = -2; q <= 2; ++q)
      {
        for (int p = -2; p <= 2; ++p)
        {
          int m = image[(j + q) * width + (i + p)];
          mx += kernel[2 + q][2 + p] * m;
          my += kernel[2 + p][2 + q] * m;
        }
      }
      float gx = mx * (1.0f / (255.f * 20.f));
      float gy = my * (1.0f / (255.f * 20.f));
      float g = gx * gx + gy * gy;
      gsum += g;
      float gdir = atan2f(gy, gx) / 3.141592653589793f;
      gdir -= floorf(gdir);
      int dir = gdir * ndirs;
      #pragma omp atomic
      directions[dir] += g;
    }
  }
  for (int dir = 0; dir < ndirs; ++dir)
    directions[dir] /= gsum;
  double directionality = 0;
  for (int dir = 0; dir < ndirs; ++dir)
  {
    double d = directions[dir] - 1.0 / ndirs;
    directionality += d * d;
  }
#endif


#if 1
    //
    float *alpha = malloc(sizeof(float) * width * height);
    if (! alpha)
    {
      fprintf(stderr, "ERROR: failed to allocate memory for dimension data\n");
      free(image);
      return 1;
    }
#endif

    double sum = 0;
    double black = 0;
    double white = 0;
    #pragma omp parallel for schedule(static) reduction(+:black) reduction(+:white) reduction(+:sum)
    for (int64_t i = 0; i < width * height; ++i)
    {
      sum += linear(image[i]);
      black += (image[i] < 4);
      white += (image[i] > 251);
    }
    double mean = sum / (width * height);
    black /= (width * height);
    white /= (width * height);
    double gray = 1 - black - white;

#if 1
    #pragma omp parallel for schedule(static)
    for (int64_t j = 0; j < height; ++j)
    {
      for (int64_t i = 0; i < width; ++i)
      {
        // compute local fractal dimension by simple linear regression
        // finds slope of log/log graph of box size vs contained measure
        // note: this is likely sRGB, perhaps it should be linearized?
        double x[boxes], y[boxes];
        for (int box = 0; box < boxes; ++box)
        {
          int box_radius = (2 << box) - 1;
          double measure = 0;
          int count = 0;
          for (int64_t jj = j - box_radius; jj <= j + box_radius; ++jj)
          {
            if (jj < 0) continue;
            if (jj >= height) continue;
            for (int64_t ii = i - box_radius; ii <= i + box_radius; ++ii)
            {
              if (ii < 0) continue;
              if (ii >= width) continue;
              measure += image[jj * width + ii];
              count += 1;
            }
          }
          x[box] = log2(count);
          y[box] = log2(measure + count); // prevent log(0)
        }
        double sx = 0, sy = 0;
        for (int box = 0; box < boxes; ++box)
        {
          sx += x[box];
          sy += y[box];
        }
        sx /= boxes;
        sy /= boxes;
        double cov = 0, var = 0;
        for (int box = 0; box < boxes; ++box)
        {
          cov += (x[box] - sx) * (y[box] - sy);
          var += (x[box] - sx) * (x[box] - sx);
        }
        double beta = cov / var;
        double dimension = fmin(fmax(fabs(beta), 0), 2);
        alpha[j * width + i] = dimension;
      }
    }
#endif

    // compute deviation from flat greyscale histogram
    qsort(image, width * height, sizeof(uint8_t), cmp_image);
    double gray_deviation = 0;
    #pragma omp parallel for schedule(static) reduction(+:gray_deviation)
    for (int64_t i = 0; i < width * height; ++i)
    {
      double ideal = (i + 0.5) / (width * height);
      double actual = linear(image[i]);
      double dev = actual - ideal;
      gray_deviation += dev * dev;
    }
    gray_deviation /= width * height;
    gray_deviation = sqrt(gray_deviation);
    free(image);

#if 1
    // compute width of central 90th percentile excluding 0 2
    qsort(alpha, width * height, sizeof(float), cmp_alpha);
    int64_t lo0 = 0;
    for (int64_t i = 0; i < width * height; ++i)
    {
      if (alpha[i] == alpha[0]) lo0 = i; else break;
    }
    int64_t hi2 = width * height - 1;
    for (int64_t i = width * height - 1; i >= 0; --i)
    {
      if (alpha[i] == alpha[width * height - 1]) hi2 = i; else break;
    }
    int64_t lo = lo0 + 0.05 * (hi2 - lo0);
    int64_t hi = lo0 + 0.95 * (hi2 - lo0);
    double alpha_width = alpha[hi] - alpha[lo];
    double beta_width = (hi2 - lo0) / (double) (width * height);
    double gamma = 1 / (1 + 3 * 4 * pow(mean - 0.5, 2));
    free(alpha);
#endif

//    double score = /*alpha_width * beta_width * gamma */ (1 - gray_deviation) * (1 - black) * (1 - white) * (1 - directionality);// * isotropy;//pow(alpha_width * beta_width * gamma, 0.25) * pow(1 - gray_deviation, 2) * pow(1 - black, 2) * pow(1 - white, 2) * pow(isotropy, 4);
    double score = alpha_width * (1 - black) * (1 - white) * (1 - gray_deviation) * (1 - directionality);
#if 0
    fprintf(stderr, "alpha   \t%e\n", alpha_width);
    fprintf(stderr, "beta    \t%e\n", beta_width);
    fprintf(stderr, "gamma   \t%e\n", gamma);
    fprintf(stderr, "black   \t%e\n", 1 - black);
    fprintf(stderr, "white   \t%e\n", 1 - white);
    fprintf(stderr, "gray    \t%e\n", 1 - gray_deviation);
#endif
//    fprintf(stderr, "isotropy\t%e\n", isotropy);
    fprintf(stdout, "%.7e\n", score);
  }
  return 0;
}
