{-
et -- escape time fractals
Copyright (C) 2018,2019,2020,2021 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Fractal.EscapeTime.Runtime.Render.Plain where

import Control.Exception (Exception, catch, throwIO)
import Control.Monad (when)
import Foreign
import GHC.Conc (getNumCapabilities)
import Control.Concurrent.Async (async, wait, replicateConcurrently_)
import Data.Atomics.Counter (newCounter, incrCounter)
import Numeric.LongDouble (LongDouble)
--import Numeric.Compensated (Compensated)
import Numeric.Rounded (RoundingMode(TowardNearest), Precision)
import qualified Numeric.Rounded as R
import Numeric.Rounded.Simple (Rounded, reifyRounded)
import qualified Data.Vector.Storable as S
import qualified Data.Vector.Storable.Mutable as SM
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM

import qualified Fractal.EscapeTime.Runtime.Loader as ET
import Fractal.EscapeTime.Runtime.Loader (ET)
import Fractal.EscapeTime.Runtime.Render.View
import Fractal.EscapeTime.Runtime.Render.Buffer
import Fractal.EscapeTime.Runtime.Render.Coord
import Fractal.EscapeTime.Runtime.Render.Transform
import Fractal.EscapeTime.Runtime.Render.Progressive

class (Storable t, RealFrac t) => Plain t where plain :: ET -> ET.Plain t
--instance Plain Float  where plain = ET.plainf
instance Plain Double where plain = ET.plain
instance Plain LongDouble where plain = ET.plainl
--instance Plain (Compensated Double) where plain = ET.plainc

render :: Plain t => t -> ET -> Int -> t -> t -> View -> Buffer Float -> Transform -> Coord t -> Maybe Progressive -> IO (IO Bool, IO Bool)
render er et maxiters d_ e_ (View x0hi y0hi r0hi) buf@(Buffer w h 4 _) tr coord' mprog = do
  runningP <- malloc
  poke runningP 1
  a <- async $ Control.Exception.catch (do
    let x0 = recodeFloat x0hi `asTypeOf` er
        y0 = recodeFloat y0hi
        r0 = recodeFloat r0hi
        px = 2 * r0 / fromIntegral h
        (aa, ab, ba, bb) = getTransform tr
        pxt = (px * realToFrac aa, px * realToFrac ab, px * realToFrac ba, px * realToFrac bb)
        count = w * h
    done <- SM.replicate count False
    npixelR <- newCounter 0
    threads <- getNumCapabilities
    let worker = do
          npixel <- subtract 1 `fmap` incrCounter 1 npixelR
          when (npixel < count) $ do
            let Pixel (i, j, _, _) = case mprog of
                                 Just prog -> prog S.! npixel
                                 Nothing -> case npixel `divMod` w of (j, i) -> Pixel (fromIntegral i, fromIntegral j, 0, 0)
                (a0, b0) = coord' tr px w h (fromIntegral i) (fromIntegral j)
                a = a0 + x0
                b = b0 + y0
            m <- plain et maxiters (er * er) pxt d_ e_ a b runningP
            case m of
              Nothing -> throwIO Cancelled
              Just (de, p, ni, nf) -> output mprog done buf npixel True (realToFrac de, realToFrac p, realToFrac ni, realToFrac nf)
            worker
    replicateConcurrently_ threads worker
    return True
    ) (\Cancelled -> return False)
  return  ( do{ poke runningP 0 ; r <- wait a ; free runningP ; return r }
          , do{                   r <- wait a ; free runningP ; return r }
          )
render _ _ _ _ _ _ _ _ _ _ = return (return False, return False)

data Cancelled = Cancelled deriving Show
instance Exception Cancelled

recodeFloat :: forall t . Fractional t => Rounded -> t
recodeFloat a = reifyRounded a (realToFrac :: forall k (p :: k) . Precision p => R.Rounded TowardNearest p -> t)
