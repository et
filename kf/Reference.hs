{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Reference (reference) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types hiding (i)

fts :: [FType]
fts = [FDouble,FLongDouble,FFloatExp]

reference :: Bool -> Int -> Int -> [(Int, R2Formula (Expr String))] -> String
reference derivs typ powr fs@[(_, f)] = concat . flip map fts $ \t -> runCompile_ t . function NInt ("reference" ++ (if derivs then "D" else "") ++ fSuffix t)
  ([(NInt, "m_nFractalType"), (NInt, "m_nPower")
  , (NFloatPtr, "m_db_dxr"), (NFloatPtr, "m_db_dxi"), (NFloatPtr, "m_db_z")
  , (NVolatileIntPtr, "m_bStop"), (NIntPtr, "m_nRDone")
  , (NIntPtr, "m_nGlitchIter"), (NIntPtr, "m_nMaxIter")
  , (NReal, "Cr0"), (NReal, "Ci0")
  , (NFloat, "g_SeedR"), (NFloat, "g_SeedI")
  , (NFloat, "g_FactorAR"), (NFloat, "g_FactorAI")
  , (NFloat, "terminate"), (NFloat, "g_real"), (NFloat, "g_imag")
  , (NInt, "m_bGlitchLowTolerance")
  , (NIntPtr, "antal"), (NFloatPtr, "test1"), (NFloatPtr, "test2")
  ] ++ if derivs then [ (NFloatPtr, "dzc"), (NFloatPtr, "dci") ] else []) $ \ret -> do
    early "mpfr_prec_t prec=mpfr_get_prec(Cr0);\n"
    let m_nFractalType = EVar NInt "m_nFractalType"
        m_nPower = EVar NInt "m_nPower"
        m_db_dxr = EVar NFloatPtr "m_db_dxr"
        m_db_dxi = EVar NFloatPtr "m_db_dxi"
        m_db_z = EVar NFloatPtr "m_db_z"
        m_bStop = EVar NVolatileIntPtr "m_bStop"
        m_nRDone = EVar NVolatileIntPtr "m_nRDone"
        m_nGlitchIter = EVar NVolatileIntPtr "m_nGlitchIter"
        m_nMaxIter = EVar NVolatileIntPtr "m_nMaxIter"
        cr0 = EVar NReal "Cr0"
        ci0 = EVar NReal "Ci0"
        g_SeedR = EVar NFloat "g_SeedR"
        g_SeedI = EVar NFloat "g_SeedI"
        d_@_g_FactorAR = EVar NFloat "g_FactorAR"
        e_@_g_FactorAI = EVar NFloat "g_FactorAI"
        terminate = EVar NFloat "terminate"
        g_real = EVar NFloat "g_real"
        g_imag = EVar NFloat "g_imag"
        m_bGlitchLowTolerance = EVar NInt "m_bGlitchLowTolerance"
        antal = EVar NVolatileIntPtr "antal"
        test1 = EVar NFloatPtr "test1"
        test2 = EVar NFloatPtr "test2"
        dzc = EVar NFloatPtr "dzc"
        dci = EVar NFloatPtr "dci"
    _ <- if_ (EEqual m_nFractalType (fromIntegral typ) `EAnd` EEqual m_nPower (fromIntegral powr)) (do
      stored <- int
      stored .= 0
      old_absval <- float
      old_absval .= 0
      abs_val <- float
      abs_val .= 0
      nMaxIter <- int
      nMaxIter .= read m_nMaxIter 0
      writeI m_nGlitchIter 0 (nMaxIter + 1)
      glitch <- float
      glitch .= realToFrac (0.1 ** (([7,7,7,6,5,4,4,3,3] ++ repeat 2) !! powr))
      _ <- if_ (m_bGlitchLowTolerance) (glitch .= sqrt glitch) (return ())
      a@cr <- real
      cr .= cr0
      b@ci <- real
      ci .= ci0
      x@xr <- real
      xr .= g_SeedR
      y@xi <- real
      xi .= g_SeedI
      xrd <- float
      xrd .= xr
      xid <- float
      xid .= xi
      xrn <- real
      xin <- real
      mderivs <- if not derivs then return Nothing else do
        daa <- float
        dab <- float
        dba <- float
        dbb <- float
        daa .= read dci 0
        dab .= read dci 1
        dba .= read dci 2
        dbb .= read dci 3
        dxa <- float
        dxb <- float
        dya <- float
        dyb <- float
        dxa .= 0
        dxb .= 0
        dya .= 0
        dyb .= 0
        dxan <- float
        dxbn <- float
        dyan <- float
        dybn <- float
        let af = EVar NFloat "af_unused"
            bf = EVar NFloat "bf_unused"
        return (Just (daa, dab, dba, dbb, dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, af, bf))
      i <- int
      i .= 0
      k <- int
      k .= 0

      while_ (ELess i nMaxIter `EAnd` EEqual 0 (read m_bStop 0)) $ do
          -- do formula
          let R2 fx0 fy0 = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize fx0
              fy = optimize fy0
          comment $ "fx   = " ++ pretty fx
          comment $ "fy   = " ++ pretty fy
          xrn .= fx
          xin .= fy
          case mderivs of
            Nothing -> return ()
            Just (daa, dab, dba, dbb, dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, af, bf) -> do
              let R2 ffx0 ffy0 = f (R2 d_ e_) (R2 af bf) (R2 xrd xid)
                  ffx = optimize ffx0
                  ffy = optimize ffy0
                  fdxa = optimize $ ddx d af ffx
                  fdxb = optimize $ ddx d bf ffx
                  fdya = optimize $ ddx d af ffy
                  fdyb = optimize $ ddx d bf ffy
                  d v u
                    | u == xrd && v == af = dxa
                    | u == xrd && v == bf = dxb
                    | u == xid && v == af = dya
                    | u == xid && v == bf = dyb
                    | u == af  && v == af = daa
                    | u == af  && v == bf = dab
                    | u == bf  && v == af = dba
                    | u == bf  && v == bf = dbb
                    | u == v = 1
                    | otherwise = 0
              comment $ "fdxa = " ++ pretty fdxa
              comment $ "fdxb = " ++ pretty fdxb
              comment $ "fdya = " ++ pretty fdya
              comment $ "fdyb = " ++ pretty fdyb
              dxan .= fdxa
              dxbn .= fdxb
              dyan .= fdya
              dybn .= fdyb
          -- continue loop
          xr .= xrn
          xi .= xin
          case mderivs of
            Nothing -> return ()
            Just (_daa, _dab, _dba, _dbb, dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, _af, _bf) -> do
              dxa .= dxan
              dxb .= dxbn
              dya .= dyan
              dyb .= dybn
          writeI m_nRDone 0 (read m_nRDone 0 + 1)
          xrd .= xr
          xid .= xi
          old_absval .= abs_val
          abs_val .= g_real * xrd * xrd + g_imag * xid * xid
          xz <- float
          xz .= abs_val * glitch
          write m_db_dxr i xrd
          write m_db_dxi i xid
          write m_db_z   i xz
          _ <- if_ (4 `ELessEqual` abs_val)
            (if_ (EEqual 4 terminate `EAnd` EEqual stored 0)
              (do
                stored .= 1
                writeI antal 0 i
                write test1 0 abs_val
                write test2 0 old_absval
              ) (return ())
            ) (return ())
          _ <- if_ (terminate `ELessEqual` abs_val)
            (do
              _ <- if_ (ELess 4 terminate `EAnd` EEqual stored 0)
                  (do
                    stored .= 1
                    writeI antal 0 i
                    write test1 0 abs_val
                    write test2 0 old_absval
                  ) (return ())
              _ <- if_ (nMaxIter `EEqual` read m_nMaxIter 0)
                  (do
                    nMaxIter .= i + 3
                    _ <- if_ (read m_nMaxIter 0 `ELess` nMaxIter) (nMaxIter .= read m_nMaxIter 0) (return ())
                    writeI m_nGlitchIter 0 nMaxIter
                  ) (return ())
              return ()
            ) (return ())
          i .= i + 1
      case mderivs of
        Nothing -> return ()
        Just (_daa, _dab, _dba, _dbb, dxa, dxb, dya, dyb, _dxan, _dxbn, _dyan, _dybn, _af, _bf) -> do
          dr <- float
          di <- float
          sqrttest1 <- float
          sqrttest1 .= sqrt (read test1 0)
          dr .= (xrd * dxa + xid * dya) / sqrttest1;
          di .= (xrd * dxb + xid * dyb) / sqrttest1;
          write dzc 0 dr
          write dzc 1 di
      ret .= 1) (ret .= 0)
    return_ ret
