{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}

module Fractal.EscapeTime.Compiler.CodeGen.Core where

import Prelude hiding (init)
import Data.List (delete)

import Control.Monad.RWS

import Fractal.EscapeTime.Compiler.Expr.AST

data FType = FSingle | FDouble | FLongDouble | FCompensated | FFloatExp
  deriving (Read, Show, Eq, Ord, Enum, Bounded)

fTypes :: [FType]
fTypes = [FSingle, FDouble, FLongDouble, FCompensated {-, FFloatExp -}]

fSuffix :: FType -> String
fSuffix FSingle = "f"
fSuffix FDouble = ""
fSuffix FLongDouble = "l"
fSuffix FCompensated = "c"
fSuffix FFloatExp = "fe"

type CompileM a = RWS FType (String, String, String) ([String], [[(NType, String)]]) a

early, late, emit :: String -> CompileM ()
early s = tell (s, "" , "")
late s = tell ("", "", s)
emit s = tell ("", s, "")

typeName :: NType -> CompileM String
typeName NBool = return "int" -- C99 stdbool.h has no binding in Foreign.C.Types in older base
typeName NInt = return "int"
typeName NFloat = do
  b <- ask
  return $ case b of
    FSingle -> "float"
    FDouble -> "double"
    FLongDouble -> "long double"
    FCompensated -> "compensated"
    FFloatExp -> "cfloatexp"
typeName NReal = return "mpfr_t"
typeName NFloatPtr = do
  t <- typeName NFloat
  return $ t ++ "*"
typeName NIntPtr = return "int*"
typeName NVolatileIntPtr = return "volatile int*"

declare :: NType -> String -> CompileM (Expr String)
declare t v = do
  s <- typeName t
  early $ s ++ " " ++ v ++ ";\n"
  return (EVar t v)

init :: Expr String -> CompileM ()
init (EVar NReal v) = early $ "mpfr_init2(" ++ v ++ ",prec);\n"
init _ = return ()

clear :: Expr String -> CompileM ()
clear (EVar NReal v) = late $ "mpfr_clear(" ++ v ++ ");\n"
clear _ = return ()

alloc :: NType -> CompileM (Expr String)
alloc t = do
  (~(next:nexts), ~(spare:spares)) <- get
  case lookup t spare of
    Nothing -> do
      put (nexts, spare:spares)
      v <- declare t next
      init v
      clear v
      return v
    Just u -> do
      put (next:nexts, delete (t, u) spare : spares)
      return (EVar t u)

free :: Expr String -> CompileM ()
free (EVar t u) = do
  (nexts, ~(spare:spares)) <- get
  put (nexts, ((t, u):spare):spares)
free _ = error "Fractal.EscapeTime.Compile.CodeGen.Core.free: not a variable"

bool, int, float, real, intPtr, floatPtr :: CompileM (Expr String)
bool = alloc NBool
int = alloc NInt
float = alloc NFloat
real = alloc NReal
intPtr = alloc NIntPtr
floatPtr = alloc NFloatPtr

scope :: CompileM a -> CompileM a
scope a = do
  early "{\n"
  r <- bareScope a
  late "}\n"
  return r

bareScope :: CompileM a -> CompileM a
bareScope a = do
  (nexts, spares) <- get
  put (nexts, []:spares)
  r <- a
  put (nexts,    spares)
  return r

comment :: String -> CompileM ()
comment s = emit $ "// " ++ s ++ "\n"

runCompile :: FType -> CompileM a -> (a, String)
runCompile t a = case runRWS a t (map (('v':) . show) [(0::Integer)..], []) of
  (r, (_, []), (u, v, w)) -> (r, u ++ v ++ w)
  _ -> error "Fractal.EscapeTime.Compiler.CodeGen.Core.runCompile: internal error: bad scope?"

runCompile_ :: FType -> CompileM () -> String
runCompile_ t a = case runCompile t a of ((), w) -> w
