{-
et -- escape time fractals
Copyright (C) 2018,2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE DeriveDataTypeable #-}
module Fractal.EscapeTime.Compiler.Expr.AST where

import Data.Data
import Data.Typeable

import Numeric

data NType = NBool | NInt | NFloat | NReal | NFloatPtr | NIntPtr | NVolatileIntPtr
  deriving (Read, Show, Eq, Ord, Enum, Bounded, Data, Typeable)

data Expr a
  -- these ones can be perturbed
  = EInt         Integer
  | ERat         Rational
  | EVar   NType a
  | ENeg   NType (Expr a)
  | EInv   NType (Expr a) -- FIXME handle integer division properly?
  | EAdd   NType (Expr a) (Expr a)
  | EMul   NType (Expr a) (Expr a)
  | EAbs   NType (Expr a)
  | ESin   NType (Expr a)
  | ECos   NType (Expr a)
  | ETan   NType (Expr a)
  | ESinh  NType (Expr a)
  | ECosh  NType (Expr a)
  | ETanh  NType (Expr a)
  | EPi    NType
  | EExp   NType (Expr a)
  | EExpM1 NType (Expr a)
  | ELog   NType (Expr a)
  | ELog1P NType (Expr a)
  -- these can be perturbed in some limited circumstances
  | ESqrt  NType (Expr a)
  -- these ones can't be perturbed
  | EFloat       (Expr a)
  | ESgn         (Expr a)
  | EASin  NType (Expr a)
  | EACos  NType (Expr a)
  | EATan  NType (Expr a)
  | EASinh NType (Expr a)
  | EACosh NType (Expr a)
  | EATanh NType (Expr a)
  | EFloor NType (Expr a)
  | ENot        (Expr a)
  | ELess       (Expr a) (Expr a)
  | ELessEqual  (Expr a) (Expr a)
  | EEqual      (Expr a) (Expr a)
  | EOr         (Expr a) (Expr a)
  | EAnd        (Expr a) (Expr a)
  | ERead       (Expr a) (Expr a)
  | EDiffAbs NType (Expr a) (Expr a)
  | EIf (Expr a) (Expr a) (Expr a)
  deriving (Read, Show, Eq, Ord, Data, Typeable)

interpret :: (Show v, Eq v, Floating a) => [(v, a)] -> Expr v -> Maybe a
-- constant
interpret _ (EInt n) = Just $ fromInteger n
interpret _ (ERat q) = Just $ fromRational q
interpret _ (EPi _) = Just pi
-- variable
interpret vs (EVar _ v) = lookup v vs
-- functor
interpret vs (EAdd _ e f) = (+) <$> interpret vs e <*> interpret vs f
interpret vs (EMul _ e f) = (*) <$> interpret vs e <*> interpret vs f
interpret vs (EInv _ e) = recip <$> interpret vs e
interpret vs (ENeg _ e) = negate <$> interpret vs e
interpret vs (EAbs _ e) = abs <$> interpret vs e
interpret vs (ESin _ e) = sin <$> interpret vs e
interpret vs (ECos _ e) = cos <$> interpret vs e
interpret vs (ETan _ e) = tan <$> interpret vs e
interpret vs (ESinh _ e) = sinh <$> interpret vs e
interpret vs (ECosh _ e) = cosh <$> interpret vs e
interpret vs (ETanh _ e) = tanh <$> interpret vs e
interpret vs (EASin _ e) = asin <$> interpret vs e
interpret vs (EACos _ e) = acos <$> interpret vs e
interpret vs (EATan _ e) = atan <$> interpret vs e
interpret vs (EASinh _ e) = asinh <$> interpret vs e
interpret vs (EACosh _ e) = acosh <$> interpret vs e
interpret vs (EATanh _ e) = atanh <$> interpret vs e
interpret vs (ESqrt _ e) = sqrt <$> interpret vs e
interpret vs (EExp _ e) = exp <$> interpret vs e
interpret vs (EExpM1 _ e) = expm1 <$> interpret vs e
interpret vs (ELog _ e) = log <$> interpret vs e
interpret vs (ELog1P _ e) = log1p <$> interpret vs e
interpret vs (EDiffAbs _ e f) = (\x y -> abs (x + y) - abs x) <$> interpret vs e <*> interpret vs f
interpret _ e = error (show e)
-- unimplemented
--interpret _ _ = Nothing

eType :: Expr a -> NType
eType (EInt    _  ) = NInt
eType (ERat    _  ) = NFloat
eType (EVar  t _  ) = t
eType (ENeg  t _  ) = t
eType (EInv  t _  ) = t
eType (EAdd  t _ _) = t
eType (EMul  t _ _) = t
eType (EAbs  t _  ) = t
eType (ESin  t _  ) = t
eType (ECos  t _  ) = t
eType (ETan  t _  ) = t
eType (ESinh t _  ) = t
eType (ECosh t _  ) = t
eType (ETanh t _  ) = t
eType (EPi   t    ) = t
eType (EExp t _ ) = t
eType (EExpM1 t _ ) = t
eType (ELog t _ ) = t
eType (ELog1P t _ ) = t
eType (EFloat _   ) = NFloat
eType (ESgn    _  ) = NInt
eType (ESqrt t _  ) = t
eType (EASin  t _ ) = t
eType (EACos  t _ ) = t
eType (EATan  t _ ) = t
eType (EASinh t _ ) = t
eType (EACosh t _ ) = t
eType (EATanh t _ ) = t
eType (EFloor t  _) = t
eType (ENot _) = NBool
eType (ELess   _ _) = NBool
eType (ELessEqual _ _) = NBool
eType (EEqual  _ _) = NBool
eType (EAnd    _ _) = NBool
eType (EOr     _ _) = NBool
eType (ERead (EVar NFloatPtr       _) _) = NFloat
eType (ERead (EVar NIntPtr         _) _) = NInt
eType (ERead (EVar NVolatileIntPtr _) _) = NInt
eType (ERead _ _) = error $ "Fractal.EscapeTime.Compiler.Expr.AST.eType: ERead not a pointer type"
eType (EDiffAbs t _ _) = t
eType (EIf _ t e) = eeType t e

eeType :: Expr a -> Expr a -> NType
eeType a b = case (eType a, eType b) of
  (NReal, _) -> NReal
  (_, NReal) -> NReal
  (NFloat, _) -> NFloat
  (_, NFloat) -> NFloat
  (NInt, _) -> NInt
  (_, NInt) -> NInt
  _ -> NBool -- FIXME

instance Num (Expr a) where
  fromInteger a = EInt a
  a + b = EAdd (eeType a b) a b
  a * b = EMul (eeType a b) a b
  negate a = ENeg (eType a) a
  abs a = EAbs (eType a) a
  signum a = ESgn a

instance Fractional (Expr a) where
  fromRational = ERat
  recip x = EInv (case eType x of
     NReal -> NReal
     _ -> NFloat) x

instance Floating (Expr a) where
  pi = EPi NReal
  exp x = EExp (eType x) x
  log x = ELog (eType x) x
  expm1 x = EExpM1 (eType x) x
  log1p x = ELog1P (eType x) x
  sqrt x = ESqrt (eType x) x
  sin x = ESin (eType x) x
  cos x = ECos (eType x) x
  tan x = ETan (eType x) x
  sinh x = ESinh (eType x) x
  cosh x = ECosh (eType x) x
  tanh x = ETanh (eType x) x
  asin x = EASin (eType x) x
  acos x = EACos (eType x) x
  atan x = EATan (eType x) x
  asinh x = EASinh (eType x) x
  acosh x = EACosh (eType x) x
  atanh x = EATanh (eType x) x

diffabs :: Expr a -> Expr a -> Expr a
diffabs a b = EDiffAbs (eeType a b) a b
