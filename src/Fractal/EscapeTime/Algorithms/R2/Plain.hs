{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.Plain (plain) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

-- FIXME this needs the degree of each stanza to be the *highest* non-linear power
plain :: [(Int, R2Formula (Expr String))] -> String
plain fs = concat . flip map [FDouble, FLongDouble] $ \t -> runCompile_ t . function NBool ("plain" ++ fSuffix t) [(NInt, "N"), (NFloat, "R2"), (NFloatPtr, "H"), (NFloat, "D"), (NFloat, "E"), (NFloat, "A"), (NFloat, "B"), (NFloatPtr, "OUT"), (NVolatileIntPtr, "RUNNING")] $ \running -> do
  let n = EVar NInt "N"
      r2 = EVar NFloat "R2"
      hp = EVar NFloatPtr "H"
      d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
      a = EVar NFloat "A"
      b = EVar NFloat "B"
      out = EVar NFloatPtr "OUT"
      runningP = EVar NVolatileIntPtr "RUNNING"
      logdegree :: Double
      logdegree = sum (map (log . fromIntegral . fst) fs) / fromIntegral (length fs)
  daa <- float
  dab <- float
  dba <- float
  dbb <- float
  daa .= read hp 0
  dab .= read hp 1
  dba .= read hp 2
  dbb .= read hp 3
  x <- float
  y <- float
  dxa <- float
  dxb <- float
  dya <- float
  dyb <- float
  x .= 0
  y .= 0
  dxa .= 0
  dxb .= 0
  dya .= 0
  dyb .= 0
  i <- int
  i .= 0
  z2 <- float
  z2 .= 0
  minz2 <- float
  minz2 .= r2
  period <- int
  period .= 0
  running .= read runningP 0
  k <- int
  k .= 0
  _ <- while_ (ELess (i + fromIntegral (length fs - 1)) n `EAnd` ELess z2 r2 `EAnd` running) $ do
    -- allocate next
    xn <- float
    yn <- float
    dxan <- float
    dxbn <- float
    dyan <- float
    dybn <- float
    -- do formula
    let g (_, f) j kn = (,) j $ do
          let R2 gx gy = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize gx
              fy = optimize gy
              fdxa = optimize $ ddx d a fx
              fdxb = optimize $ ddx d b fx
              fdya = optimize $ ddx d a fy
              fdyb = optimize $ ddx d b fy
              d v u
                | u == x && v == a = dxa
                | u == x && v == b = dxb
                | u == y && v == a = dya
                | u == y && v == b = dyb
                | u == a && v == a = daa
                | u == a && v == b = dab
                | u == b && v == a = dba
                | u == b && v == b = dbb
                | u == v = 1
                | otherwise = 0
          comment $ "fx   = " ++ pretty fx
          comment $ "fy   = " ++ pretty fy
          comment $ "fdxa = " ++ pretty fdxa
          comment $ "fdxb = " ++ pretty fdxb
          comment $ "fdya = " ++ pretty fdya
          comment $ "fdyb = " ++ pretty fdyb
          xn .= fx
          yn .= fy
          dxan .= fdxa
          dxbn .= fdxb
          dyan .= fdya
          dybn .= fdyb
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- update next
    x .= xn
    y .= yn
    dxa .= dxan
    dxb .= dxbn
    dya .= dyan
    dyb .= dybn
    -- update loop
    z2 .= x*x + y*y
    i .= i + 1
    _ <- if_ (ELess z2 minz2) (do{ minz2 .= z2 ; period .= i }) (return ())
    running .= read runningP 0
  de <- float
  nc <- float
  ni <- float
  nf <- float
  nc .= 0
  ni .= 0
  nf .= 0
  _ <- if_ (ELess z2 r2) (de .= 0 {- interior -}) $ do {- escaped -}
    u <- float
    v <- float
    u .= x * dxa + y * dya
    v .= x * dxb + y * dyb
    de .= z2 * log (sqrt z2) / sqrt (u*u + v*v)
    nc .= if isInfinite logdegree || isNaN logdegree || 0 == logdegree then i else i + 1 - log (log (sqrt z2)) / realToFrac logdegree
    ni .= EFloor NFloat nc
    nf .= nc - ni
  write out 0 de
  write out 1 period
  write out 2 ni
  write out 3 nf
  return_ running
