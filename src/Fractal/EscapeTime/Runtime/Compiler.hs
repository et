{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Compiler (compileAndLoad) where

import Prelude
import Control.Exception (catch, SomeException)
import System.Exit (ExitCode(..))
import Data.Text.Lazy (pack)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Data.Digest.Pure.MD5 (md5)
import System.FilePath ((</>))
import System.Process (readProcessWithExitCode)

import Fractal.EscapeTime.Compiler.Expr.AST (Expr(EVar), NType(NFloat))
import Fractal.EscapeTime.Compiler.Parser as R2
import qualified Fractal.EscapeTime.Compiler.R2 as R2
import Fractal.EscapeTime.Formulas.Types
import Fractal.EscapeTime.Runtime.Loader (ET, readLib)
import Fractal.EscapeTime.Runtime.Loader.Load (load, libext)
import Paths_et (getDataDir)

catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

libraryName :: Show a => FilePath -> a -> FilePath
libraryName dir = (dir </>) . (++ ("-et" ++ libext)) . show . md5 . encodeUtf8 . pack . show

invokeGCC :: String -> FilePath -> IO (ExitCode, String, String)
invokeGCC source target = do
  include <- getDataDir
  writeFile "/tmp/et.c" source
  readProcessWithExitCode "gcc"
    [ "-std=c99", "-Wall", "-Wextra", "-pedantic"
    , "-fPIC", "-O3", "-march=native", "-shared"
    , "-I" ++ include
    , "-o", target
    , "-xc", "-"
    , "-lmpfr"
    ] source

compileAndLoad :: FilePath -> String -> Int -> Int -> IO (Either String ET)
compileAndLoad dir input p q = catchAny (case R2.formulas input of
  Left e -> return (Left e)
  Right [] -> return (Left "no formula defined")
  Right (_:_:_) -> return (Left "more than one formula defined")
  Right [(_,_,f)] -> do
    let fpq = f p q
        target = libraryName dir [ fpqs (R2 (EVar NFloat "d") (EVar NFloat "e")) (R2 (EVar NFloat "a") (EVar NFloat "b")) (R2 (EVar NFloat "x") (EVar NFloat "y")) | fpqs <- fpq ]
    r <- load target
    case r of
      Right p -> readLib p
      Left _ -> do
        let source = R2.compile $ map ((,) (p * q)) fpq -- FIXME compute degree
        (exitCode, stdout, stderr) <- invokeGCC source target
        case exitCode of
          ExitSuccess -> do
            s <- load target
            case s of
              Right p -> readLib p
              Left e -> return (Left e)
          ExitFailure n -> return (Left $ "compile failed with return code " ++ show n ++ "\nstdout:\n" ++ stdout ++ "\nstderr:\n" ++ stderr ++ "\n")) $ \e -> return (Left ("exception: " ++ show e))
