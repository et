{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.Misiurewicz (misiurewicz) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

misiurewicz :: [(Int, R2Formula (Expr String))] -> String
misiurewicz fs = runCompile_ FDouble . function NBool "misiurewicz" [(NInt, "N"), (NInt, "Q"), (NInt, "P"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NVolatileIntPtr, "RUNNING")] $ \ok -> do
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  n <- int
  q <- int
  p <- int
  a <- real
  b <- real
  n .= EVar NInt "N"
  q .= EVar NInt "Q"
  p .= EVar NInt "P"
  let d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
  a .= EVar NReal "A"
  b .= EVar NReal "B"
  qp <- int
  qp .= q + p
  i <- int
  i .= 0
  running <- bool
  running .= read (EVar NVolatileIntPtr "RUNNING") 0
  while_ (i `ELess` n `EAnd` running) $ do
    xq <- real
    yq <- real
    dxaq <- real
    dxbq <- real
    dyaq <- real
    dybq <- real
    x <- real
    y <- real
    dxa <- real
    dxb <- real
    dya <- real
    dyb <- real
    x .= 0
    y .= 0
    dxa .= 0
    dxb .= 0
    dya .= 0
    dyb .= 0
    -- loop one period
    j <- int
    j .= 0
    k <- int
    k .= 0
    while_ (j `ELess` qp `EAnd` running) $ do
      -- save after preperiod
      _ <- if_ (j `EEqual` q) (do
        xq .= x
        yq .= y
        dxaq .= dxa
        dxbq .= dxb
        dyaq .= dya
        dybq .= dyb) (return ())
      -- alloc
      xn <- real
      yn <- real
      dxan <- real
      dxbn <- real
      dyan <- real
      dybn <- real
      -- calc
      let g (_, f) j0 kn = (,) j0 $ do
            let R2 gx gy = f (R2 d_ e_) (R2 a b) (R2 x y)
                fx = optimize gx
                fy = optimize gy
                fdxa = ddx d a fx
                fdxb = ddx d b fx
                fdya = ddx d a fy
                fdyb = ddx d b fy
                d v u
                  | u == x && v == a = dxa
                  | u == x && v == b = dxb
                  | u == y && v == a = dya
                  | u == y && v == b = dyb
                  | u == v = 1
                  | otherwise = 0
            xn .= fx
            yn .= fy
            dxan .= optimize fdxa
            dxbn .= optimize fdxb
            dyan .= optimize fdya
            dybn .= optimize fdyb
            k .= fromIntegral kn
      _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
      -- step
      x .= xn
      y .= yn
      dxa .= dxan
      dxb .= dxbn
      dya .= dyan
      dyb .= dybn
      -- loop
      j .= j + 1
      running .= read (EVar NVolatileIntPtr "RUNNING") 0
    -- subtract preperiod
    x .= x - xq
    y .= y - yq
    dxa .= dxa - dxaq
    dxb .= dxb - dxbq
    dya .= dya - dyaq
    dyb .= dyb - dybq
    -- newton step
    det <- real
    det .= dxa * dyb - dxb * dya
    u <- real
    v <- real
    u .= -( dyb * x - dxb * y) / det
    v .= -(-dya * x + dxa * y) / det
    a .= a + u
    b .= b + v
    -- TODO check convergence
    i .= i + 1
  EVar NReal "A" .= a
  EVar NReal "B" .= b
  ok .= running
  return_ ok

