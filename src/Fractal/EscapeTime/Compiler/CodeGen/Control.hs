{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.CodeGen.Control
  ( if_
  , switch_
  , while_
  , function
  , return_
  , break_
  ) where

import Data.List (intercalate)

import Control.Monad.RWS (get, put)

import Fractal.EscapeTime.Compiler.Expr
import Fractal.EscapeTime.Compiler.CodeGen.Core
import Fractal.EscapeTime.Compiler.CodeGen.Expr

if_ :: Expr String -> CompileM a -> CompileM b -> CompileM (a, b)
if_ test true false = do
  ~b@(EVar _ v) <- bool
  b .= test
  emit $ "if (" ++ v ++ ") {\n"
  x <- true
  emit "} else {\n"
  y <- false
  emit "}\n"
  return (x, y)

switch_ :: Expr String -> [(Int, CompileM a)] -> CompileM [a]
switch_ e cs = do
  ~t@(EVar _ v) <- int
  t .= e
  emit $ "switch (" ++ v ++ ") {\n"
  rs <- mapM case_ cs
  emit $ "default: assert(0); /* internal error */ }\n"
  return rs
  where
    case_ (n, a) = do
      emit $ "case " ++ show n ++ ": {\n"
      r <- a
      emit $ "break; }\n"
      return r

while_ :: Expr String -> CompileM a -> CompileM a
while_ test body = do
  ~b@(EVar _ v) <- bool
  b .= test
  emit $ "while (" ++ v ++ ") {\n"
  x <- body
  b .= test
  emit "}\n"
  return x

return_ :: Expr String -> CompileM ()
return_ (EVar _ v) = do
  (nexts, ~(_:spares)) <- get
  late $ "return " ++ v ++ ";\n"
  put (nexts, []:spares)
return_ _ = error "Fractal.EscapeTime.Compile.CodeGen.Control.return_: not a variable"

break_ :: CompileM ()
break_ = emit "break;"

function :: NType -> String -> [(NType, String)] -> (Expr String -> CompileM a) -> CompileM a
function result name args body = bareScope $ do
  rt <- typeName result
  as <- mapM arg args
  early $ "static " ++ rt ++ " " ++ name ++ "(" ++ intercalate "," as ++ ")\n"
  early "{\n"
  retval <- alloc result
  r <- scope $ body retval
  late "}\n"
  return r
  where
    arg (t, v) = do
      at <- typeName t
      return $ at ++ " " ++ v
