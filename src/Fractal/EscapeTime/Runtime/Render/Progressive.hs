{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Progressive where

import Prelude hiding (read)
import Control.Exception (catch, IOException)
import Control.Monad (forM_, unless)
import Data.STRef (newSTRef, readSTRef, writeSTRef)
import Data.Bits (bit, shiftR)
import Data.Ord (comparing)
import Data.Word (Word8, Word16)

import Foreign.Storable (Storable(..))
import qualified Data.Vector.Storable.Mutable as SV
import Data.Vector.Storable (Vector, (!), createT)
import Data.Vector.Storable.Mutable (IOVector, new, read, write, slice)
import Data.Vector.Algorithms.Merge (sortBy)
import Data.Vector.Storable.MMap (writeMMapVector, unsafeMMapVector)
import System.Directory (createDirectoryIfMissing)
import System.Environment.XDG.BaseDir (getUserCacheDir)
import System.FilePath ((</>))

import Fractal.EscapeTime.Runtime.Render.Buffer (Buffer(Buffer))

newtype Pixel = Pixel (Word16, Word16, Word8, Word8)

instance Storable Pixel where
  sizeOf _ = 6
  alignment _ = 2
  peek p = do
    i <- peekByteOff p 0
    j <- peekByteOff p 2
    w <- peekByteOff p 4
    h <- peekByteOff p 5
    return $ Pixel (i, j, w, h)
  poke p (Pixel (i, j, w, h)) = do
    pokeByteOff p 0 i
    pokeByteOff p 2 j
    pokeByteOff p 4 w
    pokeByteOff p 5 h

type Progressive = Vector Pixel

calculateProgressive :: Int -> Int -> Maybe Progressive
calculateProgressive width height
  | 0 < width  && width  <= 65536 &&
    0 < height && height <= 65536 = createT $ do
      v <- new (width * height)
      let fill i x0 y0 sx sy w h = do
            ix <- newSTRef i
            forM_ [y0, y0 + sy .. height - 1] $ \y -> do
              forM_ [x0, x0 + sx .. width - 1] $ \x -> do
                j <- readSTRef ix
                write v j (Pixel (fromIntegral x, fromIntegral y, fromIntegral w, fromIntegral h))
                writeSTRef ix $! j + 1
            readSTRef ix
          sort i j = sortBy (comparing distance) (slice i (j - i) v)
          cx = fromIntegral width / 2
          cy = fromIntegral height / 2
          distance :: Pixel -> Double
          distance (Pixel (x, y, _, _)) =
            let dx = fromIntegral x - cx
                dy = fromIntegral y - cy
            in  dx * dx + dy * dy
          bit7 = bit 7
      i <- pure 0
      j <- fill i 0 0 bit7 bit7 bit7 bit7
      sort i j
      ix <- newSTRef j
      forM_ (map bit [7, 6 .. 1]) $ \step -> do
        let step1 = step `shiftR` 1 :: Int
        i <- readSTRef ix
        j <- fill i 0 step1 step step step1 step
        sort i j
        k <- fill j step1 0 step step1 step1 step1
        sort j k
        writeSTRef ix k
      n <- readSTRef ix
      return $ if n == width * height then Just v else Nothing
  | otherwise = Nothing

cacheProgressive :: Int -> Int -> IO (Maybe Progressive)
cacheProgressive width height = do
  cacheDir <- getUserCacheDir "et"
  let progDir = cacheDir </> "progressive"
  createDirectoryIfMissing True progDir
  let filePath = progDir </> (show width ++ "x" ++ show height)
      readVector = Just <$> unsafeMMapVector filePath Nothing
      catchIO a b = do
        let f :: IOException -> IO (Maybe Progressive)
            f _ = b
        a `catch` f
  readVector `catchIO` do
    case calculateProgressive width height of
      Nothing -> return Nothing
      Just v -> do
        writeMMapVector filePath v
        readVector `catchIO` return (Just v)

output :: Maybe (Vector Pixel) -> IOVector Bool -> Buffer Float -> Int -> Bool -> (Float, Float, Float, Float) -> IO ()
output (Just prog) done (Buffer width height channels@4 out) npixel d0 (o0, o1, o2, o3) = do
  let Pixel (i, j, w, h) = prog ! npixel
      ii = fromIntegral i
      jj = fromIntegral j
  forM_ [jj .. ((jj + fromIntegral h) `min` height) - 1] $ \v -> do
    forM_ [ii .. ((ii + fromIntegral w) `min` width) - 1] $ \u -> do
      let kk = v * width + u
      d <- read done kk
      unless d $ do
        let k = channels * kk
        SV.write out (k + 0) o0
        SV.write out (k + 1) o1
        SV.write out (k + 2) o2
        SV.write out (k + 3) o3
  write done (jj * width + ii) d0
output Nothing done (Buffer width height channels@4 out) npixel d0 (o0, o1, o2, o3) = do
  let kk = npixel
  d <- read done kk
  unless d $ do
    let k = channels * kk
    SV.write out (k + 0) o0
    SV.write out (k + 1) o1
    SV.write out (k + 2) o2
    SV.write out (k + 3) o3
  write done kk d0
output _ _ _ _ _ _ = fail "Fractal.EscapeTime.Runtime.Render.Progressive.output: bad channel count"
