{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE RecordWildCards #-}

module Fractal.EscapeTime.Runtime.Loader
  ( Output
  , Plain
  , Reference
  , Perturbation
  , PeriodTri
  , PeriodJSK
  , Newton
  , Misiurewicz
  , Size
  , Skew
  , DomainSize
  , ET(..)
  , readLib
  , version
  ) where

import Foreign
import Foreign.C
import Data.Coerce (coerce)
import Numeric.LongDouble (LongDouble)
--import Numeric.Compensated (Compensated)
import Numeric.MPFR.Types (MPFR)
import Numeric.Rounded.Simple (Rounded, withInRounded, withOutRounded, withInOutRounded)
import Data.Vector.Storable.Mutable (IOVector, unsafeWith)
import qualified Data.Vector.Storable.Mutable as V

import qualified Fractal.EscapeTime.Runtime.Loader.Raw as Raw
import Fractal.EscapeTime.Formulas.Types (R2(..))
import Fractal.EscapeTime.Runtime.Loader.Load (load)

type Output t = (t, t, t, t)
type Plain t = Int -> t -> (t, t, t, t) -> t -> t -> t -> t -> Ptr CInt -> IO (Maybe (Output t))
type Reference t = IOVector t -> t -> t -> t -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe Int)
type Perturbation t = IOVector t -> Int -> t -> (t, t, t, t) -> t -> t -> t -> t -> Ptr CInt -> IO (Maybe (Output t, (t, t)))
type PeriodTri = Int -> Double -> Double -> Double -> Rounded -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe Int)
type PeriodJSK = Int -> Double -> Double -> Double -> Rounded -> Rounded -> Rounded -> (Double, Double, Double, Double) -> Ptr CInt -> IO (Maybe Int)
type Newton = Int -> Int -> Double -> Double -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe (R2 Rounded))
type Misiurewicz = Int -> Int -> Int -> Double -> Double -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe (R2 Rounded))
type Size = Int -> Double -> Double -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe (Rounded, (Double, Double, Double, Double)))
type Skew = Int -> Double -> Double -> Rounded -> Rounded -> Bool -> Ptr CInt -> IO (Maybe (Double, Double, Double, Double))
type DomainSize = Int -> Double -> Double -> Rounded -> Rounded -> Ptr CInt -> IO (Maybe Rounded)

data ET = ET
  { close :: IO ()
  , name :: String
  , source :: String
  , degree :: Double
--  , plainf :: Plain Float
  , plain  :: Plain Double
  , plainl :: Plain LongDouble
--  , plainc :: Plain (Compensated Double)
--  , referencef :: Reference Float
  , reference  :: Reference Double
--  , referencel :: Reference LongDouble
--  , perturbationf :: Perturbation Float
  , perturbation  :: Perturbation Double
--  , perturbationl :: Perturbation LongDouble
  , period_tri :: PeriodTri
  , period_jsk :: PeriodJSK
  , newton :: Newton
  , misiurewicz :: Misiurewicz
  , size :: Size
  , skew :: Skew
  , domain_size :: DomainSize
  }

{-
wrapPlainF :: Raw.Plain CFloat -> Plain Float
wrapPlainF f n r2 (h1, h2, h3, h4) d e a b running = withArray (coerce [h1, h2, h3, h4]) $ \hp -> withArray [0,0,0,0] $ \out -> do
  ok <- f (fromIntegral n) (coerce r2) hp (coerce d) (coerce e) (coerce a) (coerce b) out running
  if ok /= 0
  then do
    [de,p,ni,nf] <- coerce <$> peekArray 4 out
    return $ Just (de, p, ni, nf)
  else return Nothing
-}

wrapPlain  :: Raw.Plain CDouble -> Plain Double
wrapPlain  f n r2 (h1, h2, h3, h4) d e a b running = withArray (coerce [h1, h2, h3, h4]) $ \hp -> withArray [0,0,0,0] $ \out -> do
  ok <- f (fromIntegral n) (coerce r2) hp (coerce d) (coerce e) (coerce a) (coerce b) out running
  if ok /= 0
  then do
    [de,p,ni,nf] <- coerce <$> peekArray 4 out
    return $ Just (de, p, ni, nf)
  else return Nothing

wrapPlainL :: (CInt -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr CInt -> IO CInt) -> Plain LongDouble
wrapPlainL f n r2 (h1, h2, h3, h4) d e a b running = withArray [h1, h2, h3, h4] $ \hp -> withArray [0,0,0,0] $ \out -> with r2 $ \r2p -> with d $ \dp -> with e $ \ep -> with a $ \ap -> with b $ \bp -> do
  ok <- f (fromIntegral n) r2p hp dp ep ap bp out running
  if ok /= 0
  then do
    [de,p,ni,nf] <- peekArray 4 out
    return $ Just (de, p, ni, nf)
  else return Nothing

{-
wrapPlainC :: (CInt -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr (Compensated Double) -> Ptr CInt -> IO CInt) -> Plain (Compensated Double)
wrapPlainC f n r2 (h1, h2, h3, h4) d e a b running = withArray [h1, h2, h3, h4] $ \hp -> withArray [0,0,0,0] $ \out -> with r2 $ \r2p -> with d $ \dp -> with e $ \ep -> with a $ \ap -> with b $ \bp -> do
  ok <- f (fromIntegral n) r2p hp dp ep ap bp out running
  if ok /= 0
  then do
    [de,p,ni,nf] <- peekArray 4 out
    return $ Just (de, p, ni, nf)
  else return Nothing

wrapReferenceF :: Raw.Reference CFloat -> Reference Float
wrapReferenceF f v r2 d e a b running = unsafeWith v $ \vp -> withInRounded a $ \ap -> withInRounded b $ \bp -> do
  n <- f (fromIntegral (V.length v `div` 3)) (coerce r2) (coerce d) (coerce e) ap bp (castPtr vp) running
  ok <- peek running
  if ok /= 0
    then return (Just $ fromIntegral n)
    else return Nothing
-}

wrapReference  :: Raw.Reference CDouble -> Reference Double
wrapReference  f v r2 d e a b running = unsafeWith v $ \vp -> withInRounded a $ \ap -> withInRounded b $ \bp -> do
  n <- f (fromIntegral (V.length v `div` 3)) (coerce r2) (coerce d) (coerce e) ap bp (castPtr vp) running
  ok <- peek running
  if ok /= 0
    then return (Just $ fromIntegral n)
    else return Nothing

{-
wrapReferenceL :: (CInt -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr MPFR -> Ptr MPFR -> Ptr LongDouble -> Ptr CInt -> IO CInt) -> Reference LongDouble
wrapReferenceL f v r2 d e a b running = unsafeWith v $ \vp -> withInRounded a $ \ap -> withInRounded b $ \bp -> with r2 $ \r2p -> with d $ \dp -> with e $ \ep -> do
  n <- f (fromIntegral (V.length v `div` 3)) r2p dp ep ap bp vp running
  ok <- peek running
  if ok /= 0
    then return (Just $ fromIntegral n)
    else return Nothing

wrapPerturbationF :: Raw.Perturbation CFloat -> Perturbation Float
wrapPerturbationF f v m r2 (h1, h2, h3, h4) d e a b running = unsafeWith v $ \vp -> withArray (map coerce [h1, h2, h3, h4]) $ \hp -> withArray [0,0,0,0,0,0] $ \out -> do
  ok <- f (fromIntegral m) (fromIntegral (V.length v `div` 3)) (coerce r2) hp (coerce d) (coerce e) (coerce a) (coerce b) (castPtr vp) out running
  if ok /= 0
  then do
    [de,p,ni,nf,u,v] <- coerce <$> peekArray 6 out
    return $ Just ((de, p, ni, nf),(u,v))
  else return Nothing
-}

wrapPerturbation  :: Raw.Perturbation CDouble -> Perturbation Double
wrapPerturbation  f v m r2 (h1, h2, h3, h4) d e a b running = unsafeWith v $ \vp -> withArray (map coerce [h1, h2, h3, h4]) $ \hp -> withArray [0,0,0,0,0,0] $ \out -> do
  ok <- f (fromIntegral m) (fromIntegral (V.length v `div` 3)) (coerce r2) hp (coerce d) (coerce e) (coerce a) (coerce b) (castPtr vp) out running
  if ok /= 0
  then do
    [de,p,ni,nf,u,v] <- coerce <$> peekArray 6 out
    return $ Just ((de, p, ni, nf),(u,v))
  else return Nothing

{-
wrapPerturbationL :: (CInt -> CInt -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr LongDouble -> Ptr CInt -> IO CInt) -> Perturbation LongDouble
wrapPerturbationL f v m r2(h1, h2, h3, h4) d e a b running = unsafeWith v $ \vp -> withArray [h1, h2, h3, h4] $ \hp -> withArray [0,0,0,0,0,0] $ \out -> with r2 $ \r2p -> with d $ \dp -> with e $ \ep -> with a $ \ap -> with b $ \bp -> do
  ok <- f (fromIntegral m) (fromIntegral (V.length v `div` 3)) r2p hp dp ep ap bp vp out running
  if ok /= 0
  then do
    [de,p,ni,nf,u,v] <- peekArray 6 out
    return $ Just ((de, p, ni, nf),(u,v))
  else return Nothing
-}

wrapPeriodTri :: Raw.PeriodTri -> PeriodTri
wrapPeriodTri f n r2 d e a b h running = withInRounded a $ \ap -> withInRounded b $ \bp -> withInRounded h $ \hp -> do
  p <- f (fromIntegral n) (coerce r2) (coerce d) (coerce e) ap bp hp running
  ok <- peek running
  if p > 0 && ok /= 0
    then return (Just $ fromIntegral p)
    else return Nothing

wrapPeriodJSK :: Raw.PeriodJSK -> PeriodJSK
wrapPeriodJSK f n r2 d e a b h (k0, k1, k2, k3) running = withInRounded a $ \ap -> withInRounded b $ \bp -> withInRounded h $ \hp -> withArray (coerce [k0, k1, k2, k3]) $ \kp -> do
  p <- f (fromIntegral n) (coerce r2) (coerce d) (coerce e) ap bp hp kp running
  ok <- peek running
  if p > 0 && ok /= 0
    then return (Just $ fromIntegral p)
    else return Nothing

wrapNewton :: Raw.Newton -> Newton
wrapNewton f n p d e a b running = do
  (a', (b', ok)) <- withInOutRounded a $ \ap -> withInOutRounded b $ \bp -> do
    f (fromIntegral n) (fromIntegral p) (coerce d) (coerce e) ap bp running
  if ok /= 0
    then return (Just (R2 a' b'))
    else return Nothing

wrapMisiurewicz :: Raw.Misiurewicz -> Misiurewicz
wrapMisiurewicz f n q p d e a b running = do
  (a', (b', ok)) <- withInOutRounded a $ \ap -> withInOutRounded b $ \bp -> do
    f (fromIntegral n) (fromIntegral q) (fromIntegral p) (coerce d) (coerce e) ap bp running
  if ok /= 0
    then return (Just (R2 a' b'))
    else return Nothing

wrapSize :: Raw.Size -> Size
wrapSize f p d e a b running = do
  let prec = 53
  withArray [0,0,0,0] $ \sp -> do
    (r, ok) <- withOutRounded prec $ \rp -> withInRounded a $ \ap -> withInRounded b $ \bp -> do
      f (fromIntegral p) (coerce d) (coerce e) ap bp rp sp running
    if ok /= 0
      then do
        [w,x,y,z] <- coerce <$> peekArray 4 sp
        return (Just (r, (w, x, y, z)))
      else return Nothing

wrapSkew :: Raw.Skew -> Skew
wrapSkew f n d e a b usedz running = do
  withArray [0,0,0,0] $ \sp -> do
    ok <- withInRounded a $ \ap -> withInRounded b $ \bp -> do
      f (fromIntegral n) (coerce d) (coerce e) ap bp (if usedz then 1 else 0) sp running
    if ok /= 0
      then do
        [w,x,y,z] <- coerce <$> peekArray 4 sp
        return (Just (w, x, y, z))
      else return Nothing

wrapDomainSize :: Raw.DomainSize -> DomainSize
wrapDomainSize f p d e a b running = do
  let prec = 53
  (r, ok) <- withOutRounded prec $ \rp -> withInRounded a $ \ap -> withInRounded b $ \bp -> do
    f (fromIntegral p) (coerce d) (coerce e) ap bp rp running
  if ok /= 0
    then return (Just r)
    else return Nothing

readLib :: (IO (), Ptr Raw.S_Formula) -> IO (Either String ET)
readLib (close, p) = do
    s <- peek p
    if Raw.magic s == magic && Raw.ssize s == fromIntegral (sizeOf s) && Raw.version s == version
    then do
      name <- peekCString (Raw.name s)
      source <- peekCString (Raw.source s)
      let degree = coerce (Raw.degree s)
--          plainf = wrapPlainF (Raw.mkPlainF $ Raw.plainf s)
          plain  = wrapPlain  (Raw.mkPlain  $ Raw.plain  s)
          plainl = wrapPlainL (Raw.mkPlainL $ Raw.plainl s)
--          plainc = wrapPlainC (Raw.mkPlainC $ Raw.plainc s)
--          referencef = wrapReferenceF (Raw.mkReferenceF $ Raw.referencef s)
          reference  = wrapReference  (Raw.mkReference  $ Raw.reference  s)
--          referencel = wrapReferenceL (Raw.mkReferenceL $ Raw.referencel s)
--          perturbationf = wrapPerturbationF (Raw.mkPerturbationF $ Raw.perturbationf s)
          perturbation  = wrapPerturbation  (Raw.mkPerturbation  $ Raw.perturbation  s)
--          perturbationl = wrapPerturbationL (Raw.mkPerturbationL $ Raw.perturbationl s)
          period_tri = wrapPeriodTri (Raw.mkPeriodTri $ Raw.period_tri s)
          period_jsk = wrapPeriodJSK (Raw.mkPeriodJSK $ Raw.period_jsk s)
          newton = wrapNewton (Raw.mkNewton $ Raw.newton s)
          misiurewicz = wrapMisiurewicz (Raw.mkMisiurewicz $ Raw.misiurewicz s)
          size = wrapSize (Raw.mkSize $ Raw.size s)
          skew = wrapSkew (Raw.mkSkew $ Raw.skew s)
          domain_size = wrapDomainSize (Raw.mkDomainSize $ Raw.domain_size s)
      return (Right (ET{..}))
    else close >> (return $ Left "incompatible library found")

magic :: CInt
magic = 0xC01dCaf3

version :: CInt
version = 8
