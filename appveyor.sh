# Configure the environment
MSYSTEM=MINGW64
source /etc/profile || true # a terrible, terrible workaround for msys2 brokenness

# Don't set -e until after /etc/profile is sourced
set -ex
cd $APPVEYOR_BUILD_FOLDER

export GHCVERSION="8.0.2"
export PATH="/opt/ghc-${GHCVERSION}/bin:/opt/ghc-${GHCVERSION}/lib/bin:/opt/ghc-${GHCVERSION}/mingw/bin:/opt/ghc-${GHCVERSION}/mingw/x86_64-w64-mingw32/bin:/usr/local/bin:${PATH}"

case "$1" in
    "prepare")
        # Bring msys up-to-date
        # However, we current don't do this: generally one must restart all
        # msys2 processes when updating the msys2 runtime, which this may do. We can't
        # easily do this and therefore do simply don't update.
        #pacman --noconfirm -Syuu

        # Install basic build dependencies
        pacman --noconfirm -S --needed git tar bsdtar binutils autoconf make xz curl libtool automake python python2 p7zip patch mingw-w64-$(uname -m)-tools-git

        # Install build dependencies
        mkdir -p /opt
        wget -q -O - https://downloads.haskell.org/~ghc/8.0.2/ghc-8.0.2-x86_64-unknown-mingw32-win10.tar.xz | tar -xJ -C /opt
        mkdir -p /usr/local/bin
        wget -q -O - https://www.haskell.org/cabal/release/cabal-install-2.0.0.1/cabal-install-2.0.0.1-x86_64-unknown-mingw32.zip | bsdtar -xzf- -C /usr/local/bin
        cabal update
        git clone https://code.mathr.co.uk/long-double.git
        git clone https://code.mathr.co.uk/rounded.git
        ;;

    "build")
        cabal install -j --prefix=/usr/local --disable-profiling --disable-documentation --constraint="transformers-compat +five" et.cabal rounded/rounded.cabal long-double/long-double.cabal
        et-cli out.png 640 360 0 0 2 256 1 0 0 1
        ;;

    "test")
        mkdir /tmp/et-windows
        cp out.png /tmp/et-windows/
        cp `which et-cli` /tmp/et-windows/
        cp `dirname $(which ghc)`/../mingw/bin/libgmp-10.dll /tmp/et-windows/
        cp `dirname $(which ghc)`/../mingw/bin/libwinpthread-1.dll /tmp/et-windows/
        7z a et-windows.zip /tmp/et-windows
        ;;

    *)
        echo "$0: unknown mode $1"
        exit 1
        ;;
esac
