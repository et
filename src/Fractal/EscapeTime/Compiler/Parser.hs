{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE FlexibleContexts #-}

module Fractal.EscapeTime.Compiler.Parser (Formula, formulas) where

import Data.Char (isSpace)
import Data.Monoid ((<>))
import Data.Set (Set)
import qualified Data.Set as S

import Text.Parsec (parse, ParseError, (<|>), try, choice, many)
import Text.Parsec.Char (string)
import Text.Parsec.Combinator (chainl1)

import Fractal.EscapeTime.Formulas.Types (R2(..))
import Fractal.EscapeTime.Compiler.Expr.AST hiding (interpret)

type Formula a = Int -> Int -> [R2 a -> R2 a -> R2 a -> R2 a]

data Assignment = LetW PExpr | SetZ PExpr

interpretA :: (Eq a, Floating a) => [Assignment] -> Either String (Set String, Formula a)
interpretA [] = return (S.empty, \p q -> [])
interpretA (LetW e : SetZ f : gs) = do
  _ <- letW e 1 1 (sqrt 2 `R2` sqrt 3) (sqrt 5 `R2` sqrt 7) (sqrt 31 `R2` sqrt 37) $ setZ f 1 1 (sqrt 11 `R2` sqrt 13) (sqrt 17 `R2` sqrt 19) (sqrt 23 `R2` sqrt 29)
        -- by parametricity, if this succeeds so will the case below
  (g', g) <- interpretA gs
  return (g' <> variables e <> variables f, \p q -> (\a c z -> case letW e p q a c z $ setZ f p q a c z of Right z' -> z') : g p q)
interpretA (SetZ f : gs) = do
  _ <- setZ f 1 1 (sqrt 2 `R2` sqrt 3) (sqrt 5 `R2` sqrt 7) (sqrt 11 `R2` sqrt 13) (sqrt 17 `R2` sqrt 19)
        -- by parametricity, if this succeeds so will the case below
  (g', g) <- interpretA gs
  return (g' <> variables f, \p q -> (\a c z -> case let w = 0 in setZ f p q a c z w of Right z' -> z') : g p q)

letW :: (Eq a, Floating a) => PExpr -> Int -> Int -> R2 a -> R2 a -> R2 a -> (R2 a -> Either String (R2 a)) -> Either String (R2 a)
letW ex p q (R2 d e) (R2 a b) (R2 x y) f
  = f =<< interpret
            [ ("p", fromIntegral p)
            , ("q", fromIntegral q)
            , ("i", R2 0 1)
            , ("a", R2 a 0)
            , ("b", R2 b 0)
            , ("c", R2 a b)
            , ("d", R2 d 0)
            , ("e", R2 e 0)
            , ("f", R2 d e)
            , ("x", R2 x 0)
            , ("y", R2 y 0)
            , ("z", R2 x y)
            ] ex

setZ :: (Eq a, Floating a) => PExpr -> Int -> Int -> R2 a -> R2 a -> R2 a -> R2 a -> Either String (R2 a)
setZ ex p q (R2 d e) (R2 a b) (R2 x y) (R2 u v)
  = interpret
            [ ("p", fromIntegral p)
            , ("q", fromIntegral q)
            , ("i", R2 0 1)
            , ("a", R2 a 0)
            , ("b", R2 b 0)
            , ("c", R2 a b)
            , ("d", R2 d 0)
            , ("e", R2 e 0)
            , ("f", R2 d e)
            , ("x", R2 x 0)
            , ("y", R2 y 0)
            , ("z", R2 x y)
            , ("u", R2 u 0)
            , ("v", R2 v 0)
            , ("w", R2 u v)
            ] ex

formulas :: (Eq a, Floating a) => String -> Either String [(String, Set String, Formula a)]
formulas s = sequence
  [ (\r -> case r of Left e -> Left (name ++ ":" ++ show e) ; Right (vs, x) -> Right (name, vs, x))
  . (\r -> case r of Left e -> Left (show e) ; Right x -> interpretA x)
  . sequence
  . map (uncurry assignment)
  . zip [1..]
  . map (filter (not . isSpace))
  $ as
  | name : as <-
    paras
  . map stripComment
  . lines
  $ s
  ]

stripComment :: String -> String
stripComment = takeWhile ('#' /=)

paras :: [String] -> [[String]]
paras s = case dropWhile null s of
  [] -> []
  s' -> p : paras s'' where ~(p, s'') = break null s'

assignment :: Int -> String -> Either ParseError Assignment
assignment n s = case s of
  'w':':':'=':t -> LetW <$> parse expr (show n) t
  'z':':':'=':t -> SetZ <$> parse expr (show n) t
  _ -> parse (fail $ "unexpected: " ++ s) (show n) s

expr   = do{ string "-"; PNeg <$> factor }
    <|>  do{ a <- factor;
           do { string "+"; b <- expr; return $ PAdd a b } <|>
           do { string "-"; b <- expr; return $ PAdd a (PNeg b) } <|>
           return a }

factor = do{ a <- base; try (do{ string "/"; b <- base ; return $ PMul a (PInv b) })
                    <|> try (do{ b <- factor; return $ PMul a b })
                    <|> return a }

base   = do{ a <- sub; do{ string "^"; b <- sub; return $ PPow a b } <|> return a }

sub    = do{ string "("; a <- expr; string ")"; return a }
    <|>  do{ string "|"; a <- expr; string "|"; return $ PFunc "abs" a }
    <|>  do{ f <- function ; string "("; a <- expr ; string ")" ; return (f a) }
    <|>  variable
    <|>  number

-- note: no function name should be constructible from any variable names

number = do { n <- digit ; ns <- many ((string "0" >> return '0') <|> digit) ; return $ PInt (read (n : ns)) }
  where digit = choice [string [d] >> return d | d <- "123456789"]

variable = choice $ [var "pi"] ++ [var [x] | x <- "pqiabcdefuvwxyz"]
  where var s = try (string s >> return (PVar s))

function = choice . map func . words $ "abs sqrt exp log sin cos tan sinh cosh tanh asin acos atan asinh acosh atanh"
  where func s = try (string s >> return (PFunc s))

data PExpr
  = PVar String
  | PInt Integer
  | PNeg PExpr
  | PInv PExpr
  | PAdd PExpr PExpr
  | PMul PExpr PExpr
  | PPow PExpr PExpr
  | PFunc String PExpr
  deriving Show

variables :: PExpr -> Set String
variables (PVar s) = S.singleton s
variables (PInt _) = S.empty
variables (PNeg e) = variables e
variables (PInv e) = variables e
variables (PAdd e f) = variables e <> variables f
variables (PMul e f) = variables e <> variables f
variables (PPow e f) = variables e <> variables f
variables (PFunc _ e) = variables e

class Pow t where pow :: t -> t -> t
instance Pow Double where pow = (**)
instance (Eq a, Fractional a) => Pow (R2 a) where
  pow z n = case [ i | i <- [-100..100], fromInteger i == n ] of -- FIXME
    [] -> 0
    (j:_) -> z ^^ j

interpret :: (Pow t, Floating t) => [(String, t)] -> PExpr -> Either String t
-- interpret variables
interpret vs (PVar "pi") = Right pi
interpret vs (PVar u) = maybe (Left u) Right (lookup u vs)
interpret vs (PInt n) = Right (fromInteger n)
-- applicative traversal
interpret vs (PNeg e) = negate <$> interpret vs e
interpret vs (PInv e) = recip <$> interpret vs e
interpret vs (PAdd e f) = (+) <$> interpret vs e <*> interpret vs f
interpret vs (PMul e f) = (*) <$> interpret vs e <*> interpret vs f
interpret vs (PPow e f) = pow <$> interpret vs e <*> interpret vs f
interpret vs (PFunc "abs" e) = abs <$> interpret vs e
interpret vs (PFunc "sqrt" e) = sqrt <$> interpret vs e
interpret vs (PFunc "exp" e) = exp <$> interpret vs e
interpret vs (PFunc "log" e) = log <$> interpret vs e
interpret vs (PFunc "sin" e) = sin <$> interpret vs e
interpret vs (PFunc "cos" e) = cos <$> interpret vs e
interpret vs (PFunc "tan" e) = tan <$> interpret vs e
interpret vs (PFunc "sinh" e) = sinh <$> interpret vs e
interpret vs (PFunc "cosh" e) = cosh <$> interpret vs e
interpret vs (PFunc "tanh" e) = tanh <$> interpret vs e
interpret vs (PFunc "asin" e) = asin <$> interpret vs e
interpret vs (PFunc "acos" e) = acos <$> interpret vs e
interpret vs (PFunc "atan" e) = atan <$> interpret vs e
interpret vs (PFunc "asinh" e) = asinh <$> interpret vs e
interpret vs (PFunc "acosh" e) = acosh <$> interpret vs e
interpret vs (PFunc "atanh" e) = atanh <$> interpret vs e
interpret vs e = Left (show e)
