{-
et -- escape time fractals
Copyright (C) 2018,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.Expr.Optimize (superoptimize, metric) where

import Data.Data
import Data.List (minimumBy, sort)
import Data.Ord (comparing)
import Data.Set (Set, empty, singleton, unions, size, toList, fromList)
import Data.Generics.Uniplate.Data (contexts)

import Debug.Trace

import Fractal.EscapeTime.Compiler.Expr.AST

step :: (Ord t, Data t) => Set (Expr t) -> Set (Expr t)
step s = fromList [ c y | x <- toList s, (e, c) <- contexts x, let t = rules allRules e, y <- toList t ]

fix f s = traceShow (size s) $ let t = f s in if t == s then s else fix f t

superoptimize :: (Ord t, Data t) => Int -> Int -> Expr t -> Expr t
superoptimize m n = minimumBy (comparing metric) . go n . singleton
  where
    go 0 s = s
    go n s = (\t -> if s == t then t else go (n - 1) t) . topm . step $ s
    topm = fromList . take m . map snd . sort . map (\e -> (metric e, e)) . toList

depth :: Expr t -> Integer
depth (EInt _) = 1
depth (ERat _) = 1
depth (EVar _ _) = 1
depth (ENeg _ a) = 1 + depth a
depth (EInv _ a) = 1 + depth a
depth (EAdd _ a b) = 1 + max (depth a) (depth b)
depth (EMul _ a b) = 1 + max (depth a) (depth b)
depth (EAbs _ a) = 1 + depth a
depth (ESin _ a) = 1 + depth a
depth (ECos _ a) = 1 + depth a
depth (ETan _ a) = 1 + depth a
depth (ESinh _ a) = 1 + depth a
depth (ECosh _ a) = 1 + depth a
depth (ETanh _ a) = 1 + depth a
depth (EPi _) = 1
depth (EExp _ a) = 1 + depth a
depth (ELog _ a) = 1 + depth a
depth (EExpM1 _ a) = 1 + depth a
depth (ELog1P _ a) = 1 + depth a
depth (EFloat a) = 1 + depth a
depth (ESgn a) = 1 + depth a
depth (ESqrt _ a) = 1 + depth a
depth (EASin _ a) = 1 + depth a
depth (EACos _ a) = 1 + depth a
depth (EATan _ a) = 1 + depth a
depth (EASinh _ a) = 1 + depth a
depth (EACosh _ a) = 1 + depth a
depth (EATanh _ a) = 1 + depth a
depth (EFloor _ a) = 1 + depth a
depth (ELess a b) = 1 + max (depth a) (depth b)
depth (ELessEqual a b) = 1 + max (depth a) (depth b)
depth (EEqual a b) = 1 + max (depth a) (depth b)
depth (EOr a b) = 1 + max (depth a) (depth b)
depth (EAnd a b) = 1 + max (depth a) (depth b)
depth (ERead a b) = 1 + max (depth a) (depth b)
depth (EDiffAbs _ a b) = 1 + max (depth a) (depth b)
depth (EIf t a b) = 1 + max (depth t) (max (depth a) (depth b))

cost :: Ord t => Expr t -> Integer
cost (EInt 0)    = 100000000
cost (EInt 1)    = 1000000
cost (EInt _) = 1
cost (ERat 0)    = 100000000
cost (ERat 1)    = 1000000
cost (ERat _) = 1
cost (EVar _ _) = 50
cost (ENeg _ a) = 1000000 + cost a
cost (EInv _ a) = 100 + cost a
cost (EAdd _ a b) = 10 + cost a + cost b
cost (EMul _ a b) = (if a == b then 30 else 50) + cost a + cost b
cost (EAbs _ a) = 3 + cost a
cost (ESin _ a) = 1000 + cost a
cost (ECos _ a) = 1000 + cost a
cost (ETan _ a) = 1000 + cost a
cost (ESinh _ a) = 1000 + cost a
cost (ECosh _ a) = 1000 + cost a
cost (ETanh _ a) = 1000 + cost a
cost (EPi _) = 1
cost (EExp _ a) = 1000 + cost a
cost (ELog _ a) = 1000 + cost a
cost (EExpM1 _ a) = 1000 + cost a
cost (ELog1P _ a) = 1000 + cost a
cost (EFloat a) = 5 + cost a
cost (ESgn a) = 3 + cost a
cost (ESqrt _ a) = 500 + cost a
cost (EASin _ a) = 1000 + cost a
cost (EACos _ a) = 1000 + cost a
cost (EATan _ a) = 1000 + cost a
cost (EASinh _ a) = 1000 + cost a
cost (EACosh _ a) = 1000 + cost a
cost (EATanh _ a) = 1000 + cost a
cost (EFloor _ a) = 500 + cost a
cost (ELess a b) = 10 + cost a + cost b
cost (ELessEqual a b) = 10 + cost a + cost b
cost (EEqual a b) = 10 + cost a + cost b
cost (EOr a b) = 10 + cost a + cost b
cost (EAnd a b) = 10 + cost a + cost b
cost (ERead a b) = 10 + cost a + cost b
cost (EDiffAbs _ a b) = 10000 + cost a + cost b
cost (EIf t a b) = 10 + cost t + cost a + cost b

metric :: Ord t => Expr t -> (Integer, Integer)
metric a = (cost a, depth a)

type Rule t = Expr t -> Set (Expr t)

rule :: Ord t => Rule t -> Set (Expr t) -> Maybe (Set (Expr t))
rule r s = let t = unions (map r (toList s)) in if s == t then Nothing else Just t

rules :: (Ord t, Data t) => [Rule t] -> Rule t
rules rs e = unions (map ($ e) rs)

allRules :: Ord t => [Rule t]
allRules =
  [ identity
  , add_id
  , mul_id
  , zero
  , negate_mul
  , distribute
  , constant
  , abs_mul
  , associate_r
  , associate_l
  , swap
  , undistribute_i
  , undistribute
  , abs_i
  , mul_signum
  , signum_i
  , sin_sin
  , cos_cos
  , tan_tan
  , sinh_sinh
  , cosh_cosh
  , tanh_tanh
  ]

-- identity

identity e = singleton e

-- identity

add_id (EAdd _ (EInt 0) b) = singleton b
add_id (EAdd _ b (EInt 0)) = singleton b
add_id (EAdd _ (ERat 0) b) = singleton b
add_id (EAdd _ b (ERat 0)) = singleton b
add_id _ = empty

mul_id (EMul _ (EInt 1) b) = singleton b
mul_id (EMul _ b (EInt 1)) = singleton b
mul_id (EMul _ (ERat 1) b) = singleton b
mul_id (EMul _ b (ERat 1)) = singleton b
mul_id _ = empty

-- zero

zero (ENeg _ (EInt 0)) = singleton 0
zero (EAbs _ (EInt 0)) = singleton 0
zero (EMul _ (EInt 0) _) = singleton 0
zero (EMul _ _ (EInt 0)) = singleton 0
zero (EMul _ (ERat 0) _) = singleton 0
zero (EMul _ _ (ERat 0)) = singleton 0
zero _ = empty

-- negate

negate_mul (ENeg t a) = singleton $ EMul t (EInt (-1)) a
negate_mul (EMul t (EInt (-1)) a) = singleton $ ENeg t a
negate_mul _ = empty

-- distribute

distribute (EMul t a (EAdd _ b c)) = singleton $ EAdd t (EMul t a b) (EMul t a c)
distribute (EMul t (EAdd _ a b) c) = singleton $ EAdd t (EMul t a c) (EMul t b c)
distribute _ = empty

-- combine constants

constant (ENeg t (EInt a)) = singleton (EInt (negate a))
constant (ENeg t (ERat a)) = singleton (ERat (negate a))
constant (EAdd _ (EInt a) (EInt b)) = singleton $ EInt (a + b)
constant (EAdd t (EInt a) (EAdd _ (EInt b) c)) = singleton $ EAdd t (EInt (a + b)) c
constant (EAdd _ (EInt a) (ERat b)) = singleton $ ERat $ fromInteger a + b
constant (EAdd t (EInt a) (EAdd _ (ERat b) c)) = singleton $ EAdd t (ERat (fromInteger a + b)) c
constant (EAdd _ (ERat a) (ERat b)) = singleton $ ERat (a + b)
constant (EAdd t (ERat a) (EAdd _ (ERat b) c)) = singleton $ EAdd t (ERat (a + b)) c
constant (EMul _ (EInt a) (EInt b)) = singleton $ EInt (a * b)
constant (EMul t (EInt a) (EMul _ (EInt b) c)) = singleton $ EMul t (EInt (a * b)) c
constant (EMul _ (EInt a) (ERat b)) = singleton $ ERat $ fromInteger a * b
constant (EMul t (EInt a) (EMul _ (ERat b) c)) = singleton $ EMul t (ERat (fromInteger a * b)) c
constant (EMul _ (ERat a) (ERat b)) = singleton $ ERat (a * b)
constant (EMul t (ERat a) (EMul _ (ERat b) c)) = singleton $ EMul t (ERat (a * b)) c
constant _ = empty

-- combine abs*abs

abs_mul (EMul t (EAbs _ a) (EAbs _ b))
  | a == b = singleton $ EMul t a b
  | otherwise = singleton $ EAbs t (EMul t a b)
abs_mul (EMul t (EAbs _ a) (EMul _ (EAbs _ b) c))
  | a == b = singleton $ EMul t (EMul t a b) c
  | otherwise = singleton $ EMul t (EAbs t (EMul t a b)) c
abs_mul _ = empty

-- associate

associate_l (EAdd t c (EAdd _ a b)) = singleton $ EAdd t (EAdd t c a) b
associate_l (EMul t c (EMul _ a b)) = singleton $ EMul t (EMul t c a) b
associate_l _ = empty

associate_r (EAdd t (EAdd _ a b) c) = singleton $ EAdd t a (EAdd t b c)
associate_r (EMul t (EMul _ a b) c) = singleton $ EMul t a (EMul t b c)
associate_r _ = empty

-- swap

swap (EMul t a b) = singleton $ EMul t b a
swap (EAdd t a b) = singleton $ EAdd t b a
swap _ = empty

-- undistribute int

undistribute_i (EAdd _ (EMul _ (EInt a) x) (EAdd _ (EMul _ (EInt b) y) c)) | x == y = singleton $ fromInteger (a + b) * x + c
undistribute_i (EAdd _ x (EAdd _ (EMul _ (EInt b) y) c)) | x == y = singleton $ fromInteger (1 + b) * x + c
undistribute_i (EAdd _ (EMul _ (EInt a) x) (EAdd _ y c)) | x == y = singleton $ fromInteger (a + 1) * x + c
undistribute_i (EAdd _ (EMul _ (EInt a) x) (EMul _ (EInt b) y)) | x == y = singleton $ fromInteger (a + b) * x
undistribute_i (EAdd _ x (EMul _ (EInt b) y)) | x == y = singleton $ fromInteger (1 + b) * x
undistribute_i (EAdd _ (EMul _ (EInt a) x) y) | x == y = singleton $ fromInteger (a + 1) * x
undistribute_i (EAdd _ x (EAdd _ y c)) | x == y = singleton $ 2 * x + c
undistribute_i (EAdd _ x y) | x == y = singleton $ 2 * x
undistribute_i _ = empty

-- undistribute

undistribute (EAdd _ (EMul _ x1 y1) (EMul _ x2 y2)) | x1 == x2 = singleton $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ y1 x1) (EMul _ x2 y2)) | x1 == x2 = singleton $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ x1 y1) (EMul _ y2 x2)) | x1 == x2 = singleton $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ y1 x1) (EMul _ y2 x2)) | x1 == x2 = singleton $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ x1 y1) (EAdd _ (EMul _ x2 y2) c)) | x1 == x2 = singleton $ x1 * (y1 + y2) + c
undistribute (EAdd _ (EMul _ y1 x1) (EAdd _ (EMul _ x2 y2) c)) | x1 == x2 = singleton $ x1 * (y1 + y2) + c
undistribute (EAdd _ (EMul _ x1 y1) (EAdd _ (EMul _ y2 x2) c)) | x1 == x2 = singleton $ x1 * (y1 + y2) + c
undistribute (EAdd _ (EMul _ y1 x1) (EAdd _ (EMul _ y2 x2) c)) | x1 == x2 = singleton $ x1 * (y1 + y2) + c
{-
undistribute (EAdd _ (EDiv _ y1 x1) (EDiv _ y2 x2)) | x1 == x2 = singleton $ (y1 + y2) / x1
undistribute (EAdd _ (EDiv _ y1 x1) (EDiv _ y2 x2)) | x1 == x2 = singleton $ (y1 + y2) / x1
undistribute (EAdd _ (EDiv _ y1 x1) (EAdd _ (EDiv _ y2 x2) c)) | x1 == x2 = singleton $ (y1 + y2) / x1 + c
undistribute (EAdd _ (EDiv _ y1 x1) (EAdd _ (EDiv _ y2 x2) c)) | x1 == x2 = singleton $ (y1 + y2) / x1 + c
-}
undistribute _ = empty

-- misc

abs_i (EAbs _ (EMul _ (EInt a) b)) = singleton $ fromInteger (abs a) * abs b
abs_i _ = empty

mul_signum (ESgn (EMul _ a b)) = singleton $ signum a * signum b
mul_signum _ = empty

signum_i (ESgn (EInt a)) = singleton $ EInt (signum a)
signum_i _ = empty

-- sin + sin

sin_sin (EAdd _ (ESin _ a) (ESin _ b)) = singleton $ 2 * sin ((a + b) / 2) * cos ((a - b) / 2)
sin_sin (EAdd _ (ESin _ a) (EAdd _ (ESin _ b) c)) = singleton $ 2 * sin ((a + b) / 2) * cos ((a - b) / 2) + c
sin_sin _ = empty

-- cos + cos

cos_cos (EAdd _ (ECos _ a) (ECos _ b)) = singleton $ 2 * cos ((a + b) / 2) * cos ((a - b) / 2)
cos_cos (EAdd _ (ECos _ a) (EAdd _ (ECos _ b) c)) = singleton $ 2 * cos ((a + b) / 2) * cos ((a - b) / 2) + c
cos_cos _ = empty

-- tan + tan

tan_tan (EAdd _ (ETan _ a) (ETan _ b)) = singleton $ sin (a + b) / (cos a * cos b)
tan_tan (EAdd _ (ETan _ a) (EAdd _ (ETan _ b) c)) = singleton $ sin (a + b) / (cos a * cos b) + c
tan_tan _ = empty

-- sinh + sinh

sinh_sinh (EAdd _ (ESinh _ a) (ESinh _ b)) = singleton $ 2 * sinh ((a + b) / 2) * cosh ((a - b) / 2)
sinh_sinh (EAdd _ (ESinh _ a) (EAdd _ (ESinh _ b) c)) = singleton $ 2 * sinh ((a + b) / 2) * cosh ((a - b) / 2) + c
sinh_sinh _ = empty

-- cosh + cosh

cosh_cosh (EAdd _ (ECosh _ a) (ECosh _ b)) = singleton $ 2 * cosh ((a + b) / 2) * cosh ((a - b) / 2)
cosh_cosh (EAdd _ (ECosh _ a) (EAdd _ (ECosh _ b) c)) = singleton $ 2 * cosh ((a + b) / 2) * cosh ((a - b) / 2) + c
cosh_cosh _ = empty

-- tanh + tanh

tanh_tanh (EAdd _ (ETanh _ a) (ETanh _ b)) = singleton $ sinh (a + b) / (cosh a * cosh b)
tanh_tanh (EAdd _ (ETanh _ a) (EAdd _ (ETanh _ b) c)) = singleton $ sinh (a + b) / (cosh a * cosh b) + c
tanh_tanh _ = empty
