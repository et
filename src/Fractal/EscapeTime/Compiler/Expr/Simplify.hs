{-
et -- escape time fractals
Copyright (C) 2018,2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.Expr.Simplify where

import Data.List (sort)
import Data.Maybe (fromMaybe)
import Data.Ratio (numerator, denominator)
import Numeric
import qualified Data.Map as M

import Fractal.EscapeTime.Compiler.Expr.AST

-- |int| < |rat| < abs < sgn < mul < add < var
cmpMul :: Ord a => Expr a -> Expr a -> Bool
cmpMul (EInt i) (EInt j) = abs i < abs j
cmpMul EInt{} _ = True
cmpMul _ EInt{} = False
cmpMul (ERat i) (ERat j) = abs i < abs j
cmpMul ERat{} _ = True
cmpMul _ ERat{} = False
cmpMul (EAbs _ a) (EAbs _ b) = cmpMul a b
cmpMul EAbs{} _ = True
cmpMul _ EAbs{} = False
cmpMul (ESgn a) (ESgn b) = cmpMul a b
cmpMul ESgn{} _ = True
cmpMul _ ESgn{} = False
cmpMul (EMul _ a b) (EMul _ x y) | a == x = cmpMul b y | otherwise = cmpMul a x
cmpMul EMul{} _ = True
cmpMul _ EMul{} = False
cmpMul (EAdd _ a b) (EAdd _ x y) | a == x = cmpMul b y | otherwise = cmpMul a x
cmpMul EAdd{} _ = True
cmpMul _ EAdd{} = False
cmpMul a@EVar{} b@EVar{} = a < b
cmpMul EVar{} _ = True
cmpMul _ EVar{} = False
cmpMul a b = a < b

-- var < abs < sgn < mul < add < |int| < |rat|
cmpAdd :: Ord a => Expr a -> Expr a -> Bool
cmpAdd a@EVar{} b@EVar{} = a < b
cmpAdd EVar{} _ = True
cmpAdd _ EVar{} = False
cmpAdd (EAbs _ a) (EAbs _ b) = cmpAdd a b
cmpAdd EAbs{} _ = True
cmpAdd _ EAbs{} = False
cmpAdd (ESgn a) (ESgn b) = cmpAdd a b
cmpAdd ESgn{} _ = True
cmpAdd _ ESgn{} = False
cmpAdd (EMul _ a b) (EMul _ x y) | a == x = cmpAdd b y | otherwise = cmpAdd a x
cmpAdd EMul{} _ = True
cmpAdd _ EMul{} = False
cmpAdd (EAdd _ a b) (EAdd _ x y) | a == x = cmpAdd b y | otherwise = cmpAdd a x
cmpAdd EAdd{} _ = True
cmpAdd _ EAdd{} = False
cmpAdd (EInt i) (EInt j) = abs i < abs j
cmpAdd EInt{} _ = True
cmpAdd _ EInt{} = False
cmpAdd (ERat i) (ERat j) = abs i < abs j
cmpAdd ERat{} _ = True
cmpAdd _ ERat{} = False
cmpAdd a b = a < b


rule :: Ord a => Expr a -> Maybe (Expr a)

-- identity

rule (EAdd _ (EInt 0) b) = Just b
rule (EAdd _ b (EInt 0)) = Just b
rule (EAdd _ (ERat 0) b) = Just b
rule (EAdd _ b (ERat 0)) = Just b

rule (EMul _ (EInt 1) b) = Just b
rule (EMul _ b (EInt 1)) = Just b
rule (EMul _ (ERat 1) b) = Just b
rule (EMul _ b (ERat 1)) = Just b

-- zero

rule (EMul _ (EInt 0) _) = Just 0
rule (EMul _ _ (EInt 0)) = Just 0

-- negate

rule (ENeg t a) = Just $ EMul t (EInt (-1)) a

-- distribute

rule (EMul t a (EAdd _ b c)) = Just $ EAdd t (EMul t a b) (EMul t a c)
rule (EMul t (EAdd _ a b) c) = Just $ EAdd t (EMul t a c) (EMul t b c)

-- combine constants

rule (EMul _ (EInt a) (EInt b)) = Just $ EInt (a * b)
rule (EMul t (EInt a) (EMul _ (EInt b) c)) = Just $ EMul t (EInt (a * b)) c
rule (EMul _ (EInt a) (ERat b)) = Just $ ERat $ fromInteger a * b
rule (EMul t (EInt a) (EMul _ (ERat b) c)) = Just $ EMul t (ERat (fromInteger a * b)) c
rule (EMul _ (ERat a) (ERat b)) = Just $ ERat (a * b)
rule (EMul t (ERat a) (EMul _ (ERat b) c)) = Just $ EMul t (ERat (a * b)) c

-- combine abs*abs

rule (EMul t (EAbs _ a) (EAbs _ b))
  | a == b = Just $ EMul t a b
  | otherwise = Just $ EAbs t (EMul t a b)

rule (EMul t (EAbs _ a) (EMul _ (EAbs _ b) c))
  | a == b = Just $ EMul t (EMul t a b) c
  | otherwise = Just $ EMul t (EAbs t (EMul t a b)) c

-- associate

rule (EAdd t (EAdd _ a b) c) = Just $ EAdd t a (EAdd t b c)
rule (EMul t (EMul _ a b) c) = Just $ EMul t a (EMul t b c)

-- sort

rule (EMul t a (EMul _ b c)) | cmpMul b a = Just $ EMul t b (EMul t a c)
rule (EAdd t a (EAdd _ b c)) | cmpAdd b a = Just $ EAdd t b (EAdd t a c)
rule (EMul t a (EAbs s b)) = Just $ EMul t (EAbs s b) a

-- undistribute int

rule (EAdd _ (EMul _ (EInt a) x) (EAdd _ (EMul _ (EInt b) y) c)) | x == y = Just $ fromInteger (a + b) * x + c
rule (EAdd _ x (EAdd _ (EMul _ (EInt b) y) c)) | x == y = Just $ fromInteger (1 + b) * x + c
rule (EAdd _ (EMul _ (EInt a) x) (EAdd _ y c)) | x == y = Just $ fromInteger (a + 1) * x + c

rule (EAdd _ (EMul _ (EInt a) x) (EMul _ (EInt b) y)) | x == y = Just $ fromInteger (a + b) * x
rule (EAdd _ x (EMul _ (EInt b) y)) | x == y = Just $ fromInteger (1 + b) * x
rule (EAdd _ (EMul _ (EInt a) x) y) | x == y = Just $ fromInteger (a + 1) * x

rule (EAdd _ x (EAdd _ y c)) | x == y = Just $ 2 * x + c
rule (EAdd _ x y) | x == y = Just $ 2 * x

-- misc

rule (EAbs _ (EMul _ (EInt a) b)) = Just $ fromInteger (abs a) * abs b
rule (ESgn (EMul _ a b)) = Just $ signum a * signum b
rule (ESgn (EInt a)) = Just $ EInt (signum a)
rule (ESgn (EAbs _ _)) = Just $ EInt 1

-- functor

rule (EAdd _ a b) = case (rule a, rule b) of
  (Nothing, Nothing) -> Nothing
  (ma, mb) -> Just $ fromMaybe a ma + fromMaybe b mb
rule (EMul _ a b) = case (rule a, rule b) of
  (Nothing, Nothing) -> Nothing
  (ma, mb) -> Just $ fromMaybe a ma * fromMaybe b mb
rule (EDiffAbs _ a b) = case (rule a, rule b) of
  (Nothing, Nothing) -> Nothing
  (ma, mb) -> Just $ diffabs (fromMaybe a ma) (fromMaybe b mb)
rule (EInv _ a) = recip <$> rule a
rule (EAbs _ a) = abs <$> rule a
rule (ESgn   a) = signum <$> rule a
rule (ESin _ a) = sin <$> rule a
rule (ECos _ a) = cos <$> rule a
rule (ETan _ a) = tan <$> rule a
rule (ESinh _ a) = sinh <$> rule a
rule (ECosh _ a) = cosh <$> rule a
rule (ETanh _ a) = tanh <$> rule a
rule (ESqrt _ a) = sqrt <$> rule a

-- sin + sin

rule (EAdd _ (ESin _ a) (ESin _ b)) = Just $ 2 * sin ((a + b) / 2) * cos ((a - b) / 2)
rule (EAdd _ (ESin _ a) (EAdd _ (ESin _ b) c)) = Just $ 2 * sin ((a + b) / 2) * cos ((a - b) / 2) + c

-- cos + cos

rule (EAdd _ (ECos _ a) (ECos _ b)) = Just $ 2 * cos ((a + b) / 2) * cos ((a - b) / 2)
rule (EAdd _ (ECos _ a) (EAdd _ (ECos _ b) c)) = Just $ 2 * cos ((a + b) / 2) * cos ((a - b) / 2) + c

-- tan + tan

rule (EAdd _ (ETan _ a) (ETan _ b)) = Just $ sin (a + b) / (cos a * cos b)
rule (EAdd _ (ETan _ a) (EAdd _ (ETan _ b) c)) = Just $ sin (a + b) / (cos a * cos b) + c

-- sinh + sinh

rule (EAdd _ (ESinh _ a) (ESinh _ b)) = Just $ 2 * sinh ((a + b) / 2) * cosh ((a - b) / 2)
rule (EAdd _ (ESinh _ a) (EAdd _ (ESinh _ b) c)) = Just $ 2 * sinh ((a + b) / 2) * cosh ((a - b) / 2) + c

-- cosh + cosh

rule (EAdd _ (ECosh _ a) (ECosh _ b)) = Just $ 2 * cosh ((a + b) / 2) * cosh ((a - b) / 2)
rule (EAdd _ (ECosh _ a) (EAdd _ (ECosh _ b) c)) = Just $ 2 * cosh ((a + b) / 2) * cosh ((a - b) / 2) + c

-- tanh + tanh

rule (EAdd _ (ETanh _ a) (ETanh _ b)) = Just $ sinh (a + b) / (cosh a * cosh b)
rule (EAdd _ (ETanh _ a) (EAdd _ (ETanh _ b) c)) = Just $ sinh (a + b) / (cosh a * cosh b) + c

-- irreducible

rule _ = Nothing

rule1 :: Expr a -> Expr a

rule1 n@(EInt _) = n
rule1 n@(ERat _) = n
rule1 v@(EVar _ _) = 1 * v
rule1 (ENeg _ a) = negate (rule1 a)
rule1 (EInv _ a) = recip (rule1 a)
rule1 (EAdd _ a b) = rule1 a + rule1 b
rule1 (EMul _ a b) = rule1 a * rule1 b
rule1 (EAbs _ a) = abs (rule1 a)
rule1 (ESin _ a) = sin (rule1 a)
rule1 (ECos _ a) = cos (rule1 a)
rule1 (ETan _ a) = tan (rule1 a)
rule1 (ESinh _ a) = sinh (rule1 a)
rule1 (ECosh _ a) = cosh (rule1 a)
rule1 (ETanh _ a) = tanh (rule1 a)
rule1 n@(EPi _) = n
rule1 (EExp _ a) = exp (rule1 a)
rule1 (EExpM1 _ a) = expm1 (rule1 a)
rule1 (ELog _ a) = log (rule1 a)
rule1 (ELog1P _ a) = log1p (rule1 a)
rule1 (EFloat a) = EFloat (rule1 a)
rule1 (ESgn   a) = signum (rule1 a)
rule1 (ESqrt _ a) = sqrt (rule1 a)
rule1 (EASin _ a) = asin (rule1 a)
rule1 (EACos _ a) = acos (rule1 a)
rule1 (EATan _ a) = atan (rule1 a)
rule1 (EASinh _ a) = asinh (rule1 a)
rule1 (EACosh _ a) = acosh (rule1 a)
rule1 (EATanh _ a) = atanh (rule1 a)
rule1 (EFloor t a) = EFloor t (rule1 a)
rule1 (ELess a b) = rule1 a `ELess` rule1 b
rule1 (ELessEqual a b) = rule1 a `ELessEqual` rule1 b
rule1 (EEqual a b) = rule1 a `EEqual` rule1 b
rule1 (EOr a b) = rule1 a `EOr` rule1 b
rule1 (EAnd a b) = rule1 a `EAnd` rule1 b
rule1 (ERead a b) = a `ERead` rule1 b
rule1 (EDiffAbs _ a b) = 1 * diffabs (rule1 a) (rule1 b)
rule1 (EIf t a b) = EIf (rule1 t) (rule1 a) (rule1 b)

simplify :: Ord a => Expr a -> Expr a
simplify e = case rule e of
  Nothing -> e
  Just f -> simplify f

type SAtoms t = SSum (SProduct (SAtom t))
data SSum t = SSum{ getSSum :: [t] } deriving (Eq, Ord)
data SProduct t = SProduct{ getSProduct :: [t] } deriving (Eq, Ord)
data SAtom t
  = SRat   Rational
  | SPi    NType
  | SVar   NType t
  | SSgn         (SAtoms t)
  | SInv   NType (SAtoms t)
  | SAbs   NType (SAtoms t)
  | SSin   NType (SAtoms t)
  | SCos   NType (SAtoms t)
  | STan   NType (SAtoms t)
  | SSinh  NType (SAtoms t)
  | SCosh  NType (SAtoms t)
  | STanh  NType (SAtoms t)
  | SExp NType (SAtoms t)
  | SExpM1 NType (SAtoms t)
  | SLog NType (SAtoms t)
  | SLog1P NType (SAtoms t)
  | SFloat       (SAtoms t)
  | SSqrt  NType (SAtoms t)
  | SASin  NType (SAtoms t)
  | SACos  NType (SAtoms t)
  | SATan  NType (SAtoms t)
  | SASinh NType (SAtoms t)
  | SACosh NType (SAtoms t)
  | SATanh NType (SAtoms t)
  | SDiffAbs NType (SAtoms t) (SAtoms t)
  | SRead t Integer
  deriving (Eq, Ord)

atoms :: Show t => Expr t -> SAtoms t
atoms (EInt     n) = SSum [SProduct [SRat (fromInteger n)]]
atoms (ERat     n) = SSum [SProduct [SRat n]]
atoms (EVar   t v) = SSum [SProduct [SRat 1, SVar   t v]]
atoms (EPi    t  ) = SSum [SProduct [SRat 1, SPi    t  ]]
atoms (ENeg   _ a) = SSum [SProduct (SRat (-1) : getSProduct ax) | ax <- getSSum (atoms a)]
atoms (EInv   t a) = SSum [SProduct [SRat 1, SInv   t (atoms a)]]
atoms (ESgn     a) = SSum [SProduct [SRat 1, SSgn     (atoms a)]]
atoms (EAbs   t a) = SSum [SProduct [SRat 1, SAbs   t (atoms a)]]
atoms (ESin   t a) = SSum [SProduct [SRat 1, SSin   t (atoms a)]]
atoms (ECos   t a) = SSum [SProduct [SRat 1, SCos   t (atoms a)]]
atoms (ETan   t a) = SSum [SProduct [SRat 1, STan   t (atoms a)]]
atoms (ESinh  t a) = SSum [SProduct [SRat 1, SSinh  t (atoms a)]]
atoms (ECosh  t a) = SSum [SProduct [SRat 1, SCosh  t (atoms a)]]
atoms (ETanh  t a) = SSum [SProduct [SRat 1, STanh  t (atoms a)]]
atoms (EExp t a) = SSum [SProduct [SRat 1, SExp t (atoms a)]]
atoms (EExpM1 t a) = SSum [SProduct [SRat 1, SExpM1 t (atoms a)]]
atoms (ELog t a) = SSum [SProduct [SRat 1, SLog t (atoms a)]]
atoms (ELog1P t a) = SSum [SProduct [SRat 1, SLog1P t (atoms a)]]
atoms (EFloat   a) = SSum [SProduct [SRat 1, SFloat   (atoms a)]]
atoms (ESqrt  t a) = SSum [SProduct [SRat 1, SSqrt  t (atoms a)]]
atoms (EASin  t a) = SSum [SProduct [SRat 1, SASin  t (atoms a)]]
atoms (EACos  t a) = SSum [SProduct [SRat 1, SACos  t (atoms a)]]
atoms (EATan  t a) = SSum [SProduct [SRat 1, SATan  t (atoms a)]]
atoms (EASinh t a) = SSum [SProduct [SRat 1, SASinh t (atoms a)]]
atoms (EACosh t a) = SSum [SProduct [SRat 1, SACosh t (atoms a)]]
atoms (EATanh t a) = SSum [SProduct [SRat 1, SATanh t (atoms a)]]
atoms (EDiffAbs t a b) = SSum [SProduct [SRat 1, SDiffAbs t (atoms a) (atoms b)]]
atoms (EAdd _ a b) = SSum (getSSum (atoms a) ++ getSSum (atoms b))
atoms (EMul _ a b) = SSum [SProduct $ [SRat 1] ++ getSProduct ax ++ getSProduct bx | ax <- getSSum (atoms a), bx <- getSSum (atoms b) ]
atoms (ERead (EVar NFloatPtr p) (EInt n)) = SSum [SProduct [SRead p n]]

orderS :: Ord t => SAtoms t -> SAtoms t
orderS = SSum . sort . map orderP . getSSum
orderP :: Ord t => SProduct (SAtom t) -> SProduct (SAtom t)
orderP = SProduct . sort . map order . getSProduct
order :: Ord t => SAtom t -> SAtom t
order n@(SRat _) = n
order n@(SPi _) = n
order n@(SVar _ _) = n
order (SSgn t) = SSgn $ orderS t
order (SInv   s t) = SInv   s $ orderS t
order (SAbs   s t) = SAbs   s $ orderS t
order (SSin   s t) = SSin   s $ orderS t
order (SCos   s t) = SCos   s $ orderS t
order (STan   s t) = STan   s $ orderS t
order (SSinh  s t) = SSinh  s $ orderS t
order (SCosh  s t) = SCosh  s $ orderS t
order (STanh  s t) = STanh  s $ orderS t
order (SExp s t) = SExp s $ orderS t
order (SExpM1 s t) = SExpM1 s $ orderS t
order (SLog s t) = SLog s $ orderS t
order (SLog1P s t) = SLog1P s $ orderS t
order (SFloat   t) = SFloat   $ orderS t
order (SSqrt  s t) = SSqrt  s $ orderS t
order (SASin  s t) = SASin  s $ orderS t
order (SACos  s t) = SACos  s $ orderS t
order (SATan  s t) = SATan  s $ orderS t
order (SASinh s t) = SASinh s $ orderS t
order (SACosh s t) = SACosh s $ orderS t
order (SATanh s t) = SATanh s $ orderS t
order (SDiffAbs s a b) = SDiffAbs s (orderS a) (orderS b)
order n@(SRead _ _) = n

fromS :: SAtoms t -> Expr t
fromS = sum . map fromP . getSSum
fromP :: SProduct (SAtom t) -> Expr t
fromP = product . map from . getSProduct
from :: SAtom t -> Expr t
from (SRat n)
  | denominator n == 1 = EInt (numerator n)
  | otherwise = ERat n
from (SPi t) = EPi t
from (SInv   s t) = EInv   s $ fromS t
from (SVar t v) = EVar t v
from (SSgn t) = ESgn $ fromS t
from (SAbs   s t) = EAbs   s $ fromS t
from (SSin   s t) = ESin   s $ fromS t
from (SCos   s t) = ECos   s $ fromS t
from (STan   s t) = ETan   s $ fromS t
from (SSinh  s t) = ESinh  s $ fromS t
from (SCosh  s t) = ECosh  s $ fromS t
from (STanh  s t) = ETanh  s $ fromS t
from (SExp s t) = EExp s $ fromS t
from (SExpM1 s t) = EExpM1 s $ fromS t
from (SLog s t) = ELog s $ fromS t
from (SLog1P s t) = ELog1P s $ fromS t
from (SFloat  t) = EFloat   $ fromS t
from (SSqrt  s t) = ESqrt  s $ fromS t
from (SASin  s t) = EASin  s $ fromS t
from (SACos  s t) = EACos  s $ fromS t
from (SATan  s t) = EATan  s $ fromS t
from (SASinh s t) = EASinh s $ fromS t
from (SACosh s t) = EACosh s $ fromS t
from (SATanh s t) = EATanh s $ fromS t
from (SDiffAbs s a b) = EDiffAbs s (fromS a) (fromS b)
from (SRead p n) = ERead (EVar NFloatPtr p) (EInt n)

collapseS :: Ord t => SAtoms t -> SAtoms t
collapseS (SSum bs) = SSum
  [ SProduct (SRat v : qs)
  | (qs, v) <- filter ((/= 0) . snd) . M.toList . M.fromListWith (+) $
      [ (ps, u) | x <- map collapseP bs, let SProduct (SRat u : ps) = x ]
  ]
collapseP :: Ord t => SProduct (SAtom t) -> SProduct (SAtom t)
collapseP (SProduct (SRat a : SRat b : cs)) = collapseP (SProduct (SRat (a * b) : cs))
collapseP (SProduct (SRat a : cs)) = SProduct (case getSProduct (collapseP (SProduct cs)) of (SRat b : ds) ->  SRat (a * b) : ds)
collapseP (SProduct cs) = SProduct (SRat 1 : map collapse cs)
collapse :: Ord t => SAtom t -> SAtom t
collapse (SRat n) = SRat n
collapse (SPi t) = SPi t
collapse (SInv   s t) = SInv   s $ collapseS t
collapse (SVar t v) = SVar t v
collapse (SSgn t) = SSgn $ collapseS t
collapse (SAbs   s t) = SAbs   s $ collapseS t
collapse (SSin   s t) = SSin   s $ collapseS t
collapse (SCos   s t) = SCos   s $ collapseS t
collapse (STan   s t) = STan   s $ collapseS t
collapse (SSinh  s t) = SSinh  s $ collapseS t
collapse (SCosh  s t) = SCosh  s $ collapseS t
collapse (STanh  s t) = STanh  s $ collapseS t
collapse (SExp s t) = SExp s $ collapseS t
collapse (SLog s t) = SLog s $ collapseS t
collapse (SExpM1 s t) = SExpM1 s $ collapseS t
collapse (SLog1P s t) = SLog1P s $ collapseS t
collapse (SFloat   t) = SFloat   $ collapseS t
collapse (SSqrt  s t) = SSqrt  s $ collapseS t
collapse (SASin  s t) = SASin  s $ collapseS t
collapse (SACos  s t) = SACos  s $ collapseS t
collapse (SATan  s t) = SATan  s $ collapseS t
collapse (SASinh s t) = SASinh s $ collapseS t
collapse (SACosh s t) = SACosh s $ collapseS t
collapse (SATanh s t) = SATanh s $ collapseS t
collapse (SDiffAbs s a b) = SDiffAbs s (collapseS a) (collapseS b)
collapse (SRead p n) = SRead p n

optimize :: Expr String -> Expr String
optimize = simplify . rule1 . fromS . collapseS . orderS . atoms . simplify . rule1

{-
undistribute :: Expr String -> Maybe (Expr String)
undistribute (EAdd _ (EMul _ x1 y1) (EMul _ x2 y2)) | x1 == x2 = Just $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ y1 x1) (EMul _ x2 y2)) | x1 == x2 = Just $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ x1 y1) (EMul _ y2 x2)) | x1 == x2 = Just $ x1 * (y1 + y2)
undistribute (EAdd _ (EMul _ y1 x1) (EMul _ y2 x2)) | x1 == x2 = Just $ x1 * (y1 + y2)
undistribute (EAdd _ (EDiv _ y1 x1) (EDiv _ y2 x2)) | x1 == x2 = Just $ (y1 + y2) / x1
undistribute _ = Nothing
-}
