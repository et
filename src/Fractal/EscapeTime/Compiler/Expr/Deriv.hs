{-
et -- escape time fractals
Copyright (C) 2018,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.Expr.Deriv (ddx) where

import Fractal.EscapeTime.Compiler.Expr.AST

ddx :: Eq a => (Expr a -> Expr a -> Expr a) -> Expr a -> Expr a -> Expr a
ddx _ _ (EInt _) = 0
ddx _ _ (ERat _) = 0
ddx d x a@(EVar _ _) = d x a
ddx d x (ENeg _ a) = negate (ddx d x a)
ddx d x (EInv _ a) = negate (ddx d x a) / a * a
ddx d x (EAdd _ a b) = ddx d x a + ddx d x b
ddx d x (EMul _ a b) = ddx d x a * b + a * ddx d x b
ddx d x (EAbs _ a) = signum a * ddx d x a
ddx d x (ESin _ a) =  cos a * ddx d x a
ddx d x (ECos _ a) = -sin a * ddx d x a
ddx d x (ETan _ a) = ddx d x a / cos a ^ 2
ddx d x (ESinh _ a) = cosh a * ddx d x a
ddx d x (ECosh _ a) = sinh a * ddx d x a
ddx d x (ETanh _ a) = ddx d x a / cosh a ^ 2
ddx _ _ (EPi _) = 0
ddx d x (EExp _ a) = exp a * ddx d x a
ddx d x (EExpM1 _ a) = exp a * ddx d x a
ddx d x (ELog _ a) = ddx d x a / a
ddx d x (ELog1P _ a) = ddx d x a / (1 + a)
ddx d x (EFloat a) = EFloat $ ddx d x a
ddx _ _ (ESgn   _) = 0
ddx d x (ESqrt _ a) = ddx d x a / (2 * sqrt a)
ddx d x (EASin _ a) =  ddx d x a / sqrt (1 - x * x)
ddx d x (EACos _ a) = -ddx d x a / sqrt (1 - x * x)
ddx d x (EATan _ a) = ddx d x a / (1 + x * x)
ddx d x (EASinh _ a) = ddx d x a / sqrt (x * x + 1)
ddx d x (EACosh _ a) = ddx d x a / sqrt (x * x - 1)
ddx d x (EATanh _ a) = ddx d x a / (1 - x * x)
ddx _ _ (EFloor _ _) = 0
ddx d x (EDiffAbs _ a b) = ddx d x (abs (a + b)) - ddx d x (abs a)
ddx d x (EIf t a b) = EIf t (ddx d x a) (ddx d x b)
ddx _ _ _ = error "Fractal.EscapeTime.Compiler.Expr.Deriv.ddx: cant differentiate"
