{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Transform
  ( Transform()
  , transform
  , getTransform
  , identity
  , rotation
  , scaleX
  , shearX
  , inverse
  , compose
  , apply
  , Point
  , fromPoints
  ) where

import Data.Word (Word32)
import Data.Bits (xor, shiftL, shiftR)

type Point = (Double, Double)

newtype Transform = Transform (Double, Double, Double, Double)

transform :: Double -> Double -> Double -> Double -> Maybe Transform
transform a b c d =
  let det = a * d - b * c
      s = recip . sqrt . abs $ det
      t@(sa, sb, sc, sd) = (s * a, s * b, s * c, s * d)
      f x = not (isNaN x) && not (isInfinite x)
  in  if all f [sa, sb, sc, sd] then Just (Transform t) else Nothing

getTransform :: Transform -> (Double, Double, Double, Double)
getTransform (Transform t) = t

identity :: Transform
identity = Transform (1, 0, 0, 1)

rotation :: Double -> Transform
rotation t = let c = cos t ; s = sin t in Transform (c, -s, s, c)

scaleX :: Double -> Maybe Transform
scaleX x = transform x 0 0 1

shearX :: Double -> Maybe Transform
shearX x = transform 1 x 0 1

inverse :: Transform -> Transform
inverse (Transform (a, b, c, d)) = Transform (d, -c, -b, a)

compose :: Transform -> Transform -> Transform
compose (Transform (a, b, c, d)) (Transform (x, y, z, w))
  = Transform (a * x + b * z, a * y + b * w, c * x + d * z, c * y + d * w)

apply :: Transform -> Point -> Point
apply (Transform (a, b, c, d)) (x, y) = (a * x + b * y, c * x + d * y)

fromPoints :: Point -> Point -> Point -> Maybe Transform
fromPoints (ox, oy) (ax, ay) (bx, by) = transform (ax - ox) (ay - oy) (bx - ox) (by - oy)
