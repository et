{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Loader.Load (load, libext) where

import Foreign (Ptr, castFunPtrToPtr, nullPtr)
import Control.Exception (catch, SomeException)
import System.Posix.DynamicLinker (dlopen, dlsym, dlerror, dlclose, RTLDFlags(RTLD_NOW))

import Fractal.EscapeTime.Runtime.Loader.Raw (S_Formula)

libext :: String
libext = ".so"

load :: FilePath -> IO (Either String (IO (), Ptr S_Formula))
load path = catchAny (do
  dl <- dlopen path [RTLD_NOW]
  p <- castFunPtrToPtr <$> dlsym dl "et"
  if p == nullPtr
  then Left <$> dlerror
  else return $ Right (dlclose dl, p)) (\e -> return (Left ("exception: " ++ show e)))

catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch
