{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE RecordWildCards #-}

module Fractal.EscapeTime.Runtime.Metadata
  ( Metadata(..)
  , toString
  , fromString
  ) where

import Text.Read (readMaybe)
import Numeric.Rounded.Simple (Rounded, RoundingMode(TowardNearest), read', show', exponent')

data Metadata = Metadata
  { metadataFormula :: [String]
  , metadataP, metadataQ :: Int
  , metadataD, metadataE :: Double
  , metadataA, metadataB, metadataR :: Rounded
  , metadataT :: (Double, Double, Double, Double)
  , metadataN :: Int
  , metadataDEWeight :: Double
  }

toString :: Metadata -> String
toString Metadata{..} =
  unlines metadataFormula ++ "\n" ++
  "p=" ++ show metadataP ++ "\n" ++
  "q=" ++ show metadataQ ++ "\n" ++
  "d=" ++ show metadataD ++ "\n" ++
  "e=" ++ show metadataE ++ "\n" ++
  "a=" ++ show' metadataA ++ "\n" ++
  "b=" ++ show' metadataB ++ "\n" ++
  "r=" ++ show' metadataR ++ "\n" ++
  "t=" ++ show metadataT ++ "\n" ++
  "n=" ++ show metadataN ++ "\n" ++
  "\n" ++
  "de=" ++ show metadataDEWeight ++ "\n"

fromString :: String -> Maybe Metadata
fromString s =
  case break null (lines s) of
    (metadataFormula, "":('p':'=':sp):('q':'=':sq):('d':'=':sd):('e':'=':se):('a':'=':sa):('b':'=':sb):('r':'=':sr):('t':'=':st):('n':'=':sn):"":('d':'e':'=':sde):[]) -> do
      metadataP <- readMaybe sp
      metadataQ <- readMaybe sq
      metadataD <- readMaybe sd
      metadataE <- readMaybe se
      metadataR <- Just $ read' TowardNearest 53 sr -- FIXME
      let prec = max 53 (53 - exponent' metadataR)
      metadataA <- Just $ read' TowardNearest prec sa -- FIXME
      metadataB <- Just $ read' TowardNearest prec sb -- FIXME
      metadataT <- readMaybe st
      metadataN <- readMaybe sn
      metadataDEWeight <- readMaybe sde
      return Metadata{..}
    _ -> Nothing
