{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Main (main) where

import Control.Exception (Exception, catch, throwIO)
import Control.Monad (forM_, unless, when)
import Data.Word (Word8)
import Foreign (peek, poke, malloc, free, sizeOf)
import Foreign.C.Types (CInt)
import System.Environment (getArgs, getProgName)
import System.Exit (exitSuccess, exitFailure)
import System.IO (hPutStrLn, hPutBuf, withBinaryFile, IOMode(WriteMode), stderr)
import Control.Concurrent.Async (async, wait)
import qualified Data.Vector.Storable.Mutable as VM
import qualified Data.Vector.Storable as V
import qualified Data.ByteString.Lazy as BS
import Codec.Picture (generateImage, PixelYA8(..), PixelRGB8(..), PixelRGBA8(..))
import Codec.Picture.Png (encodePngWithMetadata)
import Codec.Picture.Metadata (Keys(Software, Comment, ColorSpace), singleton, ColorSpace(SRGB))

import qualified Data.Binary as B
import qualified Codec.Picture as J
import qualified Codec.Picture.Metadata as J
import qualified Codec.Picture.Png.Internal.Metadata as J

import Numeric.Rounded.Simple (scaleFloat', exponent')
import System.Directory (createDirectoryIfMissing)
import System.Environment.XDG.BaseDir (getUserCacheDir)
import System.FilePath ((</>))
import Fractal.EscapeTime.Runtime.Compiler (compileAndLoad)
import Fractal.EscapeTime.Runtime.Loader (close)
import Fractal.EscapeTime.Runtime.Metadata as M
import Fractal.EscapeTime.Runtime.Render
import Fractal.EscapeTime.Runtime.Render.Transform (transform)

saveRAW :: Bool
saveRAW = False

main :: IO ()
main = do
  args <- getArgs
  case args of
    [outfile,sw,sh,infile] -> do
      w <- readIO sw
      h <- readIO sh
      raw <- BS.readFile infile
      case (J.lookup J.Comment . J.extractMetadatas . B.decode $ raw) >>= M.fromString of
        Nothing -> hPutStrLn stderr "error: could not parse metadata from input file"
        Just m -> main' outfile w h m
    [outfile,sw,sh,sre,sim,sr,sn,saa,sab,sba,sbb,sd,se,sp,sq,formula,sweight] -> do
      w <- readIO sw
      h <- readIO sh
      View a b r <- readViewIO sre sim sr
      maxiters <- readIO sn
      aa <- readIO saa
      ab <- readIO sab
      ba <- readIO sba
      bb <- readIO sbb
      d <- readIO sd
      e <- readIO se
      p <- readIO sp
      q <- readIO sq
      weight <- readIO sweight
      let m = Metadata (lines formula) p q d e a b r (aa, ab, ba, bb) maxiters weight
      main' outfile w h m
    _ -> do
      prog <- getProgName
      hPutStrLn stderr $ "usage: " ++ prog ++ " out.png width height input-with-metadata.png"
      hPutStrLn stderr $ "usage: " ++ prog ++ " out.png width height real imag radius maxiters aa ab ba bb d e p q formula weight"
  exitFailure

main' :: FilePath -> Int -> Int -> Metadata -> IO ()
main' outfile w h m@(Metadata formula p q d e a b r (aa, ab, ba, bb) maxiters weight) = do
  case transform aa ab ba bb of
    Nothing -> hPutStrLn stderr "error: singular transform"
    Just tr -> do
      progress "compiling..."
      cacheDir <- getUserCacheDir "et"
      let libDir = cacheDir </> "lib"
      createDirectoryIfMissing True libDir
      result <- compileAndLoad libDir (unlines formula) p q
      case result of
        Left msg -> hPutStrLn stderr msg
        Right et -> do
          let er = 10000
          bufr <- buffer w h 4 (-1)
          out <- if saveRAW then Left <$> buffer w h 1 0 else Right <$> buffer w h 3 0
          let prog = Nothing
          progress $ "rendering ..."
          (_cancelR, waitR) <- render et er maxiters d e (View a b r) bufr tr prog
          okR <- waitR
          unless okR exitFailure
          case out of
            Left raw -> do
              progress "extracting ..."
              (_cancelC, waitC) <- extract bufr raw
              okC <- waitC
              unless okC exitFailure
              progress "exporting ..."
              writeRAW outfile raw
            Right ppm -> do
              progress "colouring ..."
              (_cancelC, waitC) <- colour bufr ppm (realToFrac weight)
              okC <- waitC
              unless okC exitFailure
              progress "exporting ..."
              let comment = M.toString m
              writePNG comment outfile ppm
          progress "unloading..."
          close et
          exitSuccess

{-
-- zoom in animation:
forM_ ([exponent' r - ceiling (logBase 2 (fromIntegral h)) .. 0] `zip` [0..]) $ \(ex, fr) -> do
  let outfile = (++ ".png") . reverse . take 4 . (++ "000") . reverse . show $ fr
      r' = scaleFloat' (-ex) r
  ...
-}

progress :: String -> IO ()
progress = hPutStrLn stderr

writeRAW :: FilePath -> Buffer Float -> IO ()
writeRAW file (Buffer w h c v) = do
  withBinaryFile file WriteMode $ \hd -> do
    VM.unsafeWith v $ \p -> do
      hPutBuf hd p (w * h * c * sizeOf (0 :: Float))

writePNG :: String -> FilePath -> Buffer Word8 -> IO ()
writePNG comment outfile (Buffer w h 1 v) = do
  u <- V.freeze v
  let image = generateImage (\x y -> let ix = (y * w + x) * 1 in (u V.! (ix + 0))) w h
  BS.writeFile outfile (encodePngWithMetadata (mconcat [singleton Software "et-0", singleton Comment comment, singleton ColorSpace SRGB]) image)
writePNG comment outfile (Buffer w h 2 v) = do
  u <- V.freeze v
  let image = generateImage (\x y -> let ix = (y * w + x) * 2 in PixelYA8 (u V.! (ix + 0)) (u V.! (ix + 1))) w h
  BS.writeFile outfile (encodePngWithMetadata (mconcat [singleton Software "et-0", singleton Comment comment, singleton ColorSpace SRGB]) image)
writePNG comment outfile (Buffer w h 3 v) = do
  u <- V.freeze v
  let image = generateImage (\x y -> let ix = (y * w + x) * 3 in PixelRGB8 (u V.! (ix + 0)) (u V.! (ix + 1)) (u V.! (ix + 2))) w h
  BS.writeFile outfile (encodePngWithMetadata (mconcat [singleton Software "et-0", singleton Comment comment, singleton ColorSpace SRGB]) image)
writePNG comment outfile (Buffer w h 4 v) = do
  u <- V.freeze v
  let image = generateImage (\x y -> let ix = (y * w + x) * 4 in PixelRGBA8 (u V.! (ix + 0)) (u V.! (ix + 1)) (u V.! (ix + 2)) (u V.! (ix + 3))) w h
  BS.writeFile outfile (encodePngWithMetadata (mconcat [singleton Software "et-0", singleton Comment comment, singleton ColorSpace SRGB]) image)
writePNG _ _ _ = fail "writePNG: bad number of channels"

colour :: Buffer Float -> Buffer Word8 -> Float -> IO (IO Bool, IO Bool)
colour (Buffer sw sh sc@4 output) (Buffer iw ih ic@3 image) weight | sw == iw && sh == ih = do
  runningP <- malloc
  poke runningP (1 :: CInt)
  a <- async $ Control.Exception.catch (do
    forM_ [ 0 .. sh - 1 ] $ \j -> forM_ [ 0 .. sw - 1 ] $ \i -> do
      running <- peek runningP
      when (running == 0) $ throwIO Cancelled
      de <- VM.read output ((j * sw + i) * sc + 0)
      let grey x = (x, x, x)
          (r, g, b)
            | de > 0 = grey (round . (\z -> if isNaN z || isInfinite z || z < 0 then 0 else z) $ 255 * tanh (clamp (2 + log de * weight) 0 8))
            | de == 0 = (0, 0, 0)
            | de < 0 = (0, 0, 0)
      VM.write image ((j * iw + i) * ic + 0) r
      VM.write image ((j * iw + i) * ic + 1) g
      VM.write image ((j * iw + i) * ic + 2) b
    return True
    ) (\Cancelled -> return False)
  return  ( do{ poke runningP 0 ; r <- wait a ; free runningP ; return r }
          , do{                   r <- wait a ; free runningP ; return r }
          )
colour _ _ _ = return (return False, return False)

extract :: Buffer Float -> Buffer Float -> IO (IO Bool, IO Bool)
extract (Buffer sw sh sc@4 output) (Buffer iw ih ic@1 image) | sw == iw && sh == ih = do
  runningP <- malloc
  poke runningP (1 :: CInt)
  a <- async $ Control.Exception.catch (do
    forM_ [ 0 .. sh - 1 ] $ \j -> forM_ [ 0 .. sw - 1 ] $ \i -> do
      running <- peek runningP
      when (running == 0) $ throwIO Cancelled
      de <- VM.read output ((j * sw + i) * sc + 0)
      VM.write image ((j * iw + i) * ic + 0) de
    return True
    ) (\Cancelled -> return False)
  return  ( do{ poke runningP 0 ; r <- wait a ; free runningP ; return r }
          , do{                   r <- wait a ; free runningP ; return r }
          )
extract _ _ = return (return False, return False)

data Cancelled = Cancelled deriving Show
instance Exception Cancelled

clamp :: Ord a => a -> a -> a -> a
clamp x lo hi = lo `max` x `min` hi
