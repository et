{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Coord (Coord, coord, UnCoord, unCoord) where

import Fractal.EscapeTime.Runtime.Render.Dither
import Fractal.EscapeTime.Runtime.Render.Transform

type Coord t = Transform -> t -> Int -> Int -> Int -> Int -> (t, t)
type UnCoord t = Transform -> t -> Int -> Int -> t -> t -> Maybe (Int, Int)

coord :: RealFrac t => Dither -> Coord t
coord d t px w h i j =
  let (u, v) = dither d i j
      (x, y) = apply t (u - fromIntegral w / 2, v - fromIntegral h / 2)
  in  (px * realToFrac x, px * realToFrac y)

unCoord :: RealFrac t => UnCoord t
unCoord t px w h x0 y0 =
  let x = realToFrac (x0 / px)
      y = realToFrac (y0 / px)
      (u, v) = apply (inverse t) (x, y)
      i = u + fromIntegral w / 2
      j = v + fromIntegral h / 2
      limit = 2^24
  in  if abs i > limit || abs j > limit then Nothing else Just (round i, round j)
