{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.Size (size) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

-- FIXME this needs the degree of each stanza to be the *lowest* non-linear power
size :: [(Int, R2Formula (Expr String))] -> String
size fs = runCompile_ FDouble . function NBool "size" [(NInt, "P"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NReal, "R"), (NFloatPtr, "M"), (NVolatileIntPtr, "RUNNING")] $ \s -> do
  let degree = exp $ sum (map (log . fromIntegral . fst) fs) / fromIntegral (length fs) :: Double
      deg = degree / (degree - 1)
      deg' = if isNaN deg || isInfinite deg then 0 else deg
      p = EVar NInt "P"
      d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
      a = EVar NReal "A"
      b = EVar NReal "B"
      r = EVar NReal "R"
      m = EVar NFloatPtr "M"
      runningP = EVar NVolatileIntPtr "RUNNING"
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  deg <- float
  deg .= realToFrac deg'
  x <- real
  y <- real
  lxa <- real
  lxb <- real
  lya <- real
  lyb <- real
  bxa <- real
  bxb <- real
  bya <- real
  byb <- real
  x .= 0
  y .= 0
  lxa .= 1
  lxb .= 0
  lya .= 0
  lyb .= 1
  bxa .= 1
  bxb .= 0
  bya .= 0
  byb .= 1
  -- loop one period
  j <- int
  j .= 1
  k <- int
  k .= 0
  running <- bool
  running .= read runningP 0
  while_ (j `ELess` p `EAnd` running) $ do
    -- alloc z, l
    xn <- real
    yn <- real
    lxan <- real
    lxbn <- real
    lyan <- real
    lybn <- real
    -- calc z, l
    let g (_, f) j0 kn = (,) j0 $ do
          let R2 gx gy = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize gx
              fy = optimize gy
              fdxa = ddx d x fx
              fdxb = ddx d y fx
              fdya = ddx d x fy
              fdyb = ddx d y fy
              d v u
                | u == x && v == x = lxa
                | u == x && v == y = lxb
                | u == y && v == x = lya
                | u == y && v == y = lyb
                | u == v = 1
                | otherwise = 0
          xn .= fx
          yn .= fy
          x .= xn
          y .= yn
          lxan .= optimize fdxa
          lxbn .= optimize fdxb
          lyan .= optimize fdya
          lybn .= optimize fdyb
          lxa .= lxan
          lxb .= lxbn
          lya .= lyan
          lyb .= lybn
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- step b
    det <- real
    det .= lxa * lyb - lxb * lya
    bxa .= bxa + lyb / det
    bxb .= bxb - lxb / det
    bya .= bya - lya / det
    byb .= byb + lxa / det
    -- loop
    j .= j + 1
    running .= read runningP 0
  -- l^d b
  l <- real
  beta <- real
  l .= sqrt (abs (lxa * lyb - lxb * lya))
  beta .= sqrt (abs (bxa * byb - bxb * bya))
  llb <- real
  llb .= exp (log l * deg) * beta
  r .= 1 / llb
  byb .= byb / beta
  bxb .= negate bxb / beta
  bya .= negate bya / beta
  bxa .= bxa / beta
  write m 0 byb
  write m 1 bxb
  write m 2 bya
  write m 3 bxa
  s .= running
  return_ s
