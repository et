{-
et -- escape time fractals
Copyright (C) 2018,2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.Perturbation (perturbation) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

-- FIXME this needs the degree of each stanza to be the *highest* non-linear power
perturbation :: [(Int, R2Formula (Expr String))] -> String
perturbation fs = concat . flip map [FDouble] $ \t -> runCompile_ t . function NBool ("perturbation" ++ fSuffix t) [(NInt, "M"), (NInt, "N"), (NFloat, "R2"), (NFloatPtr, "H"), (NFloat, "d"), (NFloat, "e"), (NFloat, "A"), (NFloat, "B"), (NFloatPtr, "XYZS"), (NFloatPtr, "OUT"), (NVolatileIntPtr, "RUNNING")] $ \running -> do
  let m = EVar NInt "M"
      n = EVar NInt "N"
      r2 = EVar NFloat "R2"
      hp = EVar NFloatPtr "H"
      d_ = EVar NFloat "d"
      e_ = EVar NFloat "e"
      a = EVar NFloat "A"
      b = EVar NFloat "B"
      xyzs = EVar NFloatPtr "XYZS"
      out = EVar NFloatPtr "OUT"
      runningP = EVar NVolatileIntPtr "RUNNING"
      logdegree :: Double
      logdegree = sum (map (log . fromIntegral . fst) fs) / fromIntegral (length fs)
  absa <- float
  absb <- float
  absa .= abs a
  absb .= abs b
  daa <- float
  dab <- float
  dba <- float
  dbb <- float
  daa .= read hp 0
  dab .= read hp 1
  dba .= read hp 2
  dbb .= read hp 3
  h2 <- float
  e2 <- float
  h2 .= daa * dbb - dab * dba
  e2 .= realToFrac ((2 ** negate 24 :: Double) ^ (2 :: Int))
  one_over_e2 <- float
  one_over_e2 .= realToFrac (1 :: Double) / e2
  x <- float
  y <- float
  dxa <- float
  dxb <- float
  dya <- float
  dyb <- float
  let a0 = EVar NFloat "A0"
      b0 = EVar NFloat "B0"
      aa = EVar NFloat "aa"
      bb = EVar NFloat "bb"
  x0 <- float
  y0 <- float
  z0 <- float
  xx <- float
  yy <- float
  x .= 0
  y .= 0
  dxa .= 0
  dxb .= 0
  dya .= 0
  dyb .= 0
  i <- int
  z2 <- float
  i .= 0
  x0 .= read xyzs (3 * i + 0)
  y0 .= read xyzs (3 * i + 1)
  z0 .= read xyzs (3 * i + 2)
  xx .= x0 + x
  yy .= y0 + y
  z2 .= xx*xx + yy*yy
  minz2 <- float
  minz2 .= r2
  period <- int
  period .= 0
  unglitched <- bool
  unglitched .= 1
  running .= read runningP 0
  k <- int
  k .= 0
  da <- float
  db <- float
  _ <- while_ (ELess i m `EAnd` ELess z2 r2 `EAnd` unglitched `EAnd` running) $ do
    -- allocate next
    xn <- float
    yn <- float
    dxan <- float
    dxbn <- float
    dyan <- float
    dybn <- float
    -- do formula
    let g (_, f) j kn = (,) j $ do
          let R2 fx0 fy0 = f (R2 d_ e_) (R2 a' b') (R2 x' y')
              a' = EVar NFloat "a"
              b' = EVar NFloat "b"
              x' = EVar NFloat "x"
              y' = EVar NFloat "y"
              vs =
                [ ("d", (d_, 0))
                , ("e", (e_, 0))
                , ("a", (a0, a))
                , ("b", (b0, b))
                , ("x", (x0, x))
                , ("y", (y0, y))
                ]
              fx = perturb vs fx0
              fy = perturb vs fy0
              R2 gxx gyy = f (R2 d_ e_) (R2 aa bb) (R2 xx yy)
              fxx = optimize gxx
              fyy = optimize gyy
              j_xa = optimize $ ddx d aa fxx
              j_xb = optimize $ ddx d bb fxx
              j_ya = optimize $ ddx d aa fyy
              j_yb = optimize $ ddx d bb fyy
              d v u
                | u == xx && v == aa = dxa
                | u == xx && v == bb = dxb
                | u == yy && v == aa = dya
                | u == yy && v == bb = dyb
                | u == aa && v == aa = daa
                | u == aa && v == bb = dab
                | u == bb && v == aa = dba
                | u == bb && v == bb = dbb
                | u == v = 1
                | otherwise = 0
              d0 v u
                | u == v = 1
                | otherwise = 0
              m_xa0 = optimize $ ddx d0 a fx
              m_xb0 = optimize $ ddx d0 b fx
              m_ya0 = optimize $ ddx d0 a fy
              m_yb0 = optimize $ ddx d0 b fy
              k_xX0 = optimize $ ddx d0 x0 fx
              k_xY0 = optimize $ ddx d0 y0 fx
              k_yX0 = optimize $ ddx d0 x0 fy
              k_yY0 = optimize $ ddx d0 y0 fy
              l_xx0 = optimize $ ddx d0 x fx
              l_xy0 = optimize $ ddx d0 y fx
              l_yx0 = optimize $ ddx d0 x fy
              l_yy0 = optimize $ ddx d0 y fy
          comment $ "fx   = " ++ pretty fx
          comment $ "fy   = " ++ pretty fy
          comment $ "j_xa = " ++ pretty j_xa
          comment $ "j_xb = " ++ pretty j_xb
          comment $ "j_ya = " ++ pretty j_ya
          comment $ "j_yb = " ++ pretty j_yb
          comment $ "m_xa = " ++ pretty m_xa0
          comment $ "m_xb = " ++ pretty m_xb0
          comment $ "m_ya = " ++ pretty m_ya0
          comment $ "m_yb = " ++ pretty m_yb0
          comment $ "k_xX = " ++ pretty k_xX0
          comment $ "k_xY = " ++ pretty k_xY0
          comment $ "k_yX = " ++ pretty k_yX0
          comment $ "k_yY = " ++ pretty k_yY0
          comment $ "l_xx = " ++ pretty l_xx0
          comment $ "l_xy = " ++ pretty l_xy0
          comment $ "l_yx = " ++ pretty l_yx0
          comment $ "l_yy = " ++ pretty l_yy0
          xn .= fx
          yn .= fy
          dxan .= j_xa
          dxbn .= j_xb
          dyan .= j_ya
          dybn .= j_yb
          m_xa <- float
          m_xb <- float
          m_ya <- float
          m_yb <- float
          m_xa .= abs m_xa0
          m_xb .= abs m_xb0
          m_ya .= abs m_ya0
          m_yb .= abs m_yb0
          k_xX <- float
          k_xY <- float
          k_yX <- float
          k_yY <- float
          k_xX .= abs k_xX0
          k_xY .= abs k_xY0
          k_yX .= abs k_yX0
          k_yY .= abs k_yY0
          l_xx <- float
          l_xy <- float
          l_yx <- float
          l_yy <- float
          l_xx .= abs l_xx0
          l_xy .= abs l_xy0
          l_yx .= abs l_yx0
          l_yy .= abs l_yy0
          delta_x <- float
          delta_y <- float
          absx <- float
          absy <- float
          absx .= abs x
          absy .= abs y
          absx0 <- float
          absy0 <- float
          absx0 .= abs x0
          absy0 .= abs y0
          delta_x .= l_xx * absx + l_xy * absy + k_xX * absx0 + k_xY * absy0 + m_xa * absa + m_xb * absb
          delta_y .= l_yx * absx + l_yy * absy + k_yX * absx0 + k_yY * absy0 + m_ya * absa + m_yb * absb
          det <- float
          det .= dxan * dybn - dxbn * dyan
          delta_a <- float
          delta_b <- float
          delta_a .= (  dybn * delta_x - dyan * delta_y) / det
          delta_b .= (- dxbn * delta_x + dxan * delta_y) / det
          delta_c2 <- float
          delta_c2 .= delta_a * delta_a + delta_b * delta_b
          -- gerrit 2017-12-27 <https://fractalforums.org/fractal-mathematics-and-new-theories/28/perturbation-theory/487/msg3212#msg3212>
          unglitched .= ELess delta_c2 one_over_e2
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- update next
    x .= xn
    y .= yn
    dxa .= dxan
    dxb .= dxbn
    dya .= dyan
    dyb .= dybn
    -- update loop
    i .= i + 1
    x0 .= read xyzs (3 * i + 0)
    y0 .= read xyzs (3 * i + 1)
    z0 .= read xyzs (3 * i + 2)
    xx .= x0 + x
    yy .= y0 + y
    z2 .= xx*xx + yy*yy
    running .= read runningP 0
    if_ (ELess z2 minz2 `EAnd` ELessEqual (i + 4) m) (do
      minz2 .= z2
      period .= i
      det <- float
      det .= dxa * dyb - dxb * dya
      da .= a - (  dyb * x - dya * y) / det
      db .= b - (- dxb * x + dxa * y) / det) (return ())
  de <- float
  nc <- float
  ni <- float
  nf <- float
  nc .= 0
  ni .= 0
  nf .= 0
  _ <- if_ (ENot unglitched `EOr` (((ELessEqual r2 z2 `EAnd` ELess m (i + 4)) `EOr` (ELess z2 r2 `EAnd` EEqual i m)) `EAnd` ELess m n))
          (do{ {- glitch -}  ni .= i ; if_ (ELessEqual r2 z2 `EAnd` ELess m (i + 4) `EAnd` ELess m n)
            (do{ {- ref escaped -} nf .= negate 2 ; de .= negate minz2 })
            (do  {- criterion -}
              nf .= negate 3
              de .= negate z2
              det <- float
              det .= dxa * dyb - dxb * dya
              da .= a - (  dyb * x - dya * y) / det
              db .= b - (- dxb * x + dxa * y) / det)
          }) $ if_ (ELess z2 r2) (de .= 0 {- interior -}) $ do {- escaped -}
    u <- float
    v <- float
    u .= xx * dxa + yy * dya
    v .= xx * dxb + yy * dyb
    de .= z2 * log (sqrt z2) / sqrt (u*u + v*v)
    nc .= if isInfinite logdegree || isNaN logdegree || 0 == logdegree then i else i + 1 - log (log (sqrt z2)) / realToFrac logdegree
    ni .= EFloor NFloat nc
    nf .= nc - ni
  write out 0 de
  write out 1 period
  write out 2 ni
  write out 3 nf
  -- (a, b) - J^-1 (x, y)  -- Newton step to Pauldelbrot glitch center
  write out 4 da
  write out 5 db
  return_ running
