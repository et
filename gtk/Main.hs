{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}

module Main (main) where

import Control.Exception (catch, SomeException)
import Control.Monad (replicateM, when)
import Data.Bits ((.|.), bit)
import Data.IORef (IORef, newIORef, readIORef, writeIORef, atomicModifyIORef)
import Data.Word (Word8)
import Text.Read (readMaybe)
import Foreign (alloca, allocaArray, malloc, free, with, withArray, peek, peekArray, poke, pokeElemOff, castPtr, nullPtr)
import Foreign.C (withCString, peekCString)
import Foreign.ForeignPtr (mallocForeignPtrBytes, withForeignPtr)
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import System.Random (randomIO, randomRIO)

import Control.Concurrent (forkIO)
import Control.Concurrent.Async (async, wait)
import qualified Data.ByteString.Lazy as BS
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Vector.Storable.Mutable (unsafeWith)
import qualified Data.Vector.Storable.Mutable as V
import Numeric.Rounded.Simple (Rounded, RoundingMode(TowardNearest), precision, exponent', encodeFloat', precRound, fromDouble, add_, sub_, mul_, exp_, log_, show')

import Data.Time (getZonedTime, formatTime, defaultTimeLocale)
import System.Directory (createDirectoryIfMissing)
import System.Environment.XDG.BaseDir (getUserCacheDir)
import System.Environment.XDG.UserDir (getUserDir)
import System.FilePath ((</>), takeDirectory)

import GI.GLib (timeoutAdd, pattern PRIORITY_DEFAULT)
import GI.Gdk (keyvalName, EventMask(EventMaskScrollMask, EventMaskPointerMotionMask), ScrollDirection(ScrollDirectionUp, ScrollDirectionDown), threadsAddIdle)
import GI.Gtk hiding (main)
import qualified GI.GLib as GLib
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import Data.GI.Base
import GI.Gdk.Objects.GLContext (GLContext, gLContextGetUseEs, gLContextMakeCurrent, gLContextClearCurrent)
import Graphics.GL

import qualified Data.Binary as B
import qualified Codec.Picture as J
import qualified Codec.Picture.Metadata as J
import qualified Codec.Picture.Extra as J
import qualified Codec.Picture.Png.Internal.Metadata as J

import Fractal.EscapeTime.Formulas.Types (R2(R2))
import Fractal.EscapeTime.Runtime.Compiler (compileAndLoad)
import Fractal.EscapeTime.Runtime.Loader (ET(..))
import Fractal.EscapeTime.Runtime.Render (render)
import Fractal.EscapeTime.Runtime.Render.View
import Fractal.EscapeTime.Runtime.Render.Buffer
import Fractal.EscapeTime.Runtime.Render.Transform
import Fractal.EscapeTime.Runtime.Render.Progressive (Progressive, cacheProgressive)
import Fractal.EscapeTime.Runtime.Metadata as M
import Paths_et (getDataFileName, version)

catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

toGTK :: IO () -> IO ()
toGTK action = do
  Gdk.threadsAddIdle GLib.PRIORITY_DEFAULT $ do
    action
    return False
  return ()

data GUI = GUI
  { tmpDir :: FilePath
  , imagePrefix :: FilePath
  , sequenceNumber :: Int
  , formula :: ET
  , formulaName :: String
  , formulaSource :: [String]
  , formulaER :: Double
  , formulaP :: Int
  , formulaQ :: Int
  , iterations :: Int
  , location :: View
  , transformation :: Transform
  , renderBuffer :: Buffer Float
  , progress :: Maybe Progressive
  , imageWidth :: Int
  , imageHeight :: Int
  , displaySize :: Double
  , srcx0 :: Double
  , srcy0 :: Double
  , srcx1 :: Double
  , srcy1 :: Double
  , log2weight :: Double
  , cancel :: IO Bool
  , glarea :: GLArea
  , gl :: GUIGL
  }

data GUIGL = GUIGL
  { fbo, fbo2 :: GLuint
  , blit_vao :: GLuint
  , blit_p :: GLuint
  , blit_u_gamma :: GLint
  , bound_u :: GLint
  , colourize_vao :: GLuint
  , colourize_p :: GLuint
  , weight_u :: GLint
  , tex :: GLuint
  , ftex, ftex2 :: GLuint
  }
defaultGUIGL :: GUIGL
defaultGUIGL = GUIGL 0 0 0 0 (-1) (-1) 0 0 (-1) 0 0 0

defaultGUI :: IO (Either String GUI)
defaultGUI = do
  let name = "Burning Ship"
      source = ["z := (|x| + i |y|)^p + c"]
      er = 10000
      p = 2
      q = 1
      w = 1024
      h = 576
      maxiters = 256
      view = defaultView
      tr = identity
  cacheDir <- getUserCacheDir "et"
  picturesDir <- getUserDir "PICTURES"
  now <- getZonedTime
  let libDir = cacheDir </> "lib"
      locale = defaultTimeLocale
      imgPrefix = picturesDir </> "et" </> ("et-" ++ formatTime locale "%Y%m%dT%H%M%S%z" now ++ "-")
  createDirectoryIfMissing True libDir
  m <- compileAndLoad libDir (unlines (name:source)) p q
  case m of
    Left msg -> return (Left msg)
    Right et -> do
      buf <- buffer w h 4 (-1)
      mprog <- cacheProgressive w h
      let d_ = 1
          e_ = 0
      (cancelR, _waitR) <- render et er maxiters d_ e_ view buf tr mprog
      return $ Right GUI
        { tmpDir = libDir
        , imagePrefix = imgPrefix
        , sequenceNumber = 1
        , formula = et
        , formulaName = name
        , formulaSource = source
        , formulaER = er
        , formulaP = p
        , formulaQ = q
        , iterations = maxiters
        , location = view
        , transformation = tr
        , renderBuffer = buf
        , progress = mprog
        , imageWidth = w
        , imageHeight = h
        , displaySize = 1
        , srcx0 = 0
        , srcy0 = 0
        , srcx1 = fromIntegral w
        , srcy1 = fromIntegral h
        , log2weight = 0
        , cancel = cancelR
        , glarea = error "glarea"
        , gl = defaultGUIGL
        }

cancelRender :: IORef GUI -> IO ()
cancelRender guiR = do
  cancel' <- atomicModifyIORef guiR $ \g -> (g{ cancel = return False }, cancel g)
  cancel'
  return ()

startRender :: IORef GUI -> IO ()
startRender guiR = do
  cancelRender guiR
  gui <- readIORef guiR
  let View x y r = location gui
      d_ = 1
      e_ = 0
  print (show' x)
  print (show' y)
  print (show' r)
  (cancelR, _waitR) <- render (formula gui) (formulaER gui) (iterations gui) d_ e_ (location gui) (renderBuffer gui) (transformation gui) (progress gui)
  atomicModifyIORef guiR $ \g -> (g{ cancel = cancelR }, ())

type MenuBarDescr = [(Text, [(Text, Maybe (IO ()))])]

createMenuBar :: MenuBarDescr -> IO Gtk.MenuBar
createMenuBar descr
    = do bar <- menuBarNew
         mapM_ (createMenu bar) descr
         return bar
    where
      createMenu bar (name,items)
          = do menu <- menuNew
               item <- menuItemNewWithLabelOrMnemonic name
               menuItemSetSubmenu item (Just menu)
               menuShellAppend bar item
               mapM_ (createMenuItem menu) items
      createMenuItem menu (name,action)
          = do item <- menuItemNewWithLabelOrMnemonic name
               menuShellAppend menu item
               case action of
                 Just act -> onMenuItemActivate item act
                 Nothing  -> onMenuItemActivate item (return ())
      menuItemNewWithLabelOrMnemonic name
          | '_' `elem` T.unpack name = menuItemNewWithMnemonic name
          | otherwise                = menuItemNewWithLabel name

menuBarDescr :: IORef GUI -> Window -> MenuBarDescr
menuBarDescr guiR w
    = [ ("_File",   [ ("_Open", Just (fileOpen guiR w))
{-
                    , ("_Save", Nothing)
                    , ("Save _As", Nothing)
-}
                    , ("_Quit", Just mainQuit)
                    ]
        )
{-
      , ("_Edit",   [ ("_Undo", Nothing)
                    , ("_Redo", Nothing)
                    , ("_Copy", Nothing)
                    , ("_Paste", Nothing)
                    ]
        )
-}
      , ("_Image",  [ ("Save _As", Just (imageSaveAs guiR w))
                    , ("_Size", Just (imageSize guiR w))
{-
                    , ("_Colouring", Nothing)
-}
                    ]
        )
      , ("Fracta_l",[ ("_Formula", Just (formulaUI guiR w))
{-
                    , ("_Location", Nothing)
                    , ("_Iterations", Nothing)
                    , ("_Transform", Nothing)
-}
                    ]
        )
      , ("_Help",   [ ("_About", Just $ about w)
                    ]
        )
      ]

debug_program :: GLuint -> String -> IO ()
debug_program program name = do
  if program /= 0
  then do
    linked <- with GL_FALSE $ \p -> glGetProgramiv program GL_LINK_STATUS p >> peek p
    when (linked /= GL_TRUE) $ hPutStrLn stderr $ name ++ ": OpenGL shader program link failed"
    len <- with 0 $ \p -> glGetProgramiv program GL_INFO_LOG_LENGTH p >> peek p
    s <- allocaArray (fromIntegral len + 1) $ \p -> do
      glGetProgramInfoLog program len nullPtr p
      pokeElemOff p (fromIntegral len) 0
      peekCString p
    when (not (null s)) $ hPutStrLn stderr $ name ++ ": OpenGL shader program info log\n" ++ s
  else do
    hPutStrLn stderr $ name ++ ": OpenGL shader program creation failed"

debug_shader :: GLuint -> GLenum -> String -> IO ()
debug_shader shader typ name = do
  let tname = case typ of
        GL_VERTEX_SHADER -> "vertex"
        GL_FRAGMENT_SHADER-> "fragment"
        _ -> "unknown"
  if shader /= 0
  then do
    compiled <- with GL_FALSE $ \p -> glGetShaderiv shader GL_COMPILE_STATUS p >> peek p
    when (compiled /= GL_TRUE) $ hPutStrLn stderr $ name ++ ": OpenGL " ++ tname ++ " shader compile failed"
    len <- with 0 $ \p -> glGetShaderiv shader GL_INFO_LOG_LENGTH p >> peek p
    s <- allocaArray (fromIntegral len + 1) $ \p -> do
      glGetShaderInfoLog shader len nullPtr p
      pokeElemOff p (fromIntegral len) 0
      peekCString p
    when (not (null s)) $ hPutStrLn stderr $ name ++ ": OpenGL " ++ tname ++ " shader info log\n" ++ s
  else do
    hPutStrLn stderr $ name ++ ": OpenGL " ++ tname ++ " shader creation failed"

compile_shader :: GLuint -> GLenum -> String -> String -> IO ()
compile_shader program typ name source = do
  shader <- glCreateShader typ
  withCString source $ \p -> with p $ \pp -> glShaderSource shader 1 pp nullPtr
  glCompileShader shader
  debug_shader shader typ name
  glAttachShader program shader
  glDeleteShader shader

compile_program :: String -> String -> String -> IO GLuint
compile_program name vert frag = do
  program <- glCreateProgram
  compile_shader program GL_VERTEX_SHADER name vert
  compile_shader program GL_FRAGMENT_SHADER name frag
  withCString "vertexID" $ \s -> glBindAttribLocation program 0 s
  glLinkProgram program
  debug_program program name
  return program

blit_frag, blit_vert, simple_frag, simple_vert :: String

blit_vert = unlines
  [ "#version 130"
  , "uniform vec4 bounds;"
  , "in float vertexID;"
  , "out vec2 texCoord;"
  , "void main() {"
  , "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = bounds.xy; } else"
  , "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = bounds.zy; } else"
  , "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = bounds.xw; } else"
  , "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = bounds.zw; }"
  , "}"
  ]

blit_frag = unlines
  [ "#version 130"
  , "uniform sampler2D tex;"
  , "uniform bool gamma;"
  , "in vec2 texCoord;"
  , "out vec4 fragColor;"
  , "float sRGB(float c) {"
  , "  c = clamp(c, 0.0, 1.0);"
  , "  const float a = 0.055;"
  , "  if (c <= 0.0031308)"
  , "    return 12.92 * c;"
  , "  else"
  , "    return (1.0 + a) * pow(c, 1.0 / 2.4) - a;"
  , "}"
  , "void main() {"
  , "  vec4 t = texture(tex, texCoord);"
  , "  if (gamma) {"
  , "    t.r = sRGB(t.r);"
  , "    t.g = sRGB(t.g);"
  , "    t.b = sRGB(t.b);"
  , "  }"
  , "  fragColor = t;"
  , "}"
  ]

simple_vert = unlines
  [ "#version 130"
  , "in float vertexID;"
  , "out vec2 texCoord;"
  , "void main() {"
  , "  if (vertexID == 0.0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); texCoord = vec2(0.0, 1.0); } else"
  , "  if (vertexID == 1.0) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); texCoord = vec2(1.0, 1.0); } else"
  , "  if (vertexID == 2.0) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); texCoord = vec2(0.0, 0.0); } else"
  , "                       { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); texCoord = vec2(1.0, 0.0); }"
  , "}"
  ]

simple_frag = unlines
  [ "#version 130"
  , "uniform sampler2D tex;"
  , "uniform highp float weight;"
  , "in vec2 texCoord;"
  , "out vec4 fragColor;"
  , "// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl"
  , "vec3 hsv2rgb(vec3 c) {"
  , "  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);"
  , "  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);"
  , "  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);"
  , "}"
  , "void main() {"
  , "  vec2 dx = dFdx(texCoord);"
  , "  vec2 dy = dFdy(texCoord);"
  , "  vec4 me = texture(tex, texCoord);"
  , "  vec4 a = texture(tex, texCoord + dx);"
  , "  vec4 b = texture(tex, texCoord + dy);"
  , "  vec4 c = texture(tex, texCoord + dx + dy);"
  , "  float de = me.x;"
  , "  float s = /*de > 0.0 ? (me.y != c.y || a.y != b.y) ? 1.0 : 0.25 :*/ 0.0;"
  , "  float h = max(max(me.y, c.y), max(a.y, b.y)) / 24.618033988749893;"
  , "  h -= floor(h);"
  , "  float v = de > 0.0 ? tanh(clamp(2.0 + log(de / weight), 0.0, 8.0)) : 0.0;"
  , "  if (isnan(de) || isinf(de)) v = 0.0;"
  , "  bool glitch = de < 0.0;"
  , "  fragColor = vec4(glitch ? vec3(1.0, 0.0, 0.0) : hsv2rgb(vec3(h,s,v)), glitch ? 0.0 : 1.0);"
  , "}"
  ]

doRealize :: IORef GUI -> IO ()
doRealize guiR = do
  gui <- readIORef guiR
  gLAreaMakeCurrent $ glarea gui
  e <- gLAreaGetError $ glarea gui
  case e of
    Just err -> return ()
    _ -> do
      context <- gLAreaGetContext $ glarea gui
      es <- gLContextGetUseEs context
      if es
      then do
        return ()
      else do
        -- snapshot texture
        ftex <- with 0 $ \p -> glGenTextures 1 p >> peek p
        glActiveTexture GL_TEXTURE1
        glBindTexture GL_TEXTURE_2D ftex
        glTexImage2D GL_TEXTURE_2D 0 GL_SRGB8_ALPHA8 (fromIntegral (imageWidth gui)) (fromIntegral (imageHeight gui)) 0 GL_RGBA GL_UNSIGNED_BYTE nullPtr
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR_MIPMAP_LINEAR
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
        glGenerateMipmap GL_TEXTURE_2D
        -- snapshot texture
        ftex2 <- with 0 $ \p -> glGenTextures 1 p >> peek p
        glActiveTexture GL_TEXTURE1
        glBindTexture GL_TEXTURE_2D ftex2
        glTexImage2D GL_TEXTURE_2D 0 GL_SRGB8_ALPHA8 (fromIntegral (imageWidth gui)) (fromIntegral (imageHeight gui)) 0 GL_RGBA GL_UNSIGNED_BYTE nullPtr
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR_MIPMAP_LINEAR
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
        glGenerateMipmap GL_TEXTURE_2D
        -- snapshot fbo
        old_fbo <- with 0 $ \p -> glGetIntegerv GL_DRAW_FRAMEBUFFER_BINDING p >> peek p
        fbo <- with 0 $ \p -> glGenFramebuffers 1 p >> peek p
        glBindFramebuffer GL_DRAW_FRAMEBUFFER fbo
        glFramebufferTexture2D GL_DRAW_FRAMEBUFFER GL_COLOR_ATTACHMENT0 GL_TEXTURE_2D ftex2 0
        glClearColor 0 0 0 0
        glClear GL_COLOR_BUFFER_BIT
        glBindFramebuffer GL_DRAW_FRAMEBUFFER (fromIntegral old_fbo)
        glBindTexture GL_TEXTURE_2D 0
        -- snapshot fbo
        fbo2 <- with 0 $ \p -> glGenFramebuffers 1 p >> peek p
        glBindFramebuffer GL_DRAW_FRAMEBUFFER fbo2
        glFramebufferTexture2D GL_DRAW_FRAMEBUFFER GL_COLOR_ATTACHMENT0 GL_TEXTURE_2D ftex 0
        glClearColor 0 0 0 0
        glClear GL_COLOR_BUFFER_BIT
        glBindFramebuffer GL_DRAW_FRAMEBUFFER (fromIntegral old_fbo)
        glBindTexture GL_TEXTURE_2D 0
        -- image texture
        tex <- with 0 $ \p -> glGenTextures 1 p >> peek p
        glActiveTexture GL_TEXTURE0
        glBindTexture GL_TEXTURE_2D tex
        glTexImage2D GL_TEXTURE_2D 0 GL_RGBA32F (fromIntegral $ imageWidth gui) (fromIntegral $ imageHeight gui) 0 GL_RGBA GL_FLOAT nullPtr
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_MIRRORED_REPEAT
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_MIRRORED_REPEAT
        glBindTexture GL_TEXTURE_2D 0
        vbo <- with 0 $ \p -> glGenBuffers 1 p >> peek p
        glBindBuffer GL_ARRAY_BUFFER vbo
        withArray [0 .. 3 :: Word8] $ \p -> glBufferData GL_ARRAY_BUFFER 4 (castPtr p) GL_STATIC_DRAW
        -- blit shader
        blit_vao <- with 0 $ \p -> glGenVertexArrays 1 p >> peek p
        glBindVertexArray blit_vao
        blit_p <- compile_program "blit" blit_vert blit_frag
        glUseProgram blit_p
        withCString "tex" $ \s -> glGetUniformLocation blit_p s >>= \l -> glUniform1i l 1
        bound_u <- withCString "bounds" $ \s -> glGetUniformLocation blit_p s
        blit_u_gamma <- withCString "gamma" $ \s -> glGetUniformLocation blit_p s
        glVertexAttribPointer 0 1 GL_UNSIGNED_BYTE GL_FALSE 0 nullPtr
        glEnableVertexAttribArray 0
        glBindVertexArray 0
        glUseProgram 0
        -- colourize shader
        colourize_vao <- with 0 $ \p -> glGenVertexArrays 1 p >> peek p
        glBindVertexArray colourize_vao
        colourize_p <- compile_program "colourize" simple_vert simple_frag
        glUseProgram colourize_p
        weight_u <- withCString "weight" $ \s -> glGetUniformLocation colourize_p s
        glVertexAttribPointer 0 1 GL_UNSIGNED_BYTE GL_FALSE 0 nullPtr
        glEnableVertexAttribArray 0
        glBindVertexArray 0
        glUseProgram 0
        atomicModifyIORef guiR (\g -> (g{ gl = GUIGL{..} }, ()))
        return ()

doUnrealize :: IORef GUI -> IO ()
doUnrealize guiR = do
  gui <- readIORef guiR
  gLAreaMakeCurrent $ glarea gui
  e <- gLAreaGetError $ glarea gui
  case e of
    Just err -> return ()
    _ -> do
      return ()

doRender :: IORef GUI -> GLContext -> IO Bool
doRender guiR ctx = do
  gui@GUI{ gl = GUIGL{..} } <- readIORef guiR
  e <- gLAreaGetError $ glarea gui
  case e of
    Just err -> return False
    _ -> do
      old_fbo <- with 0 $ \p -> glGetIntegerv GL_DRAW_FRAMEBUFFER_BINDING p >> peek p
      glBindFramebuffer GL_DRAW_FRAMEBUFFER fbo
      glViewport 0 0 (fromIntegral $ imageWidth gui) (fromIntegral $ imageHeight gui)

      glClearColor 0 0 0 0
      glClear GL_COLOR_BUFFER_BIT
      glDisable GL_DEPTH_TEST
      glEnable GL_FRAMEBUFFER_SRGB
      glEnable GL_BLEND
      glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA

      glBindVertexArray blit_vao
      glUseProgram blit_p
      glUniform1i blit_u_gamma 0
      glActiveTexture GL_TEXTURE1
      glBindTexture GL_TEXTURE_2D ftex
      let w = fromIntegral (imageWidth gui)
          h = fromIntegral (imageHeight gui)
      glUniform4f bound_u (realToFrac $ srcx0 gui / w) (realToFrac $ srcy0 gui / h) (realToFrac $ srcx1 gui / w) (realToFrac $ srcy1 gui / h)
      glDrawArrays GL_TRIANGLE_STRIP 0 4
      glBindTexture GL_TEXTURE_2D 0
      glBindVertexArray 0
      glUseProgram 0

      glBindVertexArray colourize_vao
      glUseProgram colourize_p
      glActiveTexture GL_TEXTURE0
      glBindTexture GL_TEXTURE_2D tex
      unsafeWith (contents $ renderBuffer gui) $ \ptr ->
        glTexSubImage2D GL_TEXTURE_2D 0 0 0 (fromIntegral $ imageWidth gui) (fromIntegral $ imageHeight gui) GL_RGBA GL_FLOAT (castPtr ptr)
      glUniform1f weight_u (realToFrac $ 2 ** log2weight gui)
      glDrawArrays GL_TRIANGLE_STRIP 0 4
      glBindTexture GL_TEXTURE_2D 0
      glBindVertexArray 0
      glUseProgram 0

      glActiveTexture GL_TEXTURE1
      glBindTexture GL_TEXTURE_2D ftex2
      glGenerateMipmap GL_TEXTURE_2D

      glBindFramebuffer GL_FRAMEBUFFER (fromIntegral old_fbo)
      glBindVertexArray blit_vao
      glUseProgram blit_p
      glUniform1i blit_u_gamma 1
      glActiveTexture GL_TEXTURE1
      glBindTexture GL_TEXTURE_2D ftex
      let w = round $ displaySize gui * (fromIntegral $ imageWidth gui)
          h = round $ displaySize gui * (fromIntegral $ imageHeight gui)
      glViewport 0 0 w h
      glUniform4f bound_u 0 0 1 1
      glDrawArrays GL_TRIANGLE_STRIP 0 4
      glBindTexture GL_TEXTURE_2D 0
      glBindVertexArray 0
      glUseProgram 0

      atomicModifyIORef guiR $ \g -> (g
        { srcx0 = 0
        , srcx1 = fromIntegral (imageWidth gui)
        , srcy0 = 0
        , srcy1 = fromIntegral (imageHeight gui)
        , gl = (gl g)
          { fbo = fbo2
          , fbo2 = fbo
          , ftex = ftex2
          , ftex2 = ftex
          }
        }, ())
      glFlush
      e <- glGetError
      when (e /= 0) $ hPutStrLn stderr $ "OpenGL error " ++ show e
      return True

fromDouble' :: Double -> Rounded
fromDouble' = fromDouble TowardNearest 53

doZoom :: IORef GUI -> Double -> Double -> Double -> Double -> IO ()
doZoom guiR ex ey f g = do
  gui <- readIORef guiR
  let u = (ex / (displaySize gui * w) - 0.5) * 2 * w / h
      v = (ey / (displaySize gui * h) - 0.5) * 2
      vv = 1 - v
      (s, t) = apply (transformation gui) (u, v)
      dx = radius (location gui) .* fromDouble' s
      dy = radius (location gui) .* fromDouble' t
      r = radius (location gui) .* fromDouble' f
      prec = max 53 $ 53 - exponent' r
      (.+) = add_ TowardNearest prec
      (.-) = sub_ TowardNearest 53
      (.*) = mul_ TowardNearest 53
      cx = centerX (location gui) .+ (fromDouble' (1 - g) .* dx)
      cy = centerY (location gui) .+ (fromDouble' (1 - g) .* dy)
      x = ex / displaySize gui
      y = ey / displaySize gui
      yy = h - y
      w = fromIntegral (imageWidth gui)
      h = fromIntegral (imageHeight gui)
      (srcx0', srcx1', srcy0', srcy1')
        | g /= 0.0 = (g * (0 - x) + x, g * (w - x) + x, g * (0 - yy) + yy, g * (h - yy) + yy)
        | otherwise = (0 - (w / 2 - x), w - (w / 2 - x), 0 - (h / 2 - yy), h - (h / 2 - yy))
  atomicModifyIORef guiR $ \g -> (g{ location = View cx cy r, srcx0 = srcx0', srcx1 = srcx1', srcy0 = srcy0', srcy1 = srcy1' }, ())

doNewton :: IORef GUI -> Bool -> IO ()
doNewton guiR julia = do
  cancelRender guiR
  gui <- readIORef guiR
  runningP <- malloc
  poke runningP 1
  a <- async $ catchAny (do
    let View vx vy vr = location gui
        d_ = 1
        e_ = 0
    mp <- period_jsk (formula gui) (iterations gui) (formulaER gui * formulaER gui) d_ e_ vx vy vr (getTransform $ transformation gui) runningP
    case mp of
      Just p | p > 0 -> do
        print ("period", p)
        let iprec = precision vx * 4
            a0 = precRound TowardNearest iprec vx
            b0 = precRound TowardNearest iprec vy
            maxSteps = 16
        mn <- newton (formula gui) maxSteps p d_ e_ a0 b0 runningP
        case mn of
          Just (R2 a b) -> do
            print ("newton", show' a, show' b)
            ms <- size (formula gui) p d_ e_ a b runningP
            case ms of
              Just (s0, k@(a11, a12, a21, a22)) -> do
                print ("size", show' s0)
                print ("skew", k)
                let pow_ r p x y = exp_ r p (mul_ r p y (log_ r p x))
                    s | julia = pow_ TowardNearest 53 s0 (fromDouble' d)
                      | otherwise = precRound TowardNearest 53 s0
                    n = degree (formula gui)
                    d = (n + 1) * (n - 1) / (n * n)
                    ur = mul_ TowardNearest 53 s (fromDouble' 4)
                    sprec = max 53 (53 - exponent' ur)
                    ux = precRound TowardNearest sprec a
                    uy = precRound TowardNearest sprec b
                    view = View ux uy ur
                case transform a11 a12 a21 a22 of
                  Just t -> do
                    print ("transform", getTransform t)
                    threadsAddIdle PRIORITY_DEFAULT $ do
                      atomicModifyIORef guiR $ \g -> (g{ location = view, transformation = t }, ())
                      startRender guiR
                      return False
                    return True
                  _ -> return False
              _ -> return False
          _ -> return False
      _ -> return False
    ) (\_ -> return False)
  atomicModifyIORef guiR $ \g -> (g{ cancel = do{ poke runningP 0 ; r <- wait a ; free runningP ; return r } }, ())

doSkew :: IORef GUI -> Bool -> Bool -> IO ()
doSkew guiR useDZ useIT = do
  cancelRender guiR
  gui <- readIORef guiR
  runningP <- malloc
  poke runningP 1
  a <- async $ catchAny (do
    let View vx vy vr = location gui
        d_ = 1
        e_ = 0
    ms <- skew (formula gui) (iterations gui) {-(formulaER gui * formulaER gui)-} d_ e_ vx vy useDZ runningP
    case ms of
      Just k@(a11, a12, a21, a22) -> do
        print ("skew", k, a11 * a22 - a12 * a21)
        case if useIT then transform a11 a21 a12 a22 else transform a11 a12 a21 a22 of
          Just t -> do
            print ("transform", getTransform t)
            threadsAddIdle PRIORITY_DEFAULT $ do
              atomicModifyIORef guiR $ \g -> (g{ transformation = t }, ())
              startRender guiR
              return False
            return True
          _ -> return False
      _ -> return False
    ) (\_ -> return False)
  atomicModifyIORef guiR $ \g -> (g{ cancel = do{ poke runningP 0 ; r <- wait a ; free runningP ; return r } }, ())

doRotate :: IORef GUI -> Double -> IO ()
doRotate guiR theta = do
  cancelRender guiR
  gui <- readIORef guiR
  let c = cos theta
      s = sin theta
      (a11, a12, a21, a22) = getTransform (transformation gui)
      Just b = transform (c * a11 + s *  a12) (-s * a11 + c * a12) (c * a21 + s * a22) (-s * a21 + c * a22)
  print ("transform", getTransform b)
  atomicModifyIORef guiR $ \g -> (g{ transformation = b }, ())

imageSaveAs :: IORef GUI -> Window -> IO ()
imageSaveAs guiR w = do
  c <- fileChooserNativeNew (Just "Save Image") (Just w) FileChooserActionSave Nothing Nothing
  fileChooserSetDoOverwriteConfirmation c True
  f <- fileFilterNew
  fileFilterAddPattern f "*.png"
  fileFilterSetName f (Just "PNG Image")
  fileChooserAddFilter c f
  setNativeDialogModal c True
  r <- nativeDialogRun c
  when (r == fromIntegral (fromEnum ResponseTypeAccept)) $ do
    mpath <- fileChooserGetFilename c
    case mpath of
      Nothing -> return ()
      Just path -> doImageSave guiR path
  nativeDialogDestroy c

doImageSave :: IORef GUI -> String -> IO ()
doImageSave guiR path = do
  gui <- readIORef guiR
  fptr <- mallocForeignPtrBytes (imageWidth gui * imageHeight gui * 3)
  context <- gLAreaGetContext $ glarea gui
  gLContextMakeCurrent context
  glBindFramebuffer GL_READ_FRAMEBUFFER (fbo2 (gl gui))
  glPixelStorei GL_PACK_ALIGNMENT 1
  withForeignPtr fptr $ \ptr ->
    glReadPixels 0 0 (fromIntegral $ imageWidth gui) (fromIntegral $ imageHeight gui) GL_RGB GL_UNSIGNED_BYTE (castPtr ptr)
  gLContextClearCurrent
  let img :: J.Image J.PixelRGB8
      img = J.flipVertically $ J.imageFromUnsafePtr (imageWidth gui) (imageHeight gui) fptr
      View a b r = location gui
      comment = M.toString (Metadata
        (formulaName gui : formulaSource gui)
        (formulaP gui)
        (formulaQ gui)
        1
        0
        a
        b
        r
        (getTransform $ transformation gui)
        (iterations gui)
        (2 ** log2weight gui) )
      meta = J.insert J.ColorSpace J.SRGB . J.insert J.Comment comment $ J.singleton J.Software "et"
      png = J.encodePngWithMetadata meta img
  BS.writeFile path png

fileOpen :: IORef GUI -> Window -> IO ()
fileOpen guiR w = do
  c <- fileChooserNativeNew (Just "Open File") (Just w) FileChooserActionOpen Nothing Nothing
  f <- fileFilterNew
  fileFilterAddPattern f "*.png"
  fileFilterSetName f (Just "PNG Image")
  fileChooserAddFilter c f
  setNativeDialogModal c True
  r <- nativeDialogRun c
  when (r == fromIntegral (fromEnum ResponseTypeAccept)) $ do
    mpath <- fileChooserGetFilename c
    case mpath of
      Nothing -> return ()
      Just path -> doImageLoadMeta guiR path
  nativeDialogDestroy c

doImageLoadMeta :: IORef GUI -> String -> IO ()
doImageLoadMeta guiR path = do
  raw <- BS.readFile path
  case (J.lookup J.Comment . J.extractMetadatas . B.decode $ raw) >>= M.fromString of
    Nothing -> return ()
    Just m -> do
      gui <- readIORef guiR
      e <- compileAndLoad (tmpDir gui) (unlines (metadataFormula m)) (metadataP m) (metadataQ m)
      case e of
        Left msg -> hPutStrLn stderr msg >> return ()
        Right et -> do
          cancelRender guiR
          old_et <- atomicModifyIORef guiR $ \g -> (g
                { formulaName = head (metadataFormula m) -- FIXME
                , formulaSource = tail (metadataFormula m) -- FIXME
                , formulaP = metadataP m
                , formulaQ = metadataQ m
                , location = View (metadataA m) (metadataB m) (metadataR m)
                , transformation = case (case metadataT m of (a, b, c, d) -> transform a b c d) of Just t -> t -- FIXME
                , iterations = metadataN m
                , log2weight = logBase 2 $ metadataDEWeight m
                , formula = et
               }, formula g)
          close old_et
          startRender guiR
          return ()

imageSize :: IORef GUI -> Window -> IO ()
imageSize guiR mainWindow = do
  gui <- readIORef guiR
  dialog <- dialogNew
  windowSetTransientFor dialog (Just mainWindow)
  dialogAddButton dialog "_Ok" (fromIntegral (fromEnum ResponseTypeAccept))
  dialogAddButton dialog "_Cancel" (fromIntegral (fromEnum ResponseTypeReject))
  box <- dialogGetContentArea dialog
  grid <- gridNew
  eWidth  <- entryNew
  eHeight <- entryNew
  eSize   <- entryNew
  set eWidth  [ #inputPurpose := InputPurposeNumber, #widthChars := 6, #text := T.pack (show (imageWidth gui)) ]
  set eHeight [ #inputPurpose := InputPurposeNumber, #widthChars := 6, #text := T.pack (show (imageHeight gui)) ]
  set eSize   [ #inputPurpose := InputPurposeNumber, #widthChars := 6, #text := T.pack (show (displaySize gui)) ]
  lWidth  <- labelNewWithMnemonic $ Just "Image _Width"
  lHeight <- labelNewWithMnemonic $ Just "Image _Height"
  lSize   <- labelNewWithMnemonic $ Just "Display _Size"
  labelSetMnemonicWidget lWidth  $ Just eWidth
  labelSetMnemonicWidget lHeight $ Just eHeight
  labelSetMnemonicWidget lSize   $ Just eSize
  gridAttach grid lWidth  0 0 1 1
  gridAttach grid eWidth  1 0 1 1
  gridAttach grid lHeight 0 1 1 1
  gridAttach grid eHeight 1 1 1 1
  gridAttach grid lSize   0 2 1 1
  gridAttach grid eSize   1 2 1 1
  containerAdd box grid
  widgetShowAll dialog
  r <- dialogRun dialog
  when (r == fromIntegral (fromEnum ResponseTypeAccept)) $ do
    tw <- entryGetText eWidth
    th <- entryGetText eHeight
    ts <- entryGetText eSize
    case readMaybe (T.unpack tw) of
      Just w | w > 0 -> case readMaybe (T.unpack th) of
        Just h | h > 0 -> case readMaybe (T.unpack ts) of
          Just s | s > 0 -> doImageSize guiR w h s
          _ -> return ()
        _ -> return ()
      _ -> return ()
  widgetDestroy dialog

doImageSize :: IORef GUI -> Int -> Int -> Double -> IO ()
doImageSize guiR iw ih s = do
  gui <- readIORef guiR
  when (imageWidth gui /= iw || imageHeight gui /= ih) $ do
    -- stop rendering
    cancelRender guiR
    gui <- readIORef guiR
    -- resize textures
    context <- gLAreaGetContext $ glarea gui
    gLContextMakeCurrent context
    glActiveTexture GL_TEXTURE1
    glBindTexture GL_TEXTURE_2D (ftex (gl gui))
    glTexImage2D GL_TEXTURE_2D 0 GL_SRGB8_ALPHA8 (fromIntegral iw) (fromIntegral ih) 0 GL_RGBA GL_UNSIGNED_BYTE nullPtr
    glBindTexture GL_TEXTURE_2D (ftex2 (gl gui))
    glTexImage2D GL_TEXTURE_2D 0 GL_SRGB8_ALPHA8 (fromIntegral iw) (fromIntegral ih) 0 GL_RGBA GL_UNSIGNED_BYTE nullPtr
    glBindTexture GL_TEXTURE_2D 0
    glActiveTexture GL_TEXTURE0
    glBindTexture GL_TEXTURE_2D (tex (gl gui))
    glTexImage2D GL_TEXTURE_2D 0 GL_RGBA32F (fromIntegral iw) (fromIntegral ih) 0 GL_RGBA GL_FLOAT nullPtr
    glBindTexture GL_TEXTURE_2D 0
    -- update renderer
    buf <- buffer iw ih 4 (-1)
    mprog <- cacheProgressive iw ih
    atomicModifyIORef guiR $ \g -> (g
      { imageWidth = iw
      , imageHeight = ih
      , renderBuffer = buf
      , progress = mprog
      , srcx0 = 0
      , srcy0 = 0
      , srcx1 = fromIntegral iw
      , srcy1 = fromIntegral ih
      }, ())
  -- resize GUI
  gl <- atomicModifyIORef guiR $ \g -> (g
    { displaySize = s
    }, glarea g)
  let ww = round (s * fromIntegral iw)
      wh = round (s * fromIntegral ih)
  widgetSetSizeRequest gl ww wh
  -- restart rendering
  when (imageWidth gui /= iw || imageHeight gui /= ih) $ do
    startRender guiR

supersampling :: IORef GUI -> Double -> IO ()
supersampling guiR desiredSupersampling = do
  gui <- readIORef guiR
  let currentDisplaySize  = displaySize $ gui
      currentWindowWidth  = (currentDisplaySize *) . fromIntegral . imageWidth  $ gui
      currentWindowHeight = (currentDisplaySize *) . fromIntegral . imageHeight $ gui
      desiredDisplaySize  = recip desiredSupersampling
      desiredImageWidth   = round $ currentWindowWidth  * desiredSupersampling
      desiredImageHeight  = round $ currentWindowHeight * desiredSupersampling
  doImageSize guiR desiredImageWidth desiredImageHeight desiredDisplaySize

formulaUI :: IORef GUI -> Window -> IO ()
formulaUI guiR mainWindow = do
  gui <- readIORef guiR
  window <- windowNew WindowTypeToplevel
  windowSetTransientFor window (Just mainWindow)
  windowSetModal window True
  grid <- gridNew
  eName <- entryNew
  eSource <- textViewNew
  eP <- entryNew
  eQ <- entryNew
  eMessage <- textViewNew
  set eName [ #text := T.pack (formulaName gui) ]
  set eSource [ #editable := True ]
  sBuf <- get eSource #buffer
  set sBuf [ #text := T.pack (unlines (formulaSource gui)) ]
  set eP [ #text := T.pack (show $ formulaP gui), #inputPurpose := InputPurposeNumber ]
  set eQ [ #text := T.pack (show $ formulaQ gui), #inputPurpose := InputPurposeNumber ]
  set eMessage [ #editable := False ]
  mBuf <- get eMessage #buffer
  set mBuf [ #text := "..." ]
  lName <- labelNewWithMnemonic $ Just "_Name"
  lSource <- labelNewWithMnemonic $ Just "_Source"
  lP <- labelNewWithMnemonic $ Just "_P"
  lQ <- labelNewWithMnemonic $ Just "_Q"
  bRandom <- buttonNewWithMnemonic "_Random"
  bCompile <- buttonNewWithMnemonic "_Compile"
  lMessage <- labelNew $ Just "Messages"
  labelSetMnemonicWidget lName $ Just eName
  labelSetMnemonicWidget lSource $ Just eSource
  labelSetMnemonicWidget lP $ Just eP
  labelSetMnemonicWidget lQ $ Just eQ
  gridAttach grid lName    0 0 1 1
  gridAttach grid eName    1 0 1 1
  gridAttach grid lSource  0 1 1 1
  gridAttach grid eSource  1 1 1 1
  gridAttach grid lP       0 2 1 1
  gridAttach grid eP       1 2 1 1
  gridAttach grid lQ       0 3 1 1
  gridAttach grid eQ       1 3 1 1
  gridAttach grid bRandom  1 4 1 1
  gridAttach grid bCompile 1 5 1 1
  gridAttach grid lMessage 0 6 1 1
  gridAttach grid eMessage 1 6 1 1
  #add window grid

  on bRandom #clicked $ do
    let stanza = do
          x <- pure "x"
          y <- pure "y"
          b <- randomIO
          (x, y) <- pure $ if b then (y, x) else (x, y)
          b <- randomIO
          x <- pure $ if b then "|" ++ x ++ "|" else x
          b <- randomIO
          y <- pure $ if b then "|" ++ y ++ "|" else y
          b <- randomIO
          x <- pure $ if b then "(-" ++ x ++ ")" else x
          b <- randomIO
          y <- pure $ if b then "(-" ++ y ++ ")" else y
          pure $ "z := (" ++ x ++ " + i " ++ y ++ ")^p + c"
    n <- randomRIO (1, 4)
    s <- unlines <$> replicateM n stanza
    ebuf <- get eSource #buffer
    set ebuf [ #text := T.pack s ]

  on bCompile #clicked $ do
    ebuf <- get eMessage #buffer
    set ebuf [ #text := "...compiling..." ]
    name <- T.unpack <$> entryGetText eName
    sbuf <- get eSource #buffer
    msource <- get sbuf #text
    let source = case msource of
                    Nothing -> []
                    Just t -> lines (T.unpack t)
    sp <- T.unpack <$> entryGetText eP
    sq <- T.unpack <$> entryGetText eQ
    gui <- readIORef guiR
    case readMaybe sp of
      Just p -> case readMaybe sq of
        Just q -> do
         set bCompile [ #sensitive := False ]
         _ <- forkIO $ do
          e <- compileAndLoad (tmpDir gui) (unlines (name:source)) p q
          toGTK $ do
           set bCompile [ #sensitive := True ]
           case e of
            Left msg -> do
              set ebuf [ #text := T.pack msg ]
            Right et -> do
              cancelRender guiR
              old_et <- atomicModifyIORef guiR $ \g -> (g
                { formulaName = name
                , formulaSource = source
                , formulaP = p
                , formulaQ = q
                , formula = et
                }, formula g)
              close old_et
              set ebuf [ #text := "compiled" ]
              startRender guiR
         return ()
        _ -> do
          set ebuf [ #text := "failed to parse number: q" ]
      _ -> do
        set ebuf [ #text := "failed to parse number: p" ]
  widgetShowAll window

about :: Window -> IO ()
about w = do
  a <- new AboutDialog []
  agpl30 <- getDataFileName "LICENSE.md" >>= T.readFile
  set a
    [ #authors := ["Claude Heiland-Allen <claude@mathr.co.uk>"]
    , #comments := "Escape-time fractals."
    , #copyright := "(c) 2018,2019 Claude Heiland-Allen"
--    , #licenseType := LicenseCustom -- LicenseAgpl30 -- needs too-recent GTK library
    , #license := agpl30
    , #programName := "et"
    , #version := "0~git"
    , #website := "https://mathr.co.uk/et"
    , #websiteLabel := "mathr.co.uk/et"
    ]
  windowSetTransientFor a (Just w)
  dialogRun a
  widgetDestroy a
  return ()

main :: IO ()
main = do
  Gtk.init Nothing

  egui <- defaultGUI
  gui <- case egui of
    Left msg -> hPutStrLn stderr msg >> exitFailure
    Right gui -> return gui
  guiR <- newIORef gui

  getDataFileName "et.png" >>= windowSetDefaultIconFromFile
  window <- windowNew WindowTypeToplevel
  box <- boxNew OrientationVertical 0
  #add window box
  menuBar <- createMenuBar (menuBarDescr guiR window)
  setWindowTitle window "et"
  #add box menuBar

  on window #keyPressEvent $ \event -> do
    name <- event `get` #keyval >>= keyvalName
    case name of
      Just "Escape" -> cancelRender guiR >> return True
      Just "F5" -> startRender guiR >> return True
      Just "s" -> do
        (prefix, seqNum) <- atomicModifyIORef guiR $ \g -> (g{ sequenceNumber = sequenceNumber g + 1 }, (imagePrefix g, sequenceNumber g))
        let seqStr = reverse . take 4 . (++ "0000") . reverse . show $ seqNum
            path = prefix ++ seqStr ++ ".png"
            picturesDir = takeDirectory path
        createDirectoryIfMissing True picturesDir
        doImageSave guiR path
        return True
      Just "1" -> supersampling guiR 1 >> return True
      Just "2" -> supersampling guiR 2 >> return True
      Just "3" -> supersampling guiR 3 >> return True
      Just "4" -> supersampling guiR 4 >> return True
      Just "5" -> supersampling guiR 5 >> return True
      Just "6" -> supersampling guiR 6 >> return True
      Just "7" -> supersampling guiR 7 >> return True
      Just "8" -> supersampling guiR 8 >> return True
      Just "equal" -> do
        atomicModifyIORef guiR $ \g -> (g{ iterations = min (bit 30) (iterations g * 2) }, ())
        startRender guiR
        return True
      Just "minus" -> do
        atomicModifyIORef guiR $ \g -> (g{ iterations = max 1 (iterations g `div` 2) }, ())
        startRender guiR
        return True
      Just "Home" -> do
        atomicModifyIORef guiR $ \g -> (g{ location = defaultView }, ())
        startRender guiR
        return True
      Just "Page_Up" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex ey 0.5 0.5
        startRender guiR
        return True
      Just "Page_Down" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex ey 2.0 2.0
        startRender guiR
        return True
      Just "KP_Subtract" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex ey (0.5**0.1) (0.5**0.1)
        startRender guiR
        return True
      Just "KP_Add" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex ey (2.0**0.1) (2.0**0.1)
        startRender guiR
        return True
      Just "Left" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR (-ex) ey 1.0 0.0
        startRender guiR
        return True
      Just "Right" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR (3 * ex) ey 1.0 0.0
        startRender guiR
        return True
      Just "Up" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex (-ey) 1.0 0.0
        startRender guiR
        return True
      Just "Down" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex (3 * ey) 1.0 0.0
        startRender guiR
        return True
      Just "KP_4" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR (ex-ex/10) ey 1.0 0.0
        startRender guiR
        return True
      Just "KP_6" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR (ex+ex/10) ey 1.0 0.0
        startRender guiR
        return True
      Just "KP_8" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex (ey-ey/10) 1.0 0.0
        startRender guiR
        return True
      Just "KP_2" -> do
        gui <- readIORef guiR
        let ex = displaySize gui * fromIntegral (imageWidth gui) / 2
            ey = displaySize gui * fromIntegral (imageHeight gui) / 2
        doZoom guiR ex (ey+ey/10) 1.0 0.0
        startRender guiR
        return True
      Just "j" -> doNewton guiR True  >> return True
      Just "k" -> doSkew   guiR False False >> return True
      Just "l" -> doSkew   guiR True  False >> return True
      Just "semicolon" ->
                  doSkew   guiR False True  >> return True
      Just "apostrophe" ->
                  doSkew   guiR True  True  >> return True
      Just "m" -> doNewton guiR False >> return True
      Just "u" -> do
        atomicModifyIORef guiR $ \g -> (g{ transformation = identity }, ())
        startRender guiR
        return True
      Just "9" -> do
        atomicModifyIORef guiR $ \g -> (g{ log2weight = log2weight g - 0.25 }, ())
        return True
      Just "0" -> do
        atomicModifyIORef guiR $ \g -> (g{ log2weight = log2weight g + 0.25 }, ())
        return True
      Just "comma" -> do
        doRotate guiR (-5 * pi / 180)
        startRender guiR
        return True
      Just "period" -> do
        doRotate guiR ( 5 * pi / 180)
        startRender guiR
        return True
      Just n -> print ("unhandled key", n) >> return False
      _ -> return False

  gui <- readIORef guiR
  glarea' <- new GLArea []
  set glarea' [ #autoRender := False, #hasAlpha := True ]
  widgetSetSizeRequest glarea' (round (displaySize gui * fromIntegral (imageWidth gui))) (round (displaySize gui * fromIntegral (imageHeight gui)))
  atomicModifyIORef guiR $ \g -> (g{ glarea = glarea'}, ())
  eventBox <- new EventBox []
  set eventBox [ #aboveChild := True ]
  widgetAddEvents eventBox [ EventMaskScrollMask, EventMaskPointerMotionMask ]
  #add eventBox glarea'
  #add box eventBox
  on glarea' #realize $ doRealize guiR
  on glarea' #unrealize $ doUnrealize guiR
  on glarea' #render $ doRender guiR

  on eventBox #scrollEvent $ \event -> do
    ex <- event `get` #x
    ey <- event `get` #y
    d <- event `get` #direction
    case d of
      ScrollDirectionUp   -> doZoom guiR ex ey 0.5 0.5 >> startRender guiR >> return True
      ScrollDirectionDown -> doZoom guiR ex ey 2.0 2.0 >> startRender guiR >> return True
      _ -> return False

  on eventBox #buttonPressEvent $ \event -> do
    b <- event `get` #button
    ex <- event `get` #x
    ey <- event `get` #y
    case b of
      1 -> doZoom guiR ex ey 0.5 0.5 >> startRender guiR >> return True
      2 -> doZoom guiR ex ey 1.0 0.0 >> startRender guiR >> return True
      3 -> doZoom guiR ex ey 2.0 2.0 >> startRender guiR >> return True
      _ -> return False

  statusBar <- boxNew OrientationHorizontal 5
  lmousexl <- labelNew $ Just "X"
  lmousex <- labelNew Nothing
  lmouseyl <- labelNew $ Just "Y"
  lmousey <- labelNew Nothing
  ldistancel <- labelNew $ Just "D"
  ldistance <- labelNew Nothing
  lperiodl <- labelNew $ Just "P"
  lperiod <- labelNew Nothing
  literationl <- labelNew $ Just "N"
  literation <- labelNew Nothing
  #add statusBar lmousexl
  #add statusBar lmousex
  #add statusBar lmouseyl
  #add statusBar lmousey
  #add statusBar ldistancel
  #add statusBar ldistance
  #add statusBar lperiodl
  #add statusBar lperiod
  #add statusBar literationl
  #add statusBar literation
  #add box statusBar

  on eventBox #motionNotifyEvent $ \event -> do
    ex <- event `get` #x
    ey <- event `get` #y
    gui <- readIORef guiR
    let i = round (ex / displaySize gui)
        j = round (ey / displaySize gui)
        buf = renderBuffer gui
    when (0 <= i && i < width buf && 0 <= j && j < height buf) $ do
      de <- V.read (contents buf) ((width buf * j + i) * channels buf + 0)
      np <- V.read (contents buf) ((width buf * j + i) * channels buf + 1)
      ni <- V.read (contents buf) ((width buf * j + i) * channels buf + 2)
      nf <- V.read (contents buf) ((width buf * j + i) * channels buf + 3)
      let n = ni + nf
          p = round np
          ti  = T.pack (show i)
          tj  = T.pack (show j)
          tde = T.pack (show de)
          tp  = T.pack (show p)
          tn  = T.pack (show n)
      set lmousex [ #label := ti ]
      set lmousey [ #label := tj ]
      set ldistance [ #label := tde ]
      set lperiod [ #label := tp ]
      set literation [ #label := tn ]
    return True

  set window [ #resizable := False ]
  onWidgetDestroy window mainQuit
  widgetShowAll window
  timeoutAdd PRIORITY_DEFAULT 200 $ #queueRender glarea' >> return True
  Gtk.main
