#!/bin/bash
ow=1280
oh=1280
aa=4
tw=$((64 * aa))
th=$((64 * aa))
n=2048
running=1
while (( $running ))
do
  x="$(ghci -e "import System.Random" -e "randomRIO (-2, 2 :: Double)")"
  y="$(ghci -e "import System.Random" -e "randomRIO (-2, 2 :: Double)")"
  r=4
  fn1="$(( RANDOM % 4 ))"
  formula=Formula
  for i in $(seq 0 $((fn1)))
  do
    sx="x"
    if (( $RANDOM & 1 ))
    then
      sx="|${sx}|"
    fi
    if (( $RANDOM & 1 ))
    then
      sx="(-${sx})"
    fi
    sy="y"
    if (( $RANDOM & 1 ))
    then
      sy="|${sy}|"
    fi
    if (( $RANDOM & 1 ))
    then
      sy="(-${sy})"
    fi
    if (( $RANDOM & 1 ))
    then
      st=$sx
      sx=$sy
      sy=$st
    fi
    f="z:=(${sx}+i${sy})^p+c"
    formula="$(echo -e "${formula}\n ${f}")"
  done
  echo "$formula"
  p="$((2 + (RANDOM % 4)))"
  echo "p = $p"
  q=2
  weight="0.25"
  maxdepth="$((15 + (RANDOM % 20)))"
  stem="etbot-$(date --iso=s)"
  mkdir -p "${stem}"
  for depth in $(seq -w 0 ${maxdepth})
  do
    et-cli "${stem}/${depth}.png" "$((tw*2))" "$((th*2))" "$x" "$y" "$r" "$n" 1 0 0 1 1 0 "$p" "$q" "$formula" "$weight" > /dev/null 2>/dev/null
    totalscore=0
    for j in $(seq 0 4)
    do
      for i in $(seq 0 4)
      do
        convert "${stem}/${depth}.png" -colorspace RGB -crop "$((tw))x$((th))+$((tw/4 * i))+$((th/4 * j))" -geometry "$((tw/aa))x$((th/aa))" -colorspace sRGB "${stem}/tmp.png"
        score="$(pngtopnm < "${stem}/tmp.png" | ppmtopgm | pnmdepth 255 | ./et-metric 2>/dev/null)"
        rm "${stem}/tmp.png"
        echo "$score $i $j"
      done
    done > "${stem}/${depth}.txt"
    cat "${stem}/${depth}.txt" | sort -g -k 1,1 | grep -v nan | tail -n 1 > "${stem}/${depth}.pick"
    read score i j < "${stem}/${depth}.pick"
    x="$(ghci -e "$x + ($i / 4 - 0.5) * $r :: Double")"
    y="$(ghci -e "$y + ($j / 4 - 0.5) * $r :: Double")"
    r="$(ghci -e "$r / 2 :: Double")"
    echo "${depth}/${maxdepth} ${score} $i $j"
  done
  ok="$(ghci -e "$score > 0.5")"
  if [ "x$ok" = "xTrue" ]
  then
    et-cli "${stem}/big.png" "$((ow * aa))" "$((oh * aa))" "$x" "$y" "$r" 4096 1 0 0 1 1 0 "$p" "$q" "$formula" "$weight" > /dev/null
    convert "${stem}/big.png" -colorspace RGB -geometry "${ow}x${oh}" -colorspace sRGB "${stem}.png"
    running=0
  else
    echo "not good enough, try again"
  fi
done
