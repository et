{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Perturb where

import Control.Exception (Exception, catch, throwIO)
import Control.Monad (forM_, unless, when)
import Data.IORef (newIORef, readIORef, writeIORef, atomicModifyIORef')
import Data.List (sort, sortBy)
import Data.Ord (comparing)
import Data.Atomics.Counter (newCounter, incrCounter)
import Foreign
import GHC.Conc (getNumCapabilities)
import Control.Concurrent.Async (async, wait, replicateConcurrently)
import Numeric.LongDouble (LongDouble)
import Numeric.Rounded.Simple (RoundingMode(TowardNearest), precRound, exponent', encodeFloat', decodeFloat', add_, sub_, precision)
import qualified Data.Vector.Storable as S
import qualified Data.Vector.Storable.Mutable as SM

import qualified Fractal.EscapeTime.Runtime.Loader as ET
import Fractal.EscapeTime.Runtime.Loader (ET)
import Fractal.EscapeTime.Runtime.Render.View
import Fractal.EscapeTime.Runtime.Render.Buffer
import Fractal.EscapeTime.Runtime.Render.Coord
import Fractal.EscapeTime.Runtime.Render.Transform
import Fractal.EscapeTime.Runtime.Render.Progressive
import Fractal.EscapeTime.Formulas.Types (R2(..))

class (Storable t, RealFloat t, Show t) => Perturb t where
  reference :: ET -> ET.Reference t
  perturbation :: ET -> ET.Perturbation t

--instance Perturb Float where reference = ET.referencef ; perturbation = ET.perturbationf
instance Perturb Double where reference = ET.reference ; perturbation = ET.perturbation
--instance Perturb LongDouble where reference = ET.referencel ; perturbation = ET.perturbationl

render :: Perturb t => t -> ET -> Int -> t -> t -> View -> Buffer Float -> Transform -> (Coord t, UnCoord t) -> Maybe Progressive -> IO (IO Bool, IO Bool)
render er et maxiters d_ e_ (View x0hi y0hi r0hi) buf@(Buffer w h _c@4 _) tr (coord', unCoord') mprog = do
  runningP <- malloc
  poke runningP 1
  a <- async $ Control.Exception.catch (do
    let prec = max 53 $ 53 - exponent' r0hi
        count = w * h
    orbit <- SM.new (maxiters * 3)
    done <- SM.replicate count False
    glitch_refescaped  <- SM.replicate count (-1 `asTypeOf` er)
    glitch_pauldelbrot <- SM.replicate count (-1 `asTypeOf` er)
    glitched_refescapedR <- newIORef True
    glitched_pauldelbrotR <- newIORef True
    referenceR <- newIORef (precRound TowardNearest prec x0hi, precRound TowardNearest prec y0hi, w `div` 2, h `div` 2)
    let r0 = uncurry encodeFloat $ decodeFloat' r0hi
        px = 2 * r0 / fromIntegral h
        (aa, ab, ba, bb) = getTransform tr
        pxt = (px * realToFrac aa, px * realToFrac ab, px * realToFrac ba, px * realToFrac bb)
        active = do
          running <- peek runningP
          glitched_refescaped <- readIORef glitched_refescapedR
          glitched_pauldelbrot <- readIORef glitched_pauldelbrotR
          return (running /= 0 && (glitched_refescaped || glitched_pauldelbrot))
    whileM_ active $ do
     writeIORef glitched_refescapedR False
     writeIORef glitched_pauldelbrotR False
     with (0::Int) $ \refescapedP -> with (0::Int) $ \pauldelbrotP -> do
      (a, b, i0, j0) <- readIORef referenceR
      print (i0, j0)
      mm <- reference et orbit (er * er) d_ e_ a b runningP
      case mm of
        Just iters -> do
          let (da0, db0) = coord' tr px w h i0 j0
          npixelR <- newCounter 0
          threads <- getNumCapabilities
          let worker pb0@(pb, _) ge0@(ge, _) = do
                npixel <- subtract 1 `fmap` incrCounter 1 npixelR
                if npixel < count
                  then do
                    let Pixel (i, j, _, _) = case mprog of
                                         Just prog -> prog S.! npixel
                                         Nothing -> case npixel `divMod` w of
                                           (j, i) -> Pixel (fromIntegral i, fromIntegral j, 0, 0)
                        (a0, b0) = coord' tr px w h (fromIntegral i) (fromIntegral j)
                        da = a0 - da0
                        db = b0 - db0
                    gr <- SM.read glitch_refescaped npixel
                    gp <- SM.read glitch_pauldelbrot npixel
                    let g = min gr gp
                    if g < 0
                      then do -- pixel needs computing
                        -- BEGIN HACK
                        -- ignore isolated glitches in 3x3 neighbourhood
                        -- speeds up high resolution rendering a great deal
                        -- at cost of slightly inaccurate images
                        -- doesn't work for interactive mode because the vectors are not in image order...
                        isolated <- case mprog of
                          Just _ -> return False
                          Nothing -> do
                            let is = [fromIntegral i - 1 .. fromIntegral i + 1]
                                js = [fromIntegral j - 1 .. fromIntegral j + 1]
                                ns = [ j * w + i | j <- js, 0 <= j, j < h, i <- is, 0 <= i, i < w ]
                            grs <- mapM (SM.read glitch_refescaped) ns
                            gps <- mapM (SM.read glitch_pauldelbrot) ns
                            let (lo, hi) = span (< 0) . sort $ zipWith min grs gps
                                isolated = length lo <= 1
                            return isolated
                        mout <-
                          if isolated
                            then pure (Just ((0, 0, 0, 0), (0, 0)))
                            else
                              perturbation et orbit iters (er * er) pxt d_ e_ da db runningP
                        -- END HACK
                        case mout of
                          Nothing -> throwIO Cancelled
                          Just ((de, p, ni, nf), (u, v)) -> do
                            if de < 0
                            then do -- glitched
                              if (fromIntegral i == i0 && fromIntegral j == j0)
                              then do -- reference pixel
                                output mprog done buf npixel True (if nf == negate 2 then 1e30 else 1e-30, realToFrac p, 0, 0)
                                SM.write glitch_refescaped  npixel 0
                                SM.write glitch_pauldelbrot npixel 0
                                worker pb0 ge0
                              else if nf == negate 2
                                then do -- reference escaped
                                  output mprog done buf npixel False (-1, realToFrac p, 0, 0)
                                  SM.write glitch_refescaped npixel de
                                  SM.write glitch_pauldelbrot npixel p
                                  poke refescapedP 1
                                  worker pb0 (if de > ge then (de, ((u, v), (i, j))) else ge0)
                                else do -- pauldebrot glitch, nf == negate 3
                                  output mprog done buf npixel False (-1, realToFrac p, 0, 0)
                                  SM.write glitch_refescaped npixel 0
                                  SM.write glitch_pauldelbrot npixel de
                                  poke pauldelbrotP 1
                                  worker (if de > pb then (de, ((u, v), (i, j))) else pb0) ge0
                            else do -- non glitched
                                output mprog done buf npixel True (realToFrac de, realToFrac p, realToFrac ni, realToFrac nf)
                                SM.write glitch_refescaped  npixel 0
                                SM.write glitch_pauldelbrot npixel 0
                                worker pb0 ge0
                      else do
                        worker pb0 ge0
                  else do
                    return (pb0, ge0)
          rs <- replicateConcurrently threads $ worker (-1/0, ((0, 0), (0, 0))) (-1/0, ((0, 0), (0, 0)))
          g <- peek refescapedP
          when (g /= 0) $ writeIORef glitched_refescapedR True
          g <- peek pauldelbrotP
          when (g /= 0) $ writeIORef glitched_pauldelbrotR True
          whenM_ active $ do
            let (pb, ((u0, v0), (i0, j0))):_ = sortBy (flip (comparing fst)) (map fst rs)
                (ge, ((u1, v1), (i1, j1))):_ = sortBy (flip (comparing fst)) (map snd rs)
                mdxy
                  | pb > -1/0 = Just ((u0, v0), (i0, j0))
                  | ge > -1/0 = Just ((u1, v1), (i1, j1))
                  | otherwise = Nothing
            case mdxy of
              Just ((u, v), (ii, jj)) -> do
                let a' = add_ TowardNearest prec a $ uncurry (encodeFloat' TowardNearest 53) (decodeFloat u)
                    b' = add_ TowardNearest prec b $ uncurry (encodeFloat' TowardNearest 53) (decodeFloat v)
                case unCoord tr px w h (u - da0) (v - db0) of
                  -- Just (i', j') -> writeIORef referenceR (a', b', i', j') -- FIXME doesn't work properly, rethink
                  _ -> do
--                    print ("internal warning: far glitch?", ii, jj, u, v)
                    let (u, v) = coord' tr px w h (fromIntegral ii) (fromIntegral jj)
                        dx = u - da0
                        dy = v - db0
                        a' = add_ TowardNearest prec a $ uncurry (encodeFloat' TowardNearest 53) (decodeFloat dx)
                        b' = add_ TowardNearest prec b $ uncurry (encodeFloat' TowardNearest 53) (decodeFloat dy)
                    writeIORef referenceR (a', b', fromIntegral ii, fromIntegral jj)
              _ -> print "internal error: no glitch?"
        _ -> throwIO Cancelled
    return True
    ) (\Cancelled -> return False)
  return  ( do{ poke runningP 0 ; r <- wait a ; free runningP ; return r }
          , do{                   r <- wait a ; free runningP ; return r }
          )
render _ _ _ _ _ _ _ _ _ _ = return (return False, return False)

taxicab :: Perturb t => Int -> Int -> Maybe Progressive -> SM.IOVector t -> IO (Int, Int, Word32)
taxicab w h mprog glitch = do
  let count = w * h
  distance <- SM.replicate count (maxBound :: Word32)
  forM_ [0 .. count - 1] $ \npixel -> do
    let Pixel (i, j, _, _) = case mprog of
                         Just prog -> prog S.! npixel
                         Nothing -> case npixel `divMod` w of
                           (j, i) -> Pixel (fromIntegral i, fromIntegral j, 0, 0)
        k = fromIntegral j * w + fromIntegral i
    g <- SM.read glitch npixel
    unless (g < 0) $ SM.write distance k 0
  forM_ [0 .. h - 1] $ \j -> do -- par
    forM_ [1 .. w - 1] $ \i -> do
      let ii = i - 1
          k = j * w + i
          kk = j * w + ii
      dkk <- SM.read distance kk
      dk  <- SM.read distance k
      when (dkk < dk) $ SM.write distance k (dkk + 1)
    forM_ [w - 2, w - 3 .. 0] $ \i -> do
      let ii = i + 1
          k = j * w + i
          kk = j * w + ii
      dkk <- SM.read distance kk
      dk  <- SM.read distance k
      when (dkk < dk) $ SM.write distance k (dkk + 1)
  forM_ [0 .. w - 1] $ \i -> do -- par
    forM_ [1 .. h - 1] $ \j -> do
      let jj = j - 1
          k = j * w + i
          kk = jj * w + i
      dkk <- SM.read distance kk
      dk  <- SM.read distance k
      when (dkk < dk) $ SM.write distance k (dkk + 1)
    forM_ [h - 2, h - 3 .. 0] $ \j -> do
      let jj = j + 1
          k = j * w + i
          kk = jj * w + i
      dkk <- SM.read distance kk
      dk  <- SM.read distance k
      when (dkk < dk) $ SM.write distance k (dkk + 1)
  miR <- newIORef 0
  mjR <- newIORef 0
  mdR <- newIORef 0
  forM_ [0 .. h - 1] $ \j -> do
    forM_ [0 .. w - 1] $ \i -> do
      let k = j * w + i
      md <- readIORef mdR
      d <- SM.read distance k
      when (d > md) $ do
        writeIORef miR i
        writeIORef mjR j
        writeIORef mdR d
  i <- readIORef miR
  j <- readIORef mjR
  d <- readIORef mdR
  return (i, j, d)

data Cancelled = Cancelled deriving Show
instance Exception Cancelled

whenM_, whileM_ :: IO Bool -> IO () -> IO ()

whenM_ test action = do
  ok <- test
  when ok action

whileM_ test action = whenM_ test $ do
    action
    whileM_ test action
