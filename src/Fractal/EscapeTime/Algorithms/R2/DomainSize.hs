{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.DomainSize (domain_size) where

import Prelude hiding (read)

import Control.Monad (replicateM_)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

-- FIXME this is broken, don't know what the fixes should be
domain_size :: [(Int, R2Formula (Expr String))] -> String
domain_size fs = runCompile_ FDouble . function NBool "domain_size" [(NInt, "P"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NReal, "R"), (NVolatileIntPtr, "RUNNING")] $ \s -> do
  let degree = 2 -- FIXME
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  p <- int
  a <- real
  b <- real
  p .= EVar NInt "P"
  let d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
  a .= EVar NReal "A"
  b .= EVar NReal "B"
  x <- real
  y <- real
  lxa <- real
  lxb <- real
  lya <- real
  lyb <- real
  bxa <- real
  bxb <- real
  bya <- real
  byb <- real
  -- FIXME initialization?
  x .= a
  y .= b
  lxa .= 1
  lxb .= 0
  lya .= 0
  lyb .= 1
  zq2 <- real
  zq2 .= x * x + y * y
  -- loop one period
  q <- int
  q .= 2
  k <- int
  k .= 0
  running <- bool
  running .= read (EVar NVolatileIntPtr "RUNNING") 0
  while_ (q `ELessEqual` p `EAnd` running) $ do
    -- alloc z, l
    xn <- real
    yn <- real
    lxan <- real
    lxbn <- real
    lyan <- real
    lybn <- real
    -- calc z, l
    let g (_, f) j0 kn = (,) j0 $ do
          let R2 gx gy = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize gx
              fy = optimize gy
              fdxa = ddx d a fx
              fdxb = ddx d b fx
              fdya = ddx d a fy
              fdyb = ddx d b fy
              d v u
                | u == x && v == a = lxa
                | u == x && v == b = lxb
                | u == y && v == a = lya
                | u == y && v == b = lyb
                | u == v = 1
                | otherwise = 0
          xn .= fx
          yn .= fy
          lxan .= optimize fdxa
          lxbn .= optimize fdxb
          lyan .= optimize fdya
          lybn .= optimize fdyb
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    x .= xn
    y .= yn
    lxa .= lxan
    lxb .= lxbn
    lya .= lyan
    lyb .= lybn
    -- z2 <- |z|^2
    zp2 <- real
    zp2 .= x * x + y * y
    _ <- if_ ((zp2 `ELess` zq2) `EAnd` (q `ELess` p)) (zq2 .= zp2) (return ())
    -- loop
    q .= q + 1
    running .= read (EVar NVolatileIntPtr "RUNNING") 0
  -- abszq / cabs(dc);
  EVar NReal "R" .= ESqrt NReal (zq2 / ESqrt NReal (lxa * lxa + lxb * lxb + lya * lya + lyb * lyb))
  s .= running
  return_ s
