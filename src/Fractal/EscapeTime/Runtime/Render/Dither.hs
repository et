{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Runtime.Render.Dither (Dither(..), dither, defaultDither) where

import Control.Monad.Identity (runIdentity)
import Data.Word (Word32)
import Data.Bits (xor, shiftL, shiftR)

data Dither = Dither{ ditherGauss :: Bool, ditherSeed :: Int, ditherRadius :: Double }

defaultDither :: Dither
defaultDither = Dither False 1 1

-- http://www.burtleburtle.net/bob/hash/integer.html
burtleHash :: Word32 -> Word32
burtleHash a = runIdentity $ do
  a <- pure $ (a+0x7ed55d16) + (a`shiftL`12);
  a <- pure $ (a`xor`0xc761c23c) `xor` (a`shiftR`19);
  a <- pure $ (a+0x165667b1) + (a`shiftL`5);
  a <- pure $ (a+0xd3a2646c) `xor` (a`shiftL`9);
  a <- pure $ (a+0xfd7046c5) + (a`shiftL`3);
  a <- pure $ (a`xor`0xb55a4f09) `xor` (a`shiftR`16);
  pure a

uniform :: Word32 -> Word32 -> Word32 -> Double
uniform x y c = fromIntegral (burtleHash (x + burtleHash (y + burtleHash c))) / 0x100000000

boxMuller :: Double -> Double -> Double -> (Double, Double)
boxMuller s u v =
  let r | u < 0 && u < 1 = s * sqrt (-2 * log u)
        | otherwise = 0
      t = 2 * pi * v
  in  (r * cos t, r * sin t)

dither :: Dither -> Int -> Int -> (Double, Double)
dither (Dither gauss seed radius) x y =
  let u = uniform (fromIntegral x) (fromIntegral y) (fromIntegral (2 * seed) + 0)
      v = uniform (fromIntegral x) (fromIntegral y) (fromIntegral (2 * seed) + 1)
      (dx, dy)  | gauss = boxMuller (0.5 * radius) u v 
                | otherwise = (radius * (u - 0.5), radius * (v - 0.5))
  in  (fromIntegral x + dx, fromIntegral y + dy)
