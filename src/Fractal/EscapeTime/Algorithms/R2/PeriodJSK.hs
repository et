{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.PeriodJSK (period_jsk) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types

period_jsk :: [(Int, R2Formula (Expr String))] -> String
period_jsk fs = runCompile_ FDouble . function NInt "period_jsk" [(NInt, "N"), (NFloat, "R2"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NReal, "S"), (NFloatPtr, "K"), (NVolatileIntPtr, "RUNNING")] $ \i -> do
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  n <- int
  r2 <- float
  a <- real
  b <- real
  s <- real
  n .= EVar NInt "N"
  r2 .= EVar NFloat "R2"
  let d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
      kk = EVar NFloatPtr "K"
  a .= EVar NReal "A"
  b .= EVar NReal "B"
  s .= EVar NReal "S"
  -- K^{-1}
  ai <- float
  aj <- float
  bi <- float
  bj <- float
  ai .= read kk 0
  aj .= read kk 1
  bi .= read kk 2
  bj .= read kk 3
  detK <- float
  detK .= ai * bj - aj * bi
  ai1 <- float
  aj1 <- float
  bi1 <- float
  bj1 <- float
  ai1 .=  bj / detK
  aj1 .= -bi / detK
  bi1 .= -aj / detK
  bj1 .=  ai / detK
  -- X
  x <- real
  y <- real
  x .= 0
  y .= 0
  -- J
  xa <- real
  xb <- real
  ya <- real
  yb <- real
  xa .= 0
  xb .= 0
  ya .= 0
  yb .= 0
  -- ...
  xf <- float
  yf <- float
  z2 <- float
  i .= 0
  z2 .= 0
  p <- bool
  p .= 1
  k <- int
  k .= 0
  running <- bool
  running .= read (EVar NVolatileIntPtr "RUNNING") 0
  while_ (ELess i n `EAnd` ELess z2 r2 `EAnd` p `EAnd` running) $ do
    -- allocate next
    xn <- real
    yn <- real
    xan <- real
    xbn <- real
    yan <- real
    ybn <- real
    -- do formula
    let g (_, f) j kn = (,) j $ do
          let R2 fx0 fy0 = f (R2 d_ e_) (R2 a b) (R2 x y)
              fx = optimize fx0
              fy = optimize fy0
              d v u
                | u == x && v == a = xa
                | u == x && v == b = xb
                | u == y && v == a = ya
                | u == y && v == b = yb
                | u == v = 1
                | otherwise = 0
          -- calc next
          xn .= fx
          yn .= fy
          xan .= optimize (ddx d a fx)
          xbn .= optimize (ddx d b fx)
          yan .= optimize (ddx d a fy)
          ybn .= optimize (ddx d b fy)
          -- step next
          x .= xn
          y .= yn
          xa .= xan
          xb .= xbn
          ya .= yan
          yb .= ybn
          xf .= x
          yf .= y
          z2 .= xf*xf + yf*yf
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- J^{-1}
    detJ <- real
    detJ .= xa * yb - xb * ya
    xa1 <- real
    xb1 <- real
    ya1 <- real
    yb1 <- real
    xa1 .=  yb / detJ
    xb1 .= -ya / detJ
    ya1 .= -xb / detJ
    yb1 .=  xa / detJ
    -- (u0 v0) = J^{-1} * (x y)
    u0 <- real
    v0 <- real
    u0 .= xa1 * x + xb1 * y
    v0 .= ya1 * x + yb1 * y
    -- (u1 v1) = s^{-1} K^{-1} * (u0 v0)
    u1 <- real
    v1 <- real
    u1 .= (ai1 * u0 + aj1 * v0) / s
    v1 .= (bi1 * u0 + bj1 * v0) / s
    -- float
    u2 <- float
    v2 <- float
    u2 .= u1
    v2 .= v1
    uv <- float
    uv .= u2 * u2 + v2 * v2
    p .= 1 `ELessEqual` uv
    -- update loop
    i .= i + 1
    running .= read (EVar NVolatileIntPtr "RUNNING") 0
  _ <- if_ (EEqual i n `EOr` ELessEqual r2 z2 `EOr` p) (i .= -1) (return ())
  return_ i
