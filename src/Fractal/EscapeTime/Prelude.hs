{-
et -- escape time fractals
Copyright (C) 2018,2019,2020,2021 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}

module Fractal.EscapeTime.Prelude
  ( module Fractal.EscapeTime.Prelude
  , module Fractal.EscapeTime.Compiler.Expr
  , module Fractal.EscapeTime.Formulas.Types
  , module Fractal.EscapeTime.Runtime.Compiler
  , module Fractal.EscapeTime.Runtime.Loader
  ) where

import Fractal.EscapeTime.Compiler.Expr
import Fractal.EscapeTime.Formulas.Types
import Fractal.EscapeTime.Runtime.Compiler
import Fractal.EscapeTime.Runtime.Loader hiding (readLib)

import qualified Numeric.Rounded as R
import Numeric.Rounded.Simple hiding (simplify)

import Data.Char (toUpper)

[a,b,d,e,x,y,aa,bb,dd,ee,xx,yy] = map (var.(:[])) "abdexyABDEXY"
[daa,dab,dba,dbb,dxa,dxb,dya,dyb,dxx,dxy,dyx,dyy] = map var $ words "daa dab dba dbb dxa dxb dya dyb dxx dxy dyx dyy"
c = R2 a b
f = R2 d e
z = R2 x y
cc = R2 aa bb
ff = R2 dd ee
zz = R2 xx yy
var = EVar NFloat
vars = map vp "abxy" ++ map v0 "ef"
  where
    vp c = ([c], (EVar NFloat [toUpper c], EVar NFloat [c]))
    v0 c = ([c], (EVar NFloat [toUpper c], EInt 0))
dby (EVar _ [v]) (EVar _ [u])
  | u `elem` "abxy" && v `elem` "abxy" = var ['d',u,v]
dby v u = EInt $ if v == u then 1 else 0

jc0 = (dxa, dxb, dya, dyb)
jc xn yn =
  ( optimize $ ddx dby a xn
  , optimize $ ddx dby b xn
  , optimize $ ddx dby a yn
  , optimize $ ddx dby b yn
  )

jz0 = (dxx, dxy, dyx, dyy)
jz xn yn =
  ( optimize $ ddx dby x xn
  , optimize $ ddx dby y xn
  , optimize $ ddx dby x yn
  , optimize $ ddx dby y yn
  )

tmp :: Integer -> String
tmp n = "t_" ++ show n

{-
parse s = case P.parse expr "" s of
  Right pe -> interpretP (map (\v -> (v, var v)) . S.toList . variables $ pe) pe
  Left err -> Left (show err)
-}

go g =
  let R2 xu yu = g f c z
      xn = optimize xu
      yn = optimize yu
      (dxan, dxbn, dyan, dybn) = jc xn yn
      (dxxn, dxyn, dyxn, dyyn) = jz xn yn
      pxn = optimize $ perturb vars xn
      pyn = optimize $ perturb vars yn
      (?) = (,)
      cse _ ves = ves
  in  unlines
        [ v ++ " &:= " ++ pretty e ++ "\\\\"
        | (v, e) <- cse tmp
          [ "xn" ? xn
          , "xn" ? yn
          ] ++ cse tmp
          [ "dxan" ? dxan
          , "dxbn" ? dxbn
          , "dyan" ? dyan
          , "dybn" ? dybn
          , "dxxn" ? dxxn
          , "dxyn" ? dxyn
          , "dyxn" ? dyxn
          , "dyyn" ? dyyn
          ] ++ cse tmp
          [ "xn" ? pxn
          , "yn" ? pyn
          ]
        ]

diff :: String -> Expr String -> Expr String
diff by = ddx d (EVar NFloat by)
  where
    d :: Expr String -> Expr String -> Expr String
    d v u | v == u = 1
    d v@(EVar _ _) u@(EVar _ _) = dby v u

toR :: Double -> Rounded
toR = fromDouble R.TowardNearest 53

fromR :: Rounded -> Double
fromR r = reifyRounded r (R.toDouble :: forall k (p :: k) . R.Precision p => R.Rounded 'R.TowardNearest p -> Double)

{-
      tmp <- getCanonicalTemporaryDirectory
      dir <- createTempDirectory tmp "et"

      r <- compileAndLoad dir ("Burning Ship\n  z := (|x| + i |y|)^p + c"]) 2 1
      case r of
        Left msg -> hPutStrLn stderr msg
        Right et -> do
          (c, s, t) <- do
            Just (R2 a b) <- with 1 $ newton et 50 3 0 0 (toR 1) (toR (-2))
            Just (s, t) <- with 1 $ size bs 3 0 0 a b
            pure (R2 (fromR a) (fromR b), fromR s, t)
-}
