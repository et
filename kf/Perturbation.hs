{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Perturbation (perturbation) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types hiding (i)

fts :: [FType]
fts = [FDouble,FLongDouble,FFloatExp]

perturbation :: Bool -> Int -> Int -> [(Int, R2Formula (Expr String))] -> String
perturbation derivs typ powr fs@[(_, f)] = concat . flip map fts $ \t -> runCompile_ t . function NInt ("perturbation" ++ (if derivs then "D" else "") ++ fSuffix t)
  ([(NInt, "m_nFractalType"), (NInt, "m_nPower")
  , (NFloatPtr, "m_db_dxr"), (NFloatPtr, "m_db_dxi"), (NFloatPtr, "m_db_z")
  , (NIntPtr, "antal"), (NFloatPtr, "test1"), (NFloatPtr, "test2"), (NIntPtr, "bGlitch")
  , (NFloat, "m_nBailout2"), (NInt, "nMaxIter"), (NInt, "m_bNoGlitchDetection")
  , (NFloat, "g_real"), (NFloat, "g_imag")
  , (NFloat, "g_FactorAR"), (NFloat, "g_FactorAI")
  , (NFloatPtr, "Dr"), (NFloatPtr, "Di")
  , (NFloat, "dbD0r"), (NFloat, "dbD0i")
  ] ++ if derivs then [ (NFloatPtr, "dzc"), (NFloatPtr, "dci") ] else []) $ \ret -> do
    let m_nFractalType = EVar NInt "m_nFractalType"
        m_nPower = EVar NInt "m_nPower"
        m_db_dxr = EVar NFloatPtr "m_db_dxr"
        m_db_dxi = EVar NFloatPtr "m_db_dxi"
        m_db_z = EVar NFloatPtr "m_db_z"
        antal0 = EVar NIntPtr "antal"
        test10 = EVar NFloatPtr "test1"
        test20 = EVar NFloatPtr "test2"
        bGlitch0 = EVar NIntPtr "bGlitch"
        m_nBailout2 = EVar NFloat "m_nBailout2"
        nMaxIter = EVar NInt "nMaxIter"
        m_bNoGlitchDetection = EVar NInt "m_bNoGlitchDetection"
        g_real = EVar NFloat "g_real"
        g_imag = EVar NFloat "g_imag"
        d_@_g_FactorAR = EVar NFloat "g_FactorAR"
        e_@_g_FactorAI = EVar NFloat "g_FactorAI"
        dR0 = EVar NFloatPtr "Dr"
        dI0 = EVar NFloatPtr "Di"
        dbD0r = EVar NFloat "dbD0r"
        dbD0i = EVar NFloat "dbD0i"
        dzc = EVar NFloatPtr "dzc"
        dci = EVar NFloatPtr "dci"
    _ <- if_ (EEqual m_nFractalType (fromIntegral typ) `EAnd` EEqual m_nPower (fromIntegral powr)) (do
      antal <- int
      test1 <- float
      test2 <- float
      antal .= read antal0 0
      test1 .= read test10 0
      test2 .= read test20 0
      xr <- float
      xi <- float
      xxr <- float
      xxi <- float
      xr .= read dR0 0
      xi .= read dI0 0
      xxr .= 0
      xxi .= 0
      mderivs <- if not derivs then return Nothing else do
        dxa <- float
        dxb <- float
        dya <- float
        dyb <- float
        dxa .=  read dzc 0
        dxb .= -read dzc 1
        dya .=  read dzc 1
        dyb .=  read dzc 0
        dxan <- float
        dxbn <- float
        dyan <- float
        dybn <- float
        daa <- float
        dab <- float
        dba <- float
        dbb <- float
        daa .= read dci 0
        dab .= read dci 1
        dba .= read dci 2
        dbb .= read dci 3
        return $ Just (dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, daa, dab, dba, dbb) 
      k <- int
      k .= 0
      _ <- while_ (antal `ELess` nMaxIter) $ do
        rXr <- float
        rXi <- float
        rXz <- float
        rXr .= read m_db_dxr antal
        rXi .= read m_db_dxi antal
        rXz .= read m_db_z antal
        xxr .= rXr + xr
        xxi .= rXi + xi
        test2 .= test1
        test1 .= g_real * xxr * xxr + g_imag * xxi * xxi
        _ <- if_ (ELess test1 rXz) (do
          writeI bGlitch0 0 1
          if typ == 0 && powr == 2
            then comment "FIXME knighty glitch detection: if (type_0_power_2_pixel_has_glitched(cr, ci, xr, xi, Xr, Xi, dxa / h, dya / h, e, h)) { // FIXME matrix derivatives"
            else return ()
          _ <- if_ (EEqual 0 m_bNoGlitchDetection) break_ (return ())
          if typ == 0 && powr == 2
            then comment "FIXME knighty glitch detection: }"
            else return ()
          return ()) (return ())
        _ <- if_ (ELess m_nBailout2 test1) break_ (return ())
        xrn <- float
        xin <- float
        -- do formula
        let R2 fx0 fy0 = f (R2 d' e') (R2 a' b') (R2 x' y')
            d' = EVar NFloat "d"
            e' = EVar NFloat "e"
            a' = EVar NFloat "a"
            b' = EVar NFloat "b"
            x' = EVar NFloat "x"
            y' = EVar NFloat "y"
            a0 = EVar NFloat "A0"
            b0 = EVar NFloat "B0"
            aa = EVar NFloat "aa"
            bb = EVar NFloat "bb"
            vs =
              [ ("d", (d_, 0))
              , ("e", (e_, 0))
              , ("a", (a0, dbD0r))
              , ("b", (b0, dbD0i))
              , ("x", (rXr, xr))
              , ("y", (rXi, xi))
              ]
            fx = perturb vs fx0
            fy = perturb vs fy0
        comment $ "fx   = " ++ pretty fx
        comment $ "fy   = " ++ pretty fy
        xrn .= fx
        xin .= fy
        case mderivs of
          Nothing -> return ()
          Just (dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, daa, dab, dba, dbb) -> do
            let R2 gxx gyy = f (R2 d' e') (R2 aa bb) (R2 x' y')
                fxx = optimize gxx
                fyy = optimize gyy
                fdxa = optimize $ ddx d aa fxx
                fdxb = optimize $ ddx d bb fxx
                fdya = optimize $ ddx d aa fyy
                fdyb = optimize $ ddx d bb fyy
                d v u
                  | u == xr && v == aa = dxa
                  | u == xr && v == bb = dxb
                  | u == xi && v == aa = dya
                  | u == xi && v == bb = dyb
                  | u == aa && v == aa = daa
                  | u == aa && v == bb = dab
                  | u == bb && v == aa = dba
                  | u == bb && v == bb = dbb
                  | u == v = 1
                  | otherwise = 0
            comment $ "fdxa = " ++ pretty fdxa
            comment $ "fdxb = " ++ pretty fdxb
            comment $ "fdya = " ++ pretty fdya
            comment $ "fdyb = " ++ pretty fdyb
            dxan .= fdxa
            dxbn .= fdxb
            dyan .= fdya
            dybn .= fdyb
        -- continue loop
        xr .= xrn
        xi .= xin
        case mderivs of
          Nothing -> return ()
          Just (dxa, dxb, dya, dyb, dxan, dxbn, dyan, dybn, _daa, _dab, _dba, _dbb) -> do
            dxa .= dxan
            dxb .= dxbn
            dya .= dyan
            dyb .= dybn
        antal .= antal + 1
      writeI antal0 0 antal
      write test10 0 test1
      write test20 0 test2
      write dR0 0 xr
      write dI0 0 xi
      case mderivs of
        Nothing -> return ()
        Just (dxa, dxb, dya, dyb, _dxan, _dxbn, _dyan, _dybn, _daa, _dab, _dba, _dbb) -> do
          dr <- float
          di <- float
          sqrttest1 <- float
          sqrttest1 .= sqrt test1
          dr .= (xxr * dxa + xxi * dya) / sqrttest1
          di .= (xxr * dxb + xxi * dyb) / sqrttest1
          write dzc 0 dr
          write dzc 1 di
      ret .= 1) (ret .= 0)
    return_ ret
