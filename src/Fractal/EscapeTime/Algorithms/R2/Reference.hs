{-
et -- escape time fractals
Copyright (C) 2018,2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Algorithms.R2.Reference (reference) where

import Prelude hiding (read)

import Fractal.EscapeTime.Compiler
import Fractal.EscapeTime.Formulas.Types hiding (i)

reference :: [(Int, R2Formula (Expr String))] -> String
reference fs = concat . flip map [FDouble] $ \t -> runCompile_ t . function NInt ("reference" ++ fSuffix t) [(NInt, "N"), (NFloat, "R2"), (NFloat, "D"), (NFloat, "E"), (NReal, "A"), (NReal, "B"), (NFloatPtr, "XYZS"), (NVolatileIntPtr, "RUNNING")] $ \i -> do
  early "mpfr_prec_t prec=mpfr_get_prec(A);\n"
  let n = EVar NInt "N"
      r2 = EVar NFloat "R2"
      d_ = EVar NFloat "D"
      e_ = EVar NFloat "E"
      a = EVar NReal "A"
      b = EVar NReal "B"
      xyzs = EVar NFloatPtr "XYZS"
      runningP = EVar NVolatileIntPtr "RUNNING"
  x <- real
  y <- real
  xf <- float
  yf <- float
  x .= 0
  y .= 0
  xf .= x
  yf .= y
  z2 <- float
  i .= 0
  z2 .= 0
  running <- bool
  running .= read runningP 0
  k <- int
  k .= 0
  while_ (ELess i n `EAnd` ELess z2 r2 `EAnd` running) $ do
    -- write output
    write xyzs (3 * i + 0) xf
    write xyzs (3 * i + 1) yf
    write xyzs (3 * i + 2) (z2 / 1024.0^2) -- XXX needs .0, otherwise i_inv_ii
    -- allocate next
    xn <- real
    yn <- real
    -- do formula
    let g (_, f) j kn = (,) j $ do
          let R2 fx fy = f (R2 d_ e_) (R2 a b) (R2 x y)
          xn .= optimize fx
          yn .= optimize fy
          k .= fromIntegral kn
    _ <- switch_ k $ zipWith3 g fs [0..] ([1 .. length fs - 1] ++ [0])
    -- update next
    x .= xn
    y .= yn
    xf .= x
    yf .= y
    z2 .= xf*xf + yf*yf
    -- update loop
    i .= i + 1
    running .= read runningP 0
  return_ i
