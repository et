/*
et -- escape time fractals
Copyright (C) 2018,2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ET_FORMULA_H
#define ET_FORMULA_H 1

#include <assert.h>
#include <math.h>
#include <mpfr.h>

#define abs(x) ((x)<0?-(x):(x))
#define sgn(x) (((x)>=0)-((x)<0))

/*
typedef double compensated[2];

static inline void c_set_d(compensated k, double a)
{
  k[0] = a;
  k[1] = 0;
}

static inline void c_set_i(compensated k, int a)
{
  k[0] = a;
  k[1] = 0;
}

static inline void c_set_c(compensated k, const compensated a)
{
  k[0] = a[0];
  k[1] = a[1];
}

static inline void c_add_dd(compensated k, double a, double b)
{
  double x = a + b;
  double z = x - a;
  double y = (a - (x - z)) + (b - z);
  k[0] = x;
  k[1] = y;
}

static inline void c_split_d(compensated k, double a)
{
  static const double magic = 134217729;
  double c = magic * a;
  double x = c - (c - a);
  double y = a - x;
  k[0] = x;
  k[1] = y;
}

static inline void c_add_cc(compensated k, const compensated ab, const compensated cd)
{
  compensated k1; c_add_dd(k1, ab[0], cd[0]);
  compensated k2; c_add_dd(k2, k1[1], cd[1]);
  compensated k3; c_add_dd(k3, ab[1], k2[0]);
  compensated k4; c_add_dd(k4, k1[0], k3[0]);
                  c_add_dd(k, k4[0], k2[1] + k3[1] + k4[1]);
}

static inline void c_add_ci(compensated k, const compensated ab, int c)
{
  compensated cd;
  c_set_i(cd, c);
  c_add_cc(k, ab, cd);
}

static inline void c_add_ic(compensated k, int a, const compensated cd)
{
  compensated ab;
  c_set_i(ab, a);
  c_add_cc(k, ab, cd);
}

static inline void c_mul2_c(compensated k, const compensated ab)
{
  k[0] = ab[0] * 2;
  k[1] = ab[1] * 2;
}

static inline void c_mul_dd(compensated k, double a, double b)
{
  compensated k1, k2;
  c_split_d(k1, a);
  c_split_d(k2, b);
  double x = a * b;
  double a1 = k1[0];
  double a2 = k1[1];
  double b1 = k2[0];
  double b2 = k2[1];
  k[0] = x;
  k[1] = a2*b2 - (((x - a1*b1) - a2*b1) - a1*b2);
}

static inline void c_mul_cc(compensated k, const compensated ab, const compensated cd)
{
  compensated k1,k2,k3,k4,k5,k6,k7,k8;
  c_mul_dd(k1, ab[0], cd[0]);
  c_mul_dd(k2, ab[1], cd[0]);
  c_mul_dd(k3, ab[0], cd[1]);
  c_add_dd(k4, k1[0], k2[0]);
  c_add_dd(k5, k3[0], k4[0]);
  c_add_dd(k6, k1[1], k4[1]);
  c_add_dd(k7, k5[1], k6[0]);
  c_add_dd(k8, k5[0], k7[0]);
  c_add_dd(k, k8[0], ab[1] * cd[1] + k2[1] + k3[1] + k6[1] + k7[1] + k8[1]);
}

static inline void c_mul_ic(compensated k, int a, const compensated cd)
{
  // FIXME optimize
  compensated ab;
  c_set_i(ab, a);
  c_mul_cc(k, ab, cd);
}

static inline void c_mul_ci(compensated k, const compensated ab, int c)
{
  // FIXME optimize
  compensated cd;
  c_set_i(cd, c);
  c_mul_cc(k, ab, cd);
}

static inline void c_inv_c(compensated k, const compensated ab)
{
  c_add_dd(k, 1 / ab[0], -ab[1] / (ab[0] * ab[0]));
}

static inline void c_div_cc(compensated k, const compensated ab, const compensated cd)
{
  // FIXME accuracy
  compensated k1;
  c_inv_c(k1, cd);
  c_mul_cc(k, ab, k1);
}

static inline void c_sqr_d(compensated k, double a)
{
  compensated k1;
  c_split_d(k1, a);
  double x = a * a;
  double a1 = k1[0];
  double a2 = k1[1];
  k[0] = x;
  k[1] = a2*a2 - ((x - a1*a1) - 2*(a2*a1));
}

static inline void c_sqr_c(compensated k, const compensated ab)
{
  // FIXME optimize
  c_mul_cc(k, ab, ab);
}

static inline void c_sqrt_c(compensated k, const compensated m)
{
  compensated z0; c_set_d(z0, sqrt(m[0]));
  compensated w0; c_div_cc(w0, m, z0);
  compensated z1; c_add_cc(z1, z0, w0); z1[0] /= 2; z1[1] /= 2;
  compensated w1; c_div_cc(w1, m, z1);
  compensated z2; c_add_cc(z2, z1, w1); z2[0] /= 2; z2[1] /= 2;
  compensated w2; c_div_cc(w2, m, z2);
  compensated z3; c_add_cc(z3, z2, w2); z3[0] /= 2; z3[1] /= 2;
  compensated w3; c_div_cc(w3, m, z3);
  compensated z4; c_add_cc(z4, z3, w3); z4[0] /= 2; z4[1] /= 2;
  compensated w4; c_div_cc(w4, m, z4);
                  c_add_cc(k, z4, w4); k[0] /= 2; k[1] /= 2;
}

static inline void c_floor_c(compensated k, const compensated ab)
{
  c_add_dd(k, floor(ab[0]), floor(ab[1]));
}

static inline void c_neg_c(compensated k, const compensated ab)
{
  k[0] = -ab[0];
  k[1] = -ab[1];
}

static inline void c_abs_c(compensated k, const compensated ab)
{
  if (ab[0] < 0) c_neg_c(k, ab); else c_set_c(k, ab);
}

static inline void c_log1p_c(compensated k, const compensated ab)
{
  // FIXME accuracy
  c_set_d(k, log1p(ab[0]));
}

#define i_sgn_c(o,a) (o)=sgn((a)[0])

static inline void c_read_Ci(compensated k, const compensated *p, int i)
{
  k[0] = p[i][0];
  k[1] = p[i][1];
}

static inline void v_write_Cic(compensated *p, int i, const compensated k)
{
  p[i][0] = k[0];
  p[i][1] = k[1];
}

#define b_lt_cc(o,a,b) (o)=(((a)[0]==(b)[0])?((a)[1]<(b)[1]):((a)[0]<(b)[0]))
*/

#define b_read_Vi(a,b,c) (a)=(b)[(c)]
#define v_write_Fif(b,c,d) (b)[(c)]=(d)
#define v_write_Did(b,c,d) (b)[(c)]=(d)
#define v_write_Lil(b,c,d) (b)[(c)]=(d)
#define b_and_bb(a,b,c) (a)=(b)&&(c)
#define b_or_bb(a,b,c) (a)=(b)||(c)
#define b_set_b(a,b) (a)=(b)
#define b_set_i(a,b) (a)=(b)
#define b_not_b(a,b) (a)=!(b)
#define i_set_i(a,b) (a)=(b)
#define i_neg_i(a,b) (a)=-(b)
#define i_sqr_i(a,b) (a)=(b)*(b)
#define i_add_ii(a,b,c) (a)=(b)+(c)
#define i_mul_ii(a,b,c) (a)=(b)*(c)
#define i_ifte_bii(a,c,t,f) (a)=(c)?(t):(f)

#define f_set_i(a,b) (a)=(b)
#define f_set_f(a,b) (a)=(b)
#define f_sqr_f(a,b) (a)=(b)*(b)
#define f_neg_f(a,b) (a)=(b)
#define f_abs_f(a,b) (a)=abs((b))
#define i_sgn_f(a,b) (a)=sgn((b))
#define f_mul2_f(a,b) (a)=(b)+(b)
#define f_add_if(a,b,c) (a)=(b)+(c)
#define f_add_ff(a,b,c) (a)=(b)+(c)
#define f_mul_fi(a,b,c) (a)=(b)*(c)
#define f_mul_if(a,b,c) (a)=(b)*(c)
#define f_mul_ff(a,b,c) (a)=(b)*(c)
#define f_read_Fi(a,b,c) (a)=(b)[(c)]
#define f_diffabs_ff(a,b,c) (a)=diffabsf((b),(c))
#define f_sqrt_f(a,b) (a)=sqrtf((b))
#define f_log1p_f(a,b) (a)=log1pf((b))
#define f_floor_f(a,b) (a)=floorf((b))
#define f_inv_f(a,b) (a)=1.0f/(b)

#define d_read_Di(a,b,c) (a)=(b)[(c)]
#define d_set_i(a,b) (a)=(b)
#define d_set_d(a,b) (a)=(b)
#define d_neg_d(a,b) (a)=-(b)
#define d_abs_d(a,b) (a)=abs((b))
#define i_sgn_d(a,b) (a)=sgn((b))
#define d_sgn_d(a,b) (a)=sgn((b))
#define d_sqr_d(a,b) (a)=(b)*(b)
#define d_mul2_d(a,b) (a)=(b)+(b)
#define d_add_ii(a,b,c) (a)=(b)+(c)
#define d_add_di(a,b,c) (a)=(b)+(c)
#define d_add_id(a,b,c) (a)=(b)+(c)
#define d_add_dd(a,b,c) (a)=(b)+(c)
#define d_mul_di(a,b,c) (a)=(b)*(c)
#define d_mul_id(a,b,c) (a)=(b)*(c)
#define d_mul_dd(a,b,c) (a)=(b)*(c)
#define d_log1p_d(a,b) (a)=log1p((b))
#define d_sqrt_d(a,b) (a)=sqrt((b))
#define d_floor_d(a,b) (a)=floor((b))
#define d_diffabs_dd(a,b,c) (a)=diffabs((b),(c))
#define i_sgn_d(a,b) (a)=sgn((b))
#define d_inv_d(a,b) (a)=1.0/(b)
#define d_inv_i(a,b) (a)=1.0/(b)
#define d_exp_d(a,b) (a)=exp((b))
#define d_log_d(a,b) (a)=log((b))
#define d_expm1_d(a,b) (a)=expm1((b))
#define d_log1p_d(a,b) (a)=log1p((b))
#define d_sin_d(a,b) (a)=sin((b))
#define d_cos_d(a,b) (a)=cos((b))
#define d_tan_d(a,b) (a)=tan((b))
#define d_sinh_d(a,b) (a)=sinh((b))
#define d_cosh_d(a,b) (a)=cosh((b))
#define d_tanh_d(a,b) (a)=tanh((b))

#define l_read_Li(a,b,c) (a)=(b)[(c)]
#define l_set_i(a,b) (a)=(b)
#define l_set_l(a,b) (a)=(b)
#define l_neg_l(a,b) (a)=-(b)
#define l_abs_l(a,b) (a)=abs((b))
#define i_sgn_l(a,b) (a)=sgn((b))
#define l_sqr_l(a,b) (a)=(b)*(b)
#define l_mul2_l(a,b) (a)=(b)+(b)
#define l_add_li(a,b,c) (a)=(b)+(c)
#define l_add_il(a,b,c) (a)=(b)+(c)
#define l_add_ll(a,b,c) (a)=(b)+(c)
#define l_mul_li(a,b,c) (a)=(b)*(c)
#define l_mul_il(a,b,c) (a)=(b)*(c)
#define l_mul_ll(a,b,c) (a)=(b)*(c)
#define l_log1p_l(a,b) (a)=log1pl((b))
#define l_sqrt_l(a,b) (a)=sqrtl((b))
#define l_floor_l(a,b) (a)=floorl((b))
#define l_diffabs_ll(a,b,c) (a)=diffabsl((b),(c))
#define i_sgn_l(a,b) (a)=sgn((b))
#define l_inv_l(a,b) (a)=1.0l/(b)
#define l_inv_i(a,b) (a)=1.0l/(b)
#define l_exp_l(a,b) (a)=expl((b))
#define l_log_l(a,b) (a)=logl((b))
#define l_expm1_l(a,b) (a)=expm1l((b))
#define l_log1p_l(a,b) (a)=log1pl((b))
#define l_sin_l(a,b) (a)=sinl((b))
#define l_cos_l(a,b) (a)=cosl((b))
#define l_tan_l(a,b) (a)=tanl((b))
#define l_sinh_l(a,b) (a)=sinhl((b))
#define l_cosh_l(a,b) (a)=coshl((b))
#define l_tanh_l(a,b) (a)=tanhl((b))

#define f_set_r(a,b) (a)=mpfr_get_d((b),MPFR_RNDN)
#define d_set_r(a,b) (a)=mpfr_get_d((b),MPFR_RNDN)
#define l_set_r(a,b) (a)=mpfr_get_ld((b),MPFR_RNDN)
#define r_mul2_r(a,b) mpfr_mul_2ui((a),(b),1,MPFR_RNDN)
#define r_neg_r(a,b) mpfr_neg((a),(b),MPFR_RNDN)
#define r_sqr_r(a,b) mpfr_sqr((a),(b),MPFR_RNDN)
#define r_inv_r(a,b) mpfr_ui_div((a),1,(b),MPFR_RNDN)
#define r_abs_r(a,b) mpfr_abs((a),(b),MPFR_RNDN)
#define r_set_r(a,b) mpfr_set((a),(b),MPFR_RNDN)
#define r_add_rr(a,b,c) mpfr_add((a),(b),(c),MPFR_RNDN)
#define r_add_ir(a,b,c) mpfr_add_si((a),(c),(b),MPFR_RNDN)
#define r_add_ri(a,b,c) mpfr_add_si((a),(b),(c),MPFR_RNDN)
#define r_mul_rr(a,b,c) mpfr_mul((a),(b),(c),MPFR_RNDN)
#define r_mul_ir(a,b,c) mpfr_mul_si((a),(c),(b),MPFR_RNDN)
#define r_mul_ri(a,b,c) mpfr_mul_si((a),(b),(c),MPFR_RNDN)
#define r_mul_dr(a,b,c) mpfr_mul_d((a),(c),(b),MPFR_RNDN)
#define r_mul_rd(a,b,c) mpfr_mul_d((a),(b),(c),MPFR_RNDN)
#define i_sgn_r(a,b) (a)=sgn(mpfr_sgn((b)))
#define r_sqrt_r(a,b) mpfr_sqrt((a),(b),MPFR_RNDN)
#define r_exp_r(a,b) mpfr_exp((a),(b),MPFR_RNDN)
#define r_log_r(a,b) mpfr_log((a),(b),MPFR_RNDN)
#define r_expm1_r(a,b) mpfr_expm1((a),(b),MPFR_RNDN)
#define r_log1p_r(a,b) mpfr_log1p((a),(b),MPFR_RNDN)
#define r_sin_r(a,b) mpfr_sin((a),(b),MPFR_RNDN)
#define r_cos_r(a,b) mpfr_cos((a),(b),MPFR_RNDN)
#define r_tan_r(a,b) mpfr_tan((a),(b),MPFR_RNDN)
#define r_sinh_r(a,b) mpfr_sinh((a),(b),MPFR_RNDN)
#define r_cosh_r(a,b) mpfr_cosh((a),(b),MPFR_RNDN)
#define r_tanh_r(a,b) mpfr_tanh((a),(b),MPFR_RNDN)

#define b_lt_ii(a,b,c) (a)=(b)<(c)
#define b_lt_ff(a,b,c) (a)=(b)<(c)
#define b_lt_dd(a,b,c) (a)=(b)<(c)
#define b_lt_ll(a,b,c) (a)=(b)<(c)
#define b_lt_rr(a,b,c) (a)=mpfr_less_p((b),(c))
#define b_lt_ri(a,b,c) (a)=mpfr_cmp_si((b),(c))<0
#define b_le_ii(a,b,c) (a)=(b)<=(c)
#define b_le_ff(a,b,c) (a)=(b)<=(c)
#define b_le_dd(a,b,c) (a)=(b)<=(c)
#define b_le_ll(a,b,c) (a)=(b)<=(c)
#define i_eq_ii(a,b,c) (a)=(b)==(c)
#define b_eq_ii(a,b,c) (a)=(b)==(c)
#define b_eq_ff(a,b,c) (a)=(b)==(c)
#define b_eq_dd(a,b,c) (a)=(b)==(c)
#define b_eq_ll(a,b,c) (a)=(b)==(c)
#define b_le_if(a,b,c) (a)=(b)<=(c)
#define b_le_id(a,b,c) (a)=(b)<=(c)
#define b_le_il(a,b,c) (a)=(b)<=(c)


static inline float diffabsf(float c, float d) {
  if (c >= 0.0f) {
    if (c + d >= 0.0f) { return d; }
    else { return -(2.0f * c + d); }
  } else {
    if (c + d > 0.0f) { return 2.0f * c + d; }
    else { return -d; }
  }
}

static inline double diffabs(double c, double d) {
  if (c >= 0.0) {
    if (c + d >= 0.0) { return d; }
    else { return -(2.0 * c + d); }
  } else {
    if (c + d > 0.0) { return 2.0 * c + d; }
    else { return -d; }
  }
}

static inline long double diffabsl(long double c, long double d) {
  if (c >= 0.0L) {
    if (c + d >= 0.0L) { return d; }
    else { return -(2.0L * c + d); }
  } else {
    if (c + d > 0.0L) { return 2.0L * c + d; }
    else { return -d; }
  }
}

//typedef int f_plainf(int,float,float*,float,float,float,float,float*,volatile int*);
typedef int f_plain(int,double,double*,double,double,double,double,double*,volatile int*);
typedef int f_plainl(int,long double,long double*,long double,long double,long double,long double,long double*,volatile int*);
//typedef int f_plainc(int,compensated,compensated*,compensated,compensated,compensated,compensated,compensated*,volatile int*);
//typedef int f_referencef(int,float,float,float,mpfr_t,mpfr_t,float*,volatile int*);
typedef int f_reference(int,double,double,double,mpfr_t,mpfr_t,double*,volatile int*);
//typedef int f_referencel(int,long double,long double,long double,mpfr_t,mpfr_t,long double*,volatile int*);
//typedef int f_perturbationf(int,int,float,float*,float,float,float,float,float*,float*,volatile int*);
typedef int f_perturbation(int,int,double,double*,double,double,double,double,double*,double*,volatile int*);
//typedef int f_perturbationl(int,int,long double,long double*,long double,long double,long double,long double,long double*,long double*,volatile int*);
typedef int f_period_tri(int,double,double,double,mpfr_t,mpfr_t,mpfr_t,volatile int*);
typedef int f_period_jsk(int,double,double,double,mpfr_t,mpfr_t,mpfr_t,double*,volatile int*);
typedef int f_newton(int,int,double,double,mpfr_t,mpfr_t,volatile int*);
typedef int f_misiurewicz(int,int,int,double,double,mpfr_t,mpfr_t,volatile int*);
typedef int f_size(int,double,double,mpfr_t,mpfr_t,mpfr_t,double*,volatile int*);
typedef int f_skew(int,double,double,mpfr_t,mpfr_t,int,double*,volatile int*);
typedef int f_domain_size(int,double,double,mpfr_t,mpfr_t,mpfr_t,volatile int*);

extern int et_wrap_plainl(f_plainl*,int,long double *,long double *,long double *,long double *,long double *,long double *,long double *,volatile int*);
//extern int et_wrap_plainc(f_plainc*,int ,compensated*,compensated*,compensated*,compensated*,compensated*,compensated*,compensated*,volatile int*);
//extern int et_wrap_referencel(f_referencel*,int,long double*,long double*,long double*,mpfr_t,mpfr_t,long double*,volatile int*);
//extern int et_wrap_perturbationl(f_perturbationl*,int,int,long double*,long double*,long double*,long double*,long double*,long double*,long double*,long double*,volatile int*);

#ifndef ET_MAIN
//static f_plainf plainf;
static f_plain plain;
static f_plainl plainl;
//static f_plainc plainc;
//static f_referencef referencef;
static f_reference reference;
//static f_referencel referencel;
//static f_perturbationf perturbationf;
static f_perturbation perturbation;
//static f_perturbationl perturbationl;
static f_period_tri period_tri;
static f_period_jsk period_jsk;
static f_newton newton;
static f_misiurewicz misiurewicz;
static f_size size;
static f_skew skew;
static f_domain_size domain_size;
//static const char name[];
//static const char source[];
#endif

#define MAGIC ((int)(0xC01dCaf3))
#define SIZE ((int)(sizeof(struct formula)))
#define VERSION 8

struct formula
{
  int magic;
  int ssize;
  int version;
  const char *name;
  const char *source;
  double degree;
//  f_plainf *plainf;
  f_plain *plain;
  f_plainl *plainl;
//  f_plainc *plainc;
//  f_referencef *referencef;
  f_reference *reference;
//  f_referencel *referencel;
//  f_perturbationf *perturbationf;
  f_perturbation *perturbation;
//  f_perturbationl *perturbationl;
  f_period_tri *period_tri;
  f_period_jsk *period_jsk;
  f_newton *newton;
  f_misiurewicz *misiurewicz;
  f_size *size;
  f_skew *skew;
  f_domain_size *domain_size;
};

extern struct formula et;

#define FORMULA(name,source,degree) \
struct formula et = \
{ MAGIC, SIZE, VERSION \
, name, source, degree \
, &plain, &plainl \
, &reference \
, &perturbation \
, &period_tri, &period_jsk \
, &newton, &misiurewicz \
, &size, &skew, &domain_size \
};

#endif
