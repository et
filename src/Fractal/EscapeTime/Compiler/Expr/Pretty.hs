{-
et -- escape time fractals
Copyright (C) 2018 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Fractal.EscapeTime.Compiler.Expr.Pretty where

import Data.Ratio (numerator, denominator)

import Fractal.EscapeTime.Compiler.Expr.AST

pretty :: Expr String -> String
pretty (EInt i) = show i
pretty (ERat i) = "(" ++ show (numerator i) ++ "/" ++ show (denominator i) ++ ")"
pretty (EVar _ v) = v
pretty (ENeg _ e) = "-(" ++ pretty e ++ ")"
pretty (EInv _ e) = "1/(" ++ pretty e ++ ")"
pretty (EAdd _ a b) = "(" ++ pretty a ++ "+" ++ pretty b ++ ")"
pretty (EMul _ a b) = "(" ++ pretty a ++ "*" ++ pretty b ++ ")"
pretty (EAbs _ e) = "abs(" ++ pretty e ++ ")"
pretty (ESin _ e) = "sin(" ++ pretty e ++ ")"
pretty (ECos _ e) = "cos(" ++ pretty e ++ ")"
pretty (ETan _ e) = "tan(" ++ pretty e ++ ")"
pretty (ESinh _ e) = "sinh(" ++ pretty e ++ ")"
pretty (ECosh _ e) = "cosh(" ++ pretty e ++ ")"
pretty (ETanh _ e) = "tanh(" ++ pretty e ++ ")"
pretty (EPi _) = "pi"
pretty (EExp _ e) = "exp(" ++ pretty e ++ ")"
pretty (EExpM1 _ e) = "expm1(" ++ pretty e ++ ")"
pretty (ELog _ e) = "log(" ++ pretty e ++ ")"
pretty (ELog1P _ e) = "log1p(" ++ pretty e ++ ")"
pretty (EFloat e) = pretty e
pretty (ESgn   e) = "sgn(" ++ pretty e ++ ")"
pretty (ESqrt _ e) = "sqrt(" ++ pretty e ++ ")"
pretty (EASin _ e) = "asin(" ++ pretty e ++ ")"
pretty (EACos _ e) = "acos(" ++ pretty e ++ ")"
pretty (EATan _ e) = "atan(" ++ pretty e ++ ")"
pretty (EASinh _ e) = "asinh(" ++ pretty e ++ ")"
pretty (EACosh _ e) = "acosh(" ++ pretty e ++ ")"
pretty (EATanh _ e) = "atanh(" ++ pretty e ++ ")"
pretty (EFloor _ e) = "floor(" ++ pretty e ++ ")"
pretty (ELess a b) = "(" ++ pretty a ++ "<" ++ pretty b ++ ")"
pretty (ELessEqual a b) = "(" ++ pretty a ++ "<=" ++ pretty b ++ ")"
pretty (EEqual a b) = "(" ++ pretty a ++ "==" ++ pretty b ++ ")"
pretty (EOr a b) = "(" ++ pretty a ++ "||" ++ pretty b ++ ")"
pretty (EAnd a b) = "(" ++ pretty a ++ "&&" ++ pretty b ++ ")"
pretty (ERead a b) = "(" ++ pretty a ++ "[" ++ pretty b ++ "])"
pretty (EDiffAbs _ a b) = "diffabs(" ++ pretty a ++ "," ++ pretty b ++ ")"
pretty (EIf t a b) = "(" ++ pretty t ++ "?" ++ pretty a ++ ":" ++ pretty b ++ ")"
